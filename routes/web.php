<?php

Route::get('/', function () {
    return redirect('/login');
});

Auth::routes();

Route::group(['middleware' => ['auth', 'permission']], function () 
{
    Route::get('/home', 'HomeController@index');
    Route::group(['prefix' => 'users'], function () {
        Route::match(['get', 'post', 'put'], '/history', 'UsersController@history');
        Route::match(['get', 'post'], '/tracking', 'UsersController@tracking');
        Route::match(['get', 'post'], '/search', 'UsersController@search');
        Route::get('/{phone}/phone', 'UsersController@searchByPhone');
    });
    Route::post('/users/{id}/activate', 'UsersController@activate');
    Route::match(['get', 'post'], '/profile' , 'UsersController@profile');
    Route::match(['get', 'post'], '/change-password' , 'UsersController@setPassword');
    Route::match(['get', 'post'], '/users/{id}/password', 'UsersController@setUserPassword');
    Route::resource('users', 'UsersController');

    Route::post('/cooperatives/{id}/activate', 'CooperativesController@activate');
    Route::match(['get', 'post'], '/cooperatives/search', 'CooperativesController@search');
    Route::resource('cooperatives', 'CooperativesController');

    Route::post('/zones/{id}/activate', 'ZonesController@activate');
    Route::match(['get', 'post'], '/zones/search', 'ZonesController@search');
    Route::resource('zones', 'ZonesController');

    Route::group(['prefix' => 'reports'], function () {
        Route::post('/search', 'ReportsController@search');
        Route::post('/pdf', 'ReportsController@export');
        Route::get('/', 'ReportsController@index');
    });

    Route::group(['prefix' => 'report_payments'], function () {
        Route::post('/search', 'ReportPaymentsController@search');
        Route::post('/pdf', 'ReportPaymentsController@export');
        Route::get('/', 'ReportPaymentsController@index');
    });
    Route::get('/api-manager', 'ApiController@index');

    Route::group(['prefix' => 'drivers'], function () {
        Route::match(['get', 'post'], '/search', 'DriversController@search');
        Route::match(['get', 'post'], '/search-drivers', 'DriversController@searchDrivers');
    });

    Route::group(['prefix' => 'roads'], function () {
        Route::match(['get', 'post'], '/search', 'RoadsController@search');
        Route::post('/{id}/cancel', 'RoadsController@cancel');
        Route::post('/{id}/sendall','RoadsController@sendAll');
        Route::post('/sendone', 'RoadsController@sendOne');
    });
    Route::resource('/drivers', 'DriversController');

    Route::resource('/roads', 'RoadsController', ['except' => ['edit', 'show', 'update']]);

    Route::group(['prefix' => 'configs'], function () {
        Route::get('/', 'ConfigsController@index');
        Route::match(['get', 'post'], '/image', 'ConfigsController@image');
    });

    Route::group(['prefix' => 'reservations'], function () {
        Route::match(['get', 'post'], '/search', 'ReservatiosController@search');
        Route::post('/{id}/cancel', 'ReservationsController@cancel');
    });

    Route::resource('/reservas', 'ReservationsController');

    Route::group(['prefix' => 'customers'], function () {
        Route::get('/search', 'CustomersController@search');
    });
    Route::resource('/customers', 'CustomersController');
});

Route::get('/users/{id}/get', 'UsersController@getUser');
