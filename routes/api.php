<?php

// Auths
Route::post('/auth/login', 'Api\UsersApiController@login');
Route::post('/auth/refresh', 'Api\UsersApiController@refresh');
Route::post('/register/users', 'Api\UsersApiController@registerUser');
Route::post('/register/drivers', 'Api\UsersApiController@registerDriver');
Route::post('/register/web', 'Api\UsersApiController@registerWeb');
Route::post('/register/status', 'Api\UsersApiController@registerStatus');

Route::group(['middleware' => 'auth:api'], function() {
    //Users
    Route::group(['prefix' => 'users'], function () {
        Route::get('/{id}/user-road', 'Api\UsersApiController@getUserRoads');
        Route::get('/{id}/driver-road', 'Api\UsersApiController@getDriverRoads');
        Route::post('/{id}/update-position' , 'Api\UsersApiController@updatePosition');
        Route::post('/{id}/device', 'Api\UsersApiController@updateDevice');
        Route::get('/{id}/history', 'Api\UsersApiController@getHistory');
		Route::post('/set-password', 'Api\UsersApiController@setPassword');
		Route::post('/available', 'Api\UsersApiController@available');
        Route::post('/busy', 'Api\UsersApiController@busy');
        Route::post('/disconnected', 'Api\UsersApiController@disconnected');
        Route::post('/nearest-drivers', 'Api\UsersApiController@getNearestDrivers');
    });
    
    Route::resource('users', 'Api\UsersApiController', ['only' => ['index', 'update', 'destroy']]);
    
    //Drivers
    Route::group(['prefix' => 'drivers'], function () {
        Route::get('/', 'Api\RoadsApiController@index');
        Route::post('/', 'Api\RoadsApiController@store');
        Route::get('/{id}', 'Api\RoadsApiController@show');
        Route::post('/{id}/accept', 'Api\RoadsApiController@accept');
        Route::post('/{id}/cancel', 'Api\RoadsApiController@cancel');
        Route::post('/{id}/finish', 'Api\RoadsApiController@finish');
        Route::post('/{id}/notify', 'Api\RoadsApiController@notify');
        Route::post('/{id}/start', 'Api\RoadsApiController@start');
    });
	
    // Logout
	Route::post('auth/logout', 'Api\UsersApiController@logout');
});
