<?php

namespace App;

use Moloquent as Model;

class FailedJob extends Model
{
    protected $fillable = [
		'job', 
		'exception'
    ];
}
