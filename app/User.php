<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;
	
	/**
	 * SoftDeletingTrait
	 */
	use SoftDeletes;

	/**
	 * SoftDeletingTrait
	 *
	 * @var Date
	 */
    protected $dates = [
		'deleted_at'
	];
	
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
    protected $fillable = [
		'dni', 
		'rut', 
		'name', 
		'email', 
		'password', 
		'image', 
		'latitude', 
		'longitude', 
		'availability',
		'address', 
		'phone', 
		'cell', 
		'number_bps', 
		'license_cta', 
		'license_vto', 
		'file_permission', 
		'role', 
		'status', 
		'device_id',  
		'cooperative_id',
    ];

    public function cooperative()
    {
        return $this->belongsTo('App\Cooperative');
    }

    protected $hidden = [
		'password', 
		'remember_token', 
    ];
	
    public function roads()
    {
        return $this->hasMany('App\Road');
    }
    
	public function drives()
    {
        return $this->hasMany('App\Road', 'driver_id');
    }
}
