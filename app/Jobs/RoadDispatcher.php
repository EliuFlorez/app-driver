<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Road;
use App\User;
use GuzzleHttp\Client;
use App\FailedJob;

class RoadDispatcher implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    
	protected $road;

    public function __construct($road)
    {
        $this->road = $road;
    }
    
    public function handle()
    {
        $limit = 5;
        $max = 20;
        $incrementing = 5;
        $drivers = array();
        $client = new Client();
        do {
            if ($limit <= ($limit * 3) && $this->road->cooperative_id != null) {
                $users = User::where([
					'cooperative_id' => $this->road->cooperative_id, 
					'role' => 'T', 
					'availability' => 'L'
				])->get();
			} else {
                $users = User::where([
					'role' => 'T', 
					'availability' => 'L'
				])->get();
			}
            foreach ($users as $user)
            {
                if ($user->device_id != null && $user->latitude != null && $user->longitude != null && !in_array($user->device_id, $drivers))
                {
                    $distance = $this->getDistance(
						$this->road->location_latitude, 
						$this->road->location_longitude, 
						$user->latitude, 
						$user->longitude
					);
                    if ($distance <= $limit)
                    {
                        array_push($drivers, $user->device_id);
                        $client->post(config('app.fcm'), [
                            'form_params' => [
                                'to' => $user->device_id,
                                'data' => json_encode([
									'source' => 'new_road', 
									'road' => Road::with('user')->where('id', $this->road->id)->first()
								])
                            ],
                            'headers' => [
                                'Authorization' => 'key=' . config('app.fcm_key')
                            ]
                        ]);
                    }
                }
            }
            $limit = $limit + $incrementing;
            sleep(10);
        }
        while($this->isPending($this->road->id) && $limit <= $max);
    }
    
	private function getDistance($lat1, $lon1, $lat2, $lon2) {
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        return ($miles * 1.609344);
    }
    
	private function isPending($id)
    {
        $road = Road::with('user')->findOrFail($id);
		
        return ($road->status === 'P');
    }
    
	public function failed($exception)
    {
        FailedJob::create([
            'job' => 'dispatcher',
            'exception' => $exception->getMessage()
        ]);
    }
}
