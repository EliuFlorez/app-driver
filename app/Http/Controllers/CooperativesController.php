<?php	

namespace App\Http\Controllers;

use App\Cooperative;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use Storage;
use Auth;
use Hash;

class CooperativesController extends Controller
{
    public function __construct()
    {
        $this->middleware('distributor');
    }
    
	public function index()
    {
        $user = Auth::user();
		
        if ($user->role != 'A') {
            return view('cooperatives.list', ['cooperatives' => Cooperative::paginate(8)]);
        }
    }
    
	public function search(Request $request)
    {
		$search = $request->input('search');
		
		if ($search == '') {
			$cooperatives = Cooperative::paginate(8);
		} else {
			$cooperatives = Cooperative::where('name', 'like', "%$search%")->paginate(8);
		}

		return view('cooperatives.list', ['cooperatives' => $cooperatives]);
    }
    
	public function create()
    {
        $user = Auth::user();
		
        if ($user->role != 'A') {
            return view('cooperatives.create');
        }
    }
    
	public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255|unique:cooperatives',
            'image' => 'image',
        ]);
		
        $cooperative = Cooperative::create([
            'name' => $request->input('name'),
            'image' => '',
            'status' => true
        ]);
        
		$file = $request->file('image');
        
		if (isset($file)) {
            $imagePath = base_path() . '/public/uploads/images/';
            $imageName = $cooperative->id . '_' . $file->getClientOriginalName();
            $file->move($imagePath, $imageName);
            $cooperative->image = $imageName;
            $cooperative->save();
        }
		
        return view('cooperatives.list', ['cooperative' => $cooperative, 'cooperatives' => Cooperative::paginate(8)]);
    }
    
	public function show($id)
    {
        $user = Auth::user();
		
        if ($user->role != 'A') {
            $cooperative = Cooperative::findOrFail($id);
            return view('cooperatives.edit', ['cooperative' => $cooperative]);
        }
    }
    
	public function edit($id)
    {
		//
	}
	
    public function update(Request $request, $id)
    {
        $cooperative = Cooperative::findOrFail($id);

        if ($request->input('name') == $cooperative->name) {
            $this->validate($request, [
                'name' => 'required|max:255',
                'image' => 'image'
			]);
        } else {
            $this->validate($request, [
                'name' => 'required|max:255|unique:cooperatives',
                'image' => 'image'
			]);
        }

        $cooperative->name = $request->input('name');
        $file = $request->file('image');

        if (isset($file))
        {
            $imagePath = base_path() . '/public/uploads/images/';
            $imageName = $cooperative->id . '_' . $file->getClientOriginalName();
            $file->move($imagePath, $imageName);
            $cooperative->image = $imageName;
        }
        $cooperative->save();
		
        return view('cooperatives.list', ['cooperative' => $cooperative, 'cooperatives' => Cooperative::paginate(8)]);
    }
    
	public function destroy($id)
    {
        Cooperative::destroy($id);
		
        return view('cooperatives.list', ['cooperatives' => Cooperative::paginate(8)]);
    }

    public function activate(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'status' => 'required|boolean'
        ]);
        
		if ($validator->fails()) {
            return response()->json(['error' => true, 'messages' => $validator->errors()]);
        } else {
            $cooperative = Cooperative::findOrFail($id);
            $cooperative->status = $request->input('status');
            $cooperative->save();
            return response()->json(['error' => false, 'cooperative' => $cooperative]);
        }
    }
}