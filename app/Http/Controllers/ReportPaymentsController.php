<?php

namespace App\Http\Controllers;

use App\Cooperative;
use Illuminate\Http\Request;
use App\Road;
use App\Http\Requests;
use App\User;
use Carbon\Carbon;
use PDF;
use Auth;

class ReportPaymentsController extends Controller
{
    public function index()
    {
      $user = Auth::user();

      if($user->role=='S')
      {
          $now = Carbon::now();
         // date_sub($now, date_interval_create_from_date_string('1 months'));
          $previus_month = $now->month;
          $previus_year = $now->year;
          $current_date =Carbon::now();

          $now2 = Carbon::now();
         // date_sub($now2, date_interval_create_from_date_string('1 months'));
          $lastday = $now2->endOfMonth();


          $now3 = Carbon::now();
          //date_sub($now3, date_interval_create_from_date_string('1 months'));
          $firstday = $now3->firstOfMonth();



          $disabling_date = Carbon::now(); //fecha limite de deshabilitación de taxista
          //date_sub($disabling_date, date_interval_create_from_date_string('1 months'));
          $disabling_date->day(10);
          $disabling_date->setTime(23,59,59);

          $create_date = Carbon::now(); //fecha limite de creacion de taxista
         // date_sub($create_date, date_interval_create_from_date_string('1 months'));
          $create_date->day(25);
          $create_date->setTime(23,59,59);


          $ids_drivers_selected = [];
          $array_result=[];
          $up=null;

          $drivers = User::where('role','T')->where('prueba', '!=', true)->get();
          $cooperatives = Cooperative::get();
          $cooperative_name=null;
          $correct=false;
          $null_coop_exist=false;

          foreach($drivers as $driver)
          {
              if($driver['status']==true)
              {
                  if($driver['created_at']>$create_date)
                      $correct=false;
                  else
                      $correct=true;
              }
              else
              {
                  if($driver['status']==false)
                  {
                      if($driver['updated_at']>$disabling_date)
                          $correct=true;
                      else
                          $correct=false;
                  }
              }
              if($correct)
              {
                  if($driver['cooperative_id']==null)
                     $null_coop_exist=true;

                  foreach($cooperatives as $cooperative)
                  {
                      if((String)$cooperative['id'] == $driver['cooperative_id'])
                      {
                          $cooperative_name = $cooperative['name'];
                      }
                  }
                  array_push($ids_drivers_selected,$driver['id']);
                  array_push($array_result,(Object)[
                      'driver_id' => $driver['id'],
					  'cooperative' => $cooperative_name,
                      'numero_unidad' => $driver['vehicle_number'],
					  'name' => $driver['name'],
                      'total_carreras' => 0,
					  'total_recaudado' => 0,
                      'ultima_actualizacion' => $driver['updated_at']
				  ]);
              }
              $cooperative_name=null;
          }

          $roads = Road::where('created_at', '>', $firstday)->where('created_at', '<', $lastday)->whereIn('driver_id', $ids_drivers_selected)->get();


          $totaltaxis=count($ids_drivers_selected);
          $subtotal=$totaltaxis*13.5;
          $iva=$subtotal*0.14;
          $total=$subtotal+$iva;

          foreach($roads as $road)
          {
              foreach($array_result as $reg_driver)
              {
                  if($road['driver_id']==$reg_driver->driver_id)
                  {
                      $reg_driver->total_carreras++;
                      if($road['price']!=null)
                          $reg_driver->total_recaudado=$reg_driver->total_recaudado+floatval($road['price']);

                      break;
                  }
              }
          }

          switch($previus_month)
          {
              case 1: $previus_month='Enero'; break;
              case 2: $previus_month='Febrero'; break;
              case 3: $previus_month='Marzo'; break;
              case 4: $previus_month='Abril'; break;
              case 5: $previus_month='Mayo'; break;
              case 6: $previus_month='Junio'; break;
              case 7: $previus_month='Julio'; break;
              case 8: $previus_month='Agosto'; break;
              case 9: $previus_month='Septiembre'; break;
              case 10: $previus_month='Octubre'; break;
              case 11: $previus_month='Noviembre'; break;
              case 12: $previus_month='Diciembre'; break;
          }

          $pdf = PDF::loadView('reports.pdf-pago', [
			'previus_month' => $previus_month,
			'previus_year' => $previus_year,
			'array_result' => $array_result,
			'subtotal' => $subtotal,
			'cantidad_taxis' => $totaltaxis,
			'total' => $total,
			'iva' => $iva,
			'null_coop_exist' => $null_coop_exist, 
			'cooperatives' => $cooperatives
			]);
          
		  return $pdf->stream();
      }
    }

}

