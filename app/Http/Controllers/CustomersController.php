<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class CustomersController extends Controller
{

    public function index()
    {
        return view('customers.list', [
			'users' => User::orderBy('name', 'asc')->where('role', 'C')->paginate(10)
		]);
    }

    public function search(Request $request)
    {
        $search = $request->input('search');
        $status = $request->input('status');
        
		$users = User::where('role', 'C')->where(function ($query) use($search) {
            if ($search != null && $search != '') {
                $query->where('name', 'like', "%$search%")->orWhere('phone', 'like', "%$search%");
			}
        });
		
        switch ($status)
        {
            case 'A':
                $users->where('status', true);
                break;
            case 'I':
                $users->where('status', false);
                break;
            case 'F':
                $users->where('facebook_id', '!=', '')->whereNotNull('facebook_id');
                break;
        }
		
        return view('customers.list', [
			'users' => $users->paginate(10), 
			'status' => $status, 
			'search' => $search
		]);
    }

    public function create()
    {
        return view('customers.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'phone' => 'required|unique:users',
        ]);
		
        $user = User::create([
            'name' => $request->input('name'),
            'role' => 'C',
            'status' => true,
            'phone' => $request->input('phone'),
        ]);
		
        return $this->index();
    }

    public function show($id)
    {
        $customer = User::findOrFail($id);
		
        return view('customers.edit', ['user' => $customer]);
    }

    public function edit($id)
    {
        $customer = User::findOrFail($id);
		
        return view('customers.edit', ['user' => $customer]);
    }

    public function update(Request $request, $id)
    {
        $customer = User::findOrFail($id);
        
		$this->validate($request, [
            'name' => 'required|max:255',
            'phone' => 'required' . ($customer->phone === $request->input('phone')) ? '' : '|unique:users',
        ]);
		
        $customer->name = $request->input('name');
        $customer->phone = $request->input('phone');
        $customer->save();
		
        return $this->index();
    }

    public function destroy($id)
    {
        User::destroy($id);
		
        return $this->index();
    }
}
