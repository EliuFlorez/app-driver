<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use App\Road;
use App\User;
use GuzzleHttp\Client;
use Carbon\Carbon;
use Auth;
use App\Zone;
use DateTime;
use App\Jobs\RoadDispatcher;
use DB;

class ReservationsController extends Controller
{
    public function create()
    {
        $zones = null;
		
        switch(Auth::user()->role)
        {
            case 'S': $zones = Zone::get(); break;
            case 'D': $zones = Zone::get(); break;
            default:
                if (Auth::user()->cooperative_id != null) {
                    $zones = Zone::where('cooperative_id',Auth::user()->cooperative_id)->get();break;
                }
        }
		
        return view('reservations.create', ['zones' => $zones]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'phone' => 'required|max:255',
            'name' => 'required|max:255',
            'location_address' => 'required|max:255',
            'location_latitude' => 'required|numeric',
            'location_longitude' => 'required|numeric',
            'location_reference' => 'required|max:255',
            'destine_address' => 'required|max:255',
            'reservation_date' => 'required|date',
            'anticipation_send' => 'required'
        ]);
		
        $user = User::where('phone', $request->input('phone'))->first();
        $date =  Carbon::createFromFormat('Y/m/d H:i', $request->input('reservation_date'));
        date_sub($date, date_interval_create_from_date_string('5 hours'));

        if (!isset($user)) {
            $user = User::create([
                'name' => $request->input('name'),
                'phone' => $request->input('phone'),
                'role' => 'C'
            ]);
        }
		
        $auth = Auth::user();
        $road = Road::create([
            'user_id' => $user->id,
            'location_address' => $request->input('location_address'),
            'location_latitude' => $request->input('location_latitude'),
            'location_longitude' => $request->input('location_longitude'),
            'location_reference' => $request->input('location_reference'),
            'destine_address' => $request->input('destine_address'),
            'reservation_date' => $date,
            'anticipation_send' => $request->input('anticipation_send'),
            'status' => 'R',
            'type' => 'R',
            'cooperative_id' => $auth->cooperative_id
        ]);
		
        date_add($date, date_interval_create_from_date_string('5 hours'));
        
		$job = (new RoadDispatcher($road))->delay($date->subMinutes($request->input('anticipation_send')));
        $id = dispatch($job);
        $road->job_id = $id;
        $road->save();
        
		return redirect('/roads');
    }

    public function show($id)
    {
        $zones = null;

        switch(Auth::user()->role)
        {
            case 'S': $zones = Zone::get(); break;
            case 'D': $zones = Zone::get(); break;
            default:
                if (Auth::user()->cooperative_id != null) {
                    $zones = Zone::where('cooperative_id', Auth::user()->cooperative_id)->get(); break;
                }
        }
		
        $reserva = Road::findOrFail($id);
        $date = $reserva['reservation_date'];
        $date = $date->addHours(5);
        $user = User::findOrFail($reserva['user_id']);
		
        return view('reservas.edit', [
			'reserva' => $reserva, 
			'zones' => $zones, 
			'user' => $user, 
			'date' => $date
		]);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'phone' => 'required|max:255',
            'name' => 'required|max:255',
            'location_address' => 'required|max:255',
            'location_latitude' => 'required|numeric',
            'location_longitude' => 'required|numeric',
            'location_reference' => 'required|max:255',
            'destine_address' => 'required|max:255',
            'reservation_date' => 'required|date',
            'anticipation_send' => 'required'
        ]);

        $date =  Carbon::createFromFormat('Y/m/d H:i', $request->input('reservation_date'));
        date_sub($date, date_interval_create_from_date_string('5 hours'));
        $reserva = Road::findOrFail($id);
        $jobID = $reserva->job_id;
        
		if (isset($jobID)) {
            DB::table('jobs')->where('id', $jobID)->delete();
        }
		
        $auth = Auth::user();
        $user = User::where('phone', $request->input('phone'))->first();
		
        $reserva->user_id = $user['id'];
        $reserva->location_address = $request->input('location_address');
        $reserva->location_latitude = $request->input('location_latitude');
        $reserva->location_longitude = $request->input('location_longitude');
        $reserva->location_reference = $request->input('location_reference');
        $reserva->destine_address = $request->input('destine_address');
        $reserva->reservation_date = $date;
        $reserva->anticipation_send = $request->input('anticipation_send');
        $reserva->cooperative_id = $auth->cooperative_id;
		
        date_add($date, date_interval_create_from_date_string('5 hours'));
        $job = (new RoadDispatcher($reserva))->delay($date->subMinutes($request->input('anticipation_send')));
        $jobID = dispatch($job);
        $reserva->job_id = $jobID;
        $reserva->save();
        
		return redirect('/roads');
    }

}