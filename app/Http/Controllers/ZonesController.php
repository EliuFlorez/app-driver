<?php

namespace App\Http\Controllers;

use App\Zone;
use App\Cooperative;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use Storage;
use Auth;
use Hash;

class ZonesController extends Controller
{
    public function index()
    {
        return view('zones.list', ['zones' => Zone::paginate(8)]);
    }
    
	public function search(Request $request)
    {
        $search = $request->input('search');

        if ($search == '') {
            $zones = Zone::paginate(8);
        } else {
            $zones = Zone::where('descripcion', 'like', "%$search%")->paginate(8);
        }

        return view('zones.list', ['zones' => $zones]);
    }

    public function create()
    {
        switch(Auth::user()->role)
        {
            case 'S': $cooperatives = Cooperative::get(); break;
            case 'D': $cooperatives = Cooperative::get(); break;
            default:
                if (Auth::user()->cooperative_id != null) {
                    $cooperatives = Cooperative::where('id', Auth::user()->cooperative_id)->get(); break;
                }
        }

        return view('zones.create', ['cooperatives' => $cooperatives]);
    }
    
	public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'cooperative_id'=>'required',
            'descripcion' => 'required|max:255|unique:zones',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => true, 'messages' => $validator->errors()]);
        } else {
            $zone = Zone::create([
                'cooperative_id'=>$request->input('cooperative_id'),
                'descripcion' => $request->input('descripcion'),
                'zone' => $request->input('zone'),
                'status' => true
            ]);
            $zone->save();
            return response()->json(['error' => false]);
        }
    }

    public function show($id)
    {
        switch(Auth::user()->role)
        {
            case 'S': $cooperatives = Cooperative::get(); break;
            case 'D': $cooperatives = Cooperative::get(); break;
            default:
                if (Auth::user()->cooperative_id != null) {
                    $cooperatives = Cooperative::where('id', Auth::user()->cooperative_id)->get(); break;
                }
        }
        $zone = Zone::findOrFail($id);

        return view('zones.edit', ['zone' => $zone, 'cooperatives' => $cooperatives]);
    }
    
	public function edit($id)
    {
        //
    }
    
	public function update(Request $request, $id)
    {
        $zone = Zone::findOrFail($id);

        if($request->input('descripcion')==$zone->descripcion)
        {
            $validator = Validator::make($request->all(), [
                'descripcion' => 'required|max:255',
                'cooperative_id' => 'required'
            ]);
        }
        else {
            $validator = Validator::make($request->all(), [
                'descripcion' => 'required|max:255|unique:zones',
                'cooperative_id' => 'required'
            ]);
        }
		
        if ($validator->fails()) {
            return response()->json(['error' => true, 'messages' => $validator->errors()]);
        } else {
            $zone->cooperative_id = $request->input('cooperative_id');
            $zone->descripcion = $request->input('descripcion');
            $zone->zone = $request->input('zone');
            $zone->save();
            return response()->json(['error' => false]);
        }
    }
    
	public function destroy($id)
    {
        Zone::destroy($id);
		
        return view('zones.list', ['zones' => Zone::paginate(8)]);
    }
    
	public function activate(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'status' => 'required|boolean'
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => true, 'messages' => $validator->errors()]);
        } else {
            $zone = Zone::findOrFail($id);
            $zone->status = $request->input('status');
            $zone->save();
            return response()->json(['error' => false, 'zone' => $zone]);
        }
    }
}