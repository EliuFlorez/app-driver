<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use App\Road;
use App\User;
use GuzzleHttp\Client;
use Carbon\Carbon;
use Auth;
use App\Zone;
use App\Jobs\RoadDispatcher;

class RoadsController extends Controller
{
    public function index()
    {
        return view('roads.list');
    }
	
    public function search (Request $request)
    {
        $this->validate($request, [
            'filter' => 'required',
            'from' => 'date',
            'to' => 'date',
            'today' => 'required',
            'option' => 'required',
            'search' => 'max:255'
        ]);
		
        $filter = $request->input('filter');
        $today = $request->input('today');
        $option = $request->input('option');
        $search = $request->input('search');
        
		if ($today == 'H') {
            $from = Carbon::createFromFormat('m/d/Y H:i:s', date('m/d/Y 00:00:00'));
            $to = Carbon::createFromFormat('m/d/Y H:i:s', date('m/d/Y 23:59:59'));
        } else {
            $from = Carbon::createFromFormat('m/d/Y H:i:s', $request->input('from') . ' 00:00:00');
            $to = Carbon::createFromFormat('m/d/Y H:i:s', $request->input('to') . ' 23:59:59');
        }
		
        $roads = Road::orderBy('created_at', 'desc')
			->whereIn('status', explode(',', $filter))
			->where('created_at', '<=', $to)
			->where('created_at', '>=', $from);
			
        if ($option === 'U') {
            $driver = User::with('vehicle')->where('number', '', "%$search%")->first();
            if (isset($driver)) {
                $roads->where('driver_id', $driver->id);
			}
        }
        else if ($option === 'C')
        {
            $user = User::where('phone', $search)->first();
            if (isset($user)) {
                $roads->where('user_id', $user->id);
			}
        }
		
        $loggedUser = Auth::user();
        if ($loggedUser->role === 'A') {
            $drivers = User::where('cooperative_id', $loggedUser->cooperative_id);
            $ids = array();
            foreach ($drivers as $driver) {
                array_push($ids, $driver->id);
			}
            $roads->where(function ($query) use($ids, $loggedUser) {
                $query->whereIn('driver_id', $ids)->orWhere('cooperative_id', $loggedUser->cooperative_id);
            });
        }
        
		return view('roads.search', [
			'roads' => $roads->get(),
			'filter' => $filter
		]);
    }
    
	public function create()
    {
        $zones = null;
        
		switch(Auth::user()->role)
        {
            case 'S': $zones = Zone::get(); break;
            case 'D': $zones = Zone::get(); break;
            default:
                if (Auth::user()->cooperative_id != null) {
                    $zones = Zone::where('cooperative_id', Auth::user()->cooperative_id)->get();break;
                }
        }
		
        return view('roads.create', ['zones' => $zones]);
    }
    
	public function prueba()
    {
        $zones = null;
        
		switch(Auth::user()->role)
        {
            case 'S': $zones = Zone::get(); break;
            case 'D': $zones = Zone::get(); break;
            default:
                if (Auth::user()->cooperative_id != null) {
                    $zones = Zone::where('cooperative_id', Auth::user()->cooperative_id)->get();break;
                }
        }
        
		return view('roads.create', ['zones' => $zones, 'prueba' => true]);
    }
    
	public function store(Request $request)
    {
        $this->validate($request, [
            'phone' => 'required|max:255',
            'name' => 'required|max:255',
            'location_address' => 'required|max:255',
            'location_latitude' => 'required|numeric',
            'location_longitude' => 'required|numeric',
            'location_reference' => 'required|max:255',
            'destine_address' => 'required|max:255',
            'driver_id' => 'exists:users,id'
        ]);
		
        $user = User::where('phone', $request->input('phone'))->first();
        if (!isset($user)) {
            $user = User::create([
                'name' => $request->input('name'),
                'phone' => $request->input('phone'),
                'role' => 'C'
            ]);
        }
		
        $auth = Auth::user();
        $road = Road::create([
            'user_id' => $user->id,
            'location_address' => $request->input('location_address'),
            'location_latitude' => $request->input('location_latitude'),
            'location_longitude' => $request->input('location_longitude'),
            'location_reference' => $request->input('location_reference'),
            'destine_address' => $request->input('destine_address'),
            'status' => 'P',
            'type' => 'M',
            'cooperative_id' => $auth->cooperative_id
        ]);
		
        if ($request->input('driver_id') == null || $request->input('driver_id') === '') {
            dispatch(new RoadDispatcher(Road::with('user')->where('id', $road->id)->first()));
        } else {
            $driver = User::find($request->input('driver_id'));
            $client = new Client();
            $res = $client->post(config('app.fcm'), [
                'form_params' => [
                    'to' => $driver->device_id,
                    'data' => json_encode(['source' => 'new_road', 'road' => Road::with('user')->where('id', $road->id)->first()])
                ],
                'headers' => [
                    'Authorization' => 'key=' . config('app.fcm_key')
                ]
            ]);
        }
		
        return redirect('/roads');
    }

    public function save(Request $request)
    {
        $this->validate($request, [
            'phone' => 'required|max:255',
            'name' => 'required|max:255',
            'location_address' => 'required|max:255',
            'location_latitude' => 'required|numeric',
            'location_longitude' => 'required|numeric',
            'location_reference' => 'required|max:255',
            'destine_address' => 'required|max:255',
            'driver_id' => 'exists:users,id'
        ]);
		
        $user = User::where('phone', $request->input('phone'))->first();
        if (!isset($user))
        {
            $user = User::create([
                'name' => $request->input('name'),
                'phone' => $request->input('phone'),
                'role' => 'C'
            ]);
        }
        
		$auth = Auth::user();
        $road = Road::create([
            'user_id' => $user->id,
            'location_address' => $request->input('location_address'),
            'location_latitude' => $request->input('location_latitude'),
            'location_longitude' => $request->input('location_longitude'),
            'location_reference' => $request->input('location_reference'),
            'destine_address' => $request->input('destine_address'),
            'status' => 'P',
            'type' => 'M',
            'cooperative_id' => $auth->cooperative_id
        ]);
        
		if ($request->input('driver_id') == null || $request->input('driver_id') === '') {
            dispatch(new RoadDispatcher(Road::with('user')->where('id', $road->id)->first()));
        } else {
            $driver = User::find($request->input('driver_id'));
            $client = new Client();
            $res = $client->post(config('app.fcm'), [
                'form_params' => [
                    'to' => $driver->device_id,
                    'data' => json_encode(['source' => 'new_road', 'road' => Road::with('user')->where('id', $road->id)->first()])
                ],
                'headers' => [
                    'Authorization' => 'key=' . config('app.fcm_key')
                ]
            ]);
        }
        return redirect('/roads');
    }
    
	public function destroy($id)
    {
        $road = Road::findOrFail($id);
        $road->delete();
        return view('roads.list', ['roads' => Road::orderBy('created_at', 'desc')->paginate(10)]);
    }
    
	public function cancel($id)
    {
        $road = Road::findOrFail($id);
        $road->status = 'C';
        $road->save();
        $user = User::findOrFail($road->user_id);
        $driver = User::find($road->driver_id);
        $this->sendCancelNotification($road, $user->device_id);
        if (isset($driver))
        {
            $driver->availability = 'L';
            $driver->save();
            $this->sendCancelNotification($road, $driver->device_id);
        }
        return response()->json($road);
    }
    
	private function sendNotificationToDrivers($road)
    {    
        $limit = 3;
        $items = array();
        $drivers = User::where('role', 'T')->where('availability', 'L')->get();
        foreach ($drivers as $driver)
        {
            if ($driver->latitude != null && $driver->longitude != null && $driver->device_id != null)
            {
                $distance = $this->distance(
					$driver->latitude, 
					$driver->longitude, 
					$road->location_latitude, 
					$road->location_longitude
				);
                if ($distance <= $limit && !in_array($driver->device_id, $items)) {
                    $items[] = $driver->device_id;
				}
            }
        }
        foreach ($items as $device)
        {
            $client = new Client();
            $res = $client->post(config('app.fcm'), [
                'form_params' => [
                    'to' => $device,
                    'data' => json_encode(['source' => 'new_road', 'road' => $road])
                ],
                'headers' => [
                    'Authorization' => 'key=' . config('app.fcm_key')
                ]
            ]);
        }
        sleep(10);
        $accepted = Road::findOrFail($road->id);
        $others = array();
        if ($accepted->status != 'E')
        {
            foreach ($drivers as $driver) {
                if ($driver->latitude != null && $driver->longitude != null && $driver->device_id != null) {
                    if (!in_array($driver->device_id, $items)) {
                        $others[] = $driver->device_id;
					}
				}
			}

            foreach ($others as $device)
            {
                $client = new Client();
                $res = $client->post(config('app.fcm'), [
                    'form_params' => [
                        'to' => $device,
                        'data' => json_encode(['source' => 'new_road', 'road' => $road])
                    ],
                    'headers' => [
                        'Authorization' => 'key=' . config('app.fcm_key')
                    ]
                ]);
            }
        }
    }
    
	private function distance($lat1, $lon1, $lat2, $lon2) {
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        return ($miles * 1.609344);
    }

    private function sendCancelNotification($road, $device)
    {
        $client = new Client();
        $res = $client->post(config('app.fcm'), [
            'form_params' => [
                'to' => $device,
                'data' => json_encode(['source' => 'cancelled_road', 'road' => $road])
            ],
            'headers' => [
                'Authorization' => 'key=' . config('app.fcm_key')
            ]
        ]);
    }

    public function sendAll($id)
    {
       $this->sendNotificationToDrivers(Road::with('user')->where('id', $id)->first());
       return response()->json(['id' => $id]);
    }

    public function sendOne(Request $request)
    {
        $driver = User::find($request->input('driver_id'))->first();

        if ($driver != null) {
            $client = new Client();
            $res = $client->post(config('app.fcm'), [
                'form_params' => [
                    'to' => $driver->device_id,
                    'data' => json_encode(['source' => 'new_road', 'road' => Road::with('user')->where('id', $request->input('id_cab'))->first()])
                ],
                'headers' => [
                    'Authorization' => 'key=' . config('app.fcm_key')
                ]
            ]);
            return response()->json(['envio' => true]);
        } else {
            return response()->json(['envio' => false]);
		}
    }

}
