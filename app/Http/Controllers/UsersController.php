<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use App\User;
use Storage;
use Auth;
use Hash;
use App\Road;
use Carbon\Carbon;
use \DateTime;
use App\Cooperative;

class UsersController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $users = User::orderBy('name', 'asc')->where('id', '!=', $user->id);
        
		if ($user->role === 'A') {
            $users->whereIn('role', ['A'])->where('cooperative_id', $user->cooperative_id);
        } else if ($user->role === 'D') {
            $users->whereIn('role', ['D', 'A']);
        } else {
            $users->whereIn('role', ['D', 'A', 'S']);
		}
        
		return view('users.list', ['users' => $users->paginate(10)]);
    }
    
	public function get($id)
    {
        return response()->json(User::findOrFail($id));
    }
    
	public function search(Request $request)
    {
        $search = $request->input('search');
        $status = $request->input('status');
        $role = $request->input('role');
        $users = User::orderBy('name', 'asc')->where('id', '!=', Auth::user()->id)->where(function ($query) use ($search){
            if ($search != null && $search != '') {
                $query->where('name', 'like', "%$search%")->orWhere('email', 'like', "%$search%");
			}
        });
        
		if ($status != 'T') {
            $users->where('status', ($status === 'A'));
        }
		if ($role != 'C') {
            $users->where('role', $role);
        } else {
            $users->whereIn('role', ['D', 'A']);
		}
        if (Auth::user()->role === 'A') {
            $users->where('cooperative_id', Auth::user()->cooperative_id);
		}
		
        return view('users.list', [
			'users' => $users->paginate(10), 
			'role' => $role, 
			'status' => $status
		]);
    }
    
	public function create()
    {
        $user = Auth::user();
        
		if ($user->role == 'S' || $user->role == 'D') {
            $cooperatives = Cooperative::orderBy('name', 'asc')->get();
        } else {
            $cooperatives = Cooperative::where('id', $user->cooperative_id)->get();
		}
		
        return view('users.create', ['cooperatives' => $cooperatives]);
    }
    
	public function history(Request $request)
    {
        if ($request->isMethod('get'))
        { 
            $user = $request->user();
            if ($user->role != 'A') {
                $users = User::orderBy('name', 'asc')->where('role','T')->get();
            } else {
                $users = User::orderBy('name', 'asc')->where('cooperative_id', $user->cooperative_id)->where('role','T')->get();
			}
            return view('users.history', ['users' => $users]);
        }
        else if ($request->isMethod('put'))
        {
            $search = $request->input('search');
            $user = $request->user();
            if ($search != null)
            {
                if ($user->role != 'A') {
                    $users = User::where('name', 'like', "%$search%")->where('role','T')->get();
                } else {
                    $users = User::where('cooperative_id', $user->cooperative_id)->where('name', 'like', "%$search%")->where('role','T')->get();
				}
            }
            else {
                if ($user->role != 'A') {
                    $users = User::where('role', 'T')->get();
                } else {
                    $users = User::where('cooperative_id', $user->cooperative_id)->where('role', 'T')->get();
				}
            }
            return view('users.history', ['users' => $users]);
        }
        else {
            $this->validate($request, [
                'user' => 'required|exists:users,id',
                'filter' => 'required',
                'from' => 'required_if:filter,O|date|before:to',
                'to' => 'required_if:filter,O|date'
            ]);
            $filter = $request->input('filter');
            $user_id = $request->input('user');
            $users = User::where('id', $user_id)->get();
            if ($filter === 'H')
            {
                $roads = Road::where('created_at', '>', Carbon::today())->where('driver_id', $user_id)->paginate(10);
                return view('users.history', [
					'users' => $users, 
					'roads' => $roads, 
					'user_id' => $user_id, 
					'filter' => $filter
				]);
            }
            else if ($filter === 'S')
            {
                $from = new DateTime('monday this week'); 
                $to = new DateTime('monday next week'); 
            }
            else if ($filter === 'M')
            {
                $from = new DateTime('first day of this month');
                $to = new DateTime('last day this month +1 day');
            }
            else if ($filter === 'O')
            {
                $from = DateTime::createFromFormat('m/d/Y', $request->input('from'));
                $to = DateTime::createFromFormat('m/d/Y', $request->input('to'));
            }
			
            $roads = Road::where('created_at', '>' , $from)->where('created_at', '<', $to)->where('driver_id', $user_id)->paginate(10);
            return view('users.history', [
				'users' => $users, 
				'roads' => $roads, 
                'user_id' => $user_id, 
				'filter' => $filter, 
                'from' => $from->format('m/d/Y'), 
				'to' => $to->format('m/d/Y')
			]);
        }
    }
    
	public function activate(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'status' => 'required|boolean'
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => true, 'messages' => $validator->errors()]);
        } else {
            $user = User::findOrFail($id);
            $user->status = ($request->input('status') === '1');
            $user->save();
            return response()->json(['error' => false, 'user' => $user]);
        }
    }
    
	public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'image' => 'image',
            'role' => 'required|size:1',
            'cooperative' => 'exists:cooperatives,id|required_if:role,A'
        ]); 
        $user = User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
            'role' => $request->input('role'),
            'status' => true,
            'cooperative_id' => $request->input('cooperative')
        ]);
        $file = $request->file('image');
        if (isset($file))
        {
            $imagePath = base_path() . '/public/uploads/images/';
            $imageName = $user->id . '_' . $file->getClientOriginalName();
            $file->move($imagePath, $imageName);
            $user->image = $imageName;
            $user->save();
        }
        return $this->index();
    }
    
	public function tracking(Request $request)
    {
        $filter = $request->input('filter');
        $search = $request->input('search');
        $user = $request->user();
        if (!isset($filter) || $filter === 'T')
        {
            if ($user->role != 'A')
            {
                if ($search != null) {
                    $users = User::where('status', true)->where('name', 'like', "%$search%")->where('role', 'T')->get();
                } else {
                    $users = User::where('status', true)->where('role', 'T')->get();
				}
            }
            else 
            {
                if ($search != null) {
                    $users = User::where('cooperative_id', $user->cooperative_id)->where('status', true)->where('name', 'like', "%$search%")->where('role', 'T')->get();
                } else {
                    $users = User::where('cooperative_id', $user->cooperative_id)->where('status', true)->where('role', 'T')->get();
				}
            }
        }          
        else 
        {
            if ($user->role != 'A')
            {
                if ($search != null) {
                    $users = User::where('status', true)->where('availability', $filter)->where('name', 'like', "%$search%")->where('role', 'T')->get();
                } else {
                    $users = User::where('status', true)->where('availability', $filter)->where('role', 'T')->get();
				}
            }
            else 
            {
                if ($search != null) {
                    $users = User::where('cooperative_id', $user->cooperative_id)->where('status', true)->where('availability', $filter)->where('name', 'like', "%$search%")->where('role', 'T')->get();
                } else {
                    $users = User::where('cooperative_id', $user->cooperative_id)->where('status', true)->where('availability', $filter)->where('role', 'T')->get();
				}
            }
        }
        return view('users.tracking', ['users' => $users, 'filter' => $filter, 'search' => $search]);
    }
    
	public function show($id)
    {
        $user = User::findOrFail($id);
        $auth = Auth::user();
        if ($auth->role == 'S' || $auth->role == 'D') {
            $cooperatives = Cooperative::orderBy('name', 'asc')->get();
        } else {
            $cooperatives = Cooperative::where('id', $auth->cooperative_id)->get();
		}
        if (($auth->role != 'S' && $user->role === 'S') || $auth->id == $user->id) {
            abort(403);
        } else {
            return view('users.edit', ['user' => $user, 'cooperatives' => $cooperatives]);
		}
    }
    
	public function getUser(Request $request, $id)
    {
        return response()->json(User::findOrFail($id));
    }
    
	public function searchByPhone($phone)
    {
        return response()->json(User::where('phone', $phone)->first());
    }
    
	public function edit($id)
    {
        $user = User::findOrFail($id);
        $auth = Auth::user();
        if ($auth->role == 'S' || $auth->role == 'D') {
            $cooperatives = Cooperative::orderBy('name', 'asc')->get();
        } else {
            $cooperatives = Cooperative::where('id', $auth->cooperative_id)->get();
		}
        if (($auth->role != 'S' && $user->role === 'S') || $auth->id == $user->id) {
            abort(403);
        } else {
            return view('users.edit', ['user' => $user, 'cooperatives' => $cooperatives]);
		}
    }
    
	public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users,email,' . $id,
            'image' => 'image',
            'role' => 'required|size:1',
            'cooperative' => 'exists:cooperatives,id'
        ]);
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->role = $request->input('role');
        $user->cooperative_id = $request->input('cooperative');
        $file = $request->file('image');
        if (isset($file))
        {
            $imagePath = base_path() . '/public/uploads/images/';
            $imageName = $user->id . '_' . $file->getClientOriginalName();
            $file->move($imagePath, $imageName);
            $user->image = $imageName;
        }
        $user->save();
        return view('users.list', ['user' => $user, 'users' => User::paginate(8)]); 
    }
    
	public function destroy($id)
    {
        User::destroy($id);
		
        return view('users.list', ['users' => User::paginate(8)]);
    }
    
	public function setUserPassword(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $changed = false;
        if ($request->isMethod('post'))
        {
            $this->validate($request, [
                'new_password' => 'required|min:6|confirmed'
            ]);
            $user->password = bcrypt($request->input('new_password'));
            $user->save();
            $changed = true;
        }
        return view('users.set-password', ['user' => $user, 'changed' => $changed]);
    }
    
	public function profile(Request $request)
    {
        if ($request->isMethod('post'))
        {
            $user = Auth::user();
            $this->validate($request, [
                'name' => 'required|max:255',
                'email' => 'required|email' . ($request->input('email') === Auth::user()->email)?'':'|unique:users',
                'image' => 'image',
                'phone' => 'required|max:255' . ($user->phone === $request->input('phone')?'':'|unique:users')
            ]);
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->phone = $request->input('phone');
            $file = $request->file('image');
            if (isset($file))
            {
                $imagePath = base_path() . '/public/uploads/images/';
                $imageName = $user->id . '_' . $file->getClientOriginalName();
                $file->move($imagePath, $imageName);
                $user->image = $imageName;
            }
            $user->save();
            return view('users.profile', ['user' => $user]);
        }
        else {
            return view('users.profile');
        }
    }
    
	public function setPassword(Request $request)
    {
        if ($request->isMethod('post'))
        {
            $this->validate($request, [
                'current_password' => 'required|min:6',
                'new_password' => 'required|min:6|different:current_password|confirmed'
            ]);
            $user = Auth::user();
            if (Hash::check($request->input('current_password'), $user->password))
            {
                $user->password = bcrypt($request->input('new_password'));
                $user->save();
                return view('users.profile', ['user' => $user]);
            }
        }
        else {
            return view('users.set-password');
		}
    }
}
