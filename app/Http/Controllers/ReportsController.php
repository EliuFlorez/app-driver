<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Road;
use App\Http\Requests;
use App\User;
use Carbon\Carbon;
use PDF;
use Auth;

class ReportsController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        if ($user->role != 'A')
            $users = User::where('role', 'T')->paginate(10);
        else 
            $users = User::where('cooperative_id', $user->cooperative_id)->where('role', 'T')->paginate(10);
        return view('reports.report-form', ['users' => $users]);
    }
    
	public function export(Request $request)
    {
        set_time_limit(0);
		
        $this->validate($request, [
            'from' => 'required|date',
            'to' => 'required|date',
            'filter' => 'required',
            'users' => 'required_if:filter,U'
        ]);
        
		$filter = $request->input('filter');
        $from = Carbon::createFromFormat('m/d/Y', $request->input('from'));
        $to = Carbon::createFromFormat('m/d/Y', $request->input('to'));
        $array = $request->input('users');
        
		if ($filter === 'U') {
            $roads = Road::where('created_at', '>', $from)->where('created_at', '<', $to)->whereIn('driver_id', $array)->get();
        } else {
            $user = $request->user();
            if ($user->role != 'A') {
                $roads = Road::where('created_at', '>', $from)->where('created_at', '<', 
                $to)->get();
            } else {
                $drivers = User::where('role', 'T')->where('cooperative_id', $user->cooperative_id)->get();
                $ids = array();
                foreach ($drivers as $driver) {
                    array_push($ids, $driver->id);
				}
                $roads = Road::where('created_at', '>', $from)->where('created_at', '<', $to)
					->where(function ($query) use($ids, $user) {
						$query->whereIn('driver_id', $ids)->orWhere('cooperative_id', $user->cooperative_id);
					})->get();
            }
        }
		
        $pdf = PDF::loadView('reports.pdf', ['roads' => $roads,'from' => $from, 'to' => $to]);
        
		return $pdf->stream();
    }
    
	public function search(Request $request)
    {
        set_time_limit(0);
		
        $this->validate($request, [
            'from' => 'required|date',
            'to' => 'required|date',
            'filter' => 'required',
            'users' => 'required_if:filter,U'
        ]);
        
		$filter = $request->input('filter');
        $from = Carbon::createFromFormat('m/d/Y', $request->input('from'));
        $to = Carbon::createFromFormat('m/d/Y', $request->input('to'));
        $users = User::where('role', 'T')->paginate(10);
        $array = $request->input('users');
        
		if ($filter === 'U') {
            $roads = Road::where('created_at', '>', $from)->where('created_at', '<', $to)->whereIn('driver_id', $array)->get();
            $general = false;
        } else {
            $user = $request->user();
            if ($user->role != 'A') {
                $roads = Road::where('created_at', '>', $from)->where('created_at', '<', 
                $to)->get();
            } else {
                $drivers = User::where('role', 'T')->where('cooperative_id', $user->cooperative_id)->get();
                $ids = array();
                foreach ($drivers as $driver) {
                    array_push($ids, $driver->id);
				}
                $roads = Road::where('created_at', '>', $from)->where('created_at', '<', $to)
					->where(function ($query) use($ids, $user) {
						$query->whereIn('driver_id', $ids)->orWhere('cooperative_id', $user->cooperative_id);
					})->get();
            }
            $array = [];
            $general = true;
        }
		
        return view('reports.report-form', [
			'roads' => $roads, 
			'users' => $users, 
			'general' => $general, 
			'from' => $from, 
			'to' => $to, 
			'array' => $array
		]);
    }
}
