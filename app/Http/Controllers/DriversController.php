<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use App\User;
use App\Cooperative;
use Auth;

class DriversController extends Controller
{

    public function index()
    {
        $user = Auth::user();
        
		if ($user->role === 'A') {
            $users = User::orderBy('name', 'asc')->where('cooperative_id', $user->cooperative_id)->where('role', 'T')->paginate(8);
        } else {
            $users = User::orderBy('name', 'asc')->where('role', 'T')->paginate(8);
		}
		
        return view('drivers.list', ['users' => $users]);
    }
    
	public function search(Request $request)
    {
        $search = $request->input('search');
		
        $user = Auth::user();
        
		if ($user->role === 'A') {
            $users = User::where('cooperative_id', $user->cooperative_id)->where('role', 'T')->where('name', 'like', "%$search%")->paginate(8);
        } else {
            $users = User::where('role', 'T')->where('name', 'like', "%$search%")->paginate(8);
		}
		
        return view('drivers.list', ['users' => $users]);
    }
    
	public function searchDrivers(Request $request)
    {
        $search = $request->input('search');
		
        $users = User::where('name', 'like', "%$search%")->where('role', 'T')->where('availability', 'L')->get();
		
        return response()->json($users);
    }
    
	public function create()
    {
        $user = Auth::user();
        
		if ($user->role != 'A') {
            $cooperatives = Cooperative::orderBy('name', 'asc')->get();
        } else {
            $cooperatives = Cooperative::orderBy('name', 'asc')->where('id', $user->cooperative_id)->get();
        }
		
		return view('drivers.create', ['cooperatives' => $cooperatives]);
    }
    
	public function store(Request $request)
    {
        $this->validate($request, [
            'dni' => 'required|max:255',
            'rut' => 'required|max:255',
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'image' => 'image',
			'address' => 'required',
			'phone' => 'required',
            'cell' => 'required',
			'number_bps' => 'required',
			'license_cta' => 'required',
			'license_vto' => 'required',
            'cooperative' => 'required|exists:cooperatives,id'
        ]);
		
        $user = User::create([
            'dni' => $request->input('dni'),
            'rut' => $request->input('rut'),
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
            'address' => $request->input('address'),
			'phone' => $request->input('phone'),
            'cell' => $request->input('cell'),
			'number_bps' => $request->input('number_bps'),
			'license_cta' => $request->input('license_cta'),
			'license_vto' => $request->input('license_vto'),
            'cooperative_id' => $request->input('cooperative'),
			'role' => 'T',
            'status' => true,
        ]);
        
		$file = $request->file('image');
        if (isset($file)) {
            $imagePath = base_path() . '/public/uploads/images/';
            $imageName = $user->id . '_' . $file->getClientOriginalName();
            $file->move($imagePath, $imageName);
            $user->image = $imageName;
            $user->save();
        }
		
		$filePermission = $request->file('file_permission');
		if (isset($filePermission)) {
			$filePath = base_path() . '/public/uploads/permissions/';
			$fileName = $user->id . '_' . $filePermission->getClientOriginalName();
			$filePermission->move($filePath, $fileName);
			$user->file_permission = $fileName;
			$user->save();
		}
        
		$user = Auth::user();
        
		if ($user->role === 'A') {
            $users = User::orderBy('name', 'asc')->where('cooperative_id', $user->cooperative_id)->where('role', 'T')->paginate(8);
        } else {
            $users = User::orderBy('name', 'asc')->where('role', 'T')->paginate(8);
		}
        
		return view('drivers.list', ['user' => $user, 'users' => $users]);
    }
    
	public function show($id)
    {
        $user = Auth::user();
        $user_edit = User::where('role', 'T')->findOrFail($id);

		if ($user->role != 'A') {
			$cooperatives = Cooperative::orderBy('name', 'asc')->get();
		} else {
			$cooperatives = Cooperative::orderBy('name', 'asc')->where('id', $user->cooperative_id)->get();
		}
		return view('drivers.edit', ['user' =>$user_edit , 'cooperatives' => $cooperatives]);
    }
    
	public function edit($id)
    {
        $user = Auth::user();
        $user_edit = User::where('role', 'T')->findOrFail($id);

		if ($user->role != 'A') {
			$cooperatives = Cooperative::orderBy('name', 'asc')->get();
		} else {
			$cooperatives = Cooperative::orderBy('name', 'asc')->where('id', $user->cooperative_id)->get();
		}
		return view('drivers.edit', ['user' => $user_edit , 'cooperatives' => $cooperatives]);
    }
    
	public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
		
        $this->validate($request, [
			'dni' => 'required|max:255',
            'rut' => 'required|max:255',
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users,email,' . $id,
            'image' => 'image',
			'address' => 'required',
			'phone' => 'required|max:255' . ($user->phone === $request->input('phone') ? '' : '|unique:users'),
            'cell' => 'required',
			'number_bps' => 'required',
			'license_cta' => 'required',
			'license_vto' => 'required',
            'cooperative' => 'required|exists:cooperatives,id'
        ]);
		
        $user->dni = $request->input('dni');
        $user->rut = $request->input('rut');
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->address = $request->input('address');
        $user->phone = $request->input('phone');
        $user->cell = $request->input('cell');
        $user->number_bps = $request->input('number_bps');
		$user->license_cta = $request->input('license_cta');
		$user->license_vto = $request->input('license_vto');
        $user->cooperative_id = $request->input('cooperative');
		
        $file = $request->file('image');
		if (isset($file)) {
            $imagePath = base_path() . '/public/uploads/images/';
            $imageName = $user->id . '_' . $file->getClientOriginalName();
            $file->move($imagePath, $imageName);
            $user->image = $imageName;
        }
		
		$filePermission = $request->file('file_permission');
		if (isset($filePermission)) {
			$filePath = base_path() . '/public/uploads/permissions/';
			$fileName = $user->id . '_' . $filePermission->getClientOriginalName();
			$filePermission->move($filePath, $fileName);
			$user->file_permission = $fileName;
		}
        
		$user->save();
        $user = Auth::user();
        
		if ($user->role === 'A') {
            $users = User::orderBy('name', 'asc')->where('cooperative_id', $user->cooperative_id)->where('role', 'T')->paginate(8);
        } else {
            $users = User::orderBy('name', 'asc')->where('role', 'T')->paginate(8);
		}
		
        return view('drivers.list', ['user' => $user, 'users' =>$users]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);
		
        return view('users.list', ['users' => User::paginate(8)]);
    }
}
