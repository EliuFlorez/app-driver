<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\Client;
use App\Http\Requests;
use App\User;
use App\Road;
use Hash;
use Carbon\Carbon;
use Validator;
use Auth;

class UsersApiController extends Controller
{
	use IssueTokenTrait;
	
	private $client;

	public function __construct()
	{
		$this->client = Client::find(2);
	}
	
	public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|min:6',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => true, 'messages' => $validator->errors()]);
        } else {
            return $this->issueToken($request, 'password');
        }
    }
	
	public function refresh(Request $request)
	{
		$this->validate($request, [
			'refresh_token' => 'required'
		]);

		return $this->issueToken($request, 'refresh_token');
	}

	public function logout(Request $request)
	{
		$accessToken = Auth::user()->token();

		DB::table('oauth_refresh_tokens')
			->where('access_token_id', $accessToken->id)
			->update(['revoked' => true]);

		$accessToken->revoke();

		return response()->json([], 204);
	}
	
    public function index()
    {
		$user = Auth::user();
		
		$road = Road::orderBy('created_at', 'desc')
			->with('user', 'driver')
			->whereIn('status', ['R', 'E'])
			->where('driver_id', $user->id)
			->first();
		
		return response()->json(['error' => false, 'user' => $user, 'roads' => $road]);
    }
    
	public function update(Request $request)
    {
        $user = Auth::user();
		
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'phone' => 'required|max:255' . ($user->phone === $request->input('phone')?'':'|unique:users'),
        ]);
        
		if ($validator->fails()) {
            return response()->json(['error' => true, 'messages' => $validator->errors()]);
        } else {
            $user->name = $request->input('name');
            $user->phone = $request->input('phone');
            $user->save();
            return response()->json(['error' => false, 'user' => $user]);
        }
    }
    
	public function destroy()
    {
        $user = Auth::user();
        
		if ($user) {
			$user->delete();
			$success = true;
		} else {
			$success = false;
		}
		
        
		return response()->json(['success' => $success]);
    }
	
	public function updatePosition(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'latitude' => 'required|numeric',
            'longitude' => 'required|numeric'
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => true, 'messages' => $validator->errors()]);
        } else {
            $user = User::findOrFail($id);
            $user->latitude = $request->input('latitude');
            $user->longitude = $request->input('longitude');
            $user->save();
            return response()->json(['error' => false, 'user' => $user]);
        }
    }
    
	public function getHistory(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'from' => 'required|date',
            'to' => 'required|date'
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => true, 'messages' => $validator->errors()]);
		} else {
            $ini = Carbon::createFromFormat('Y-m-d H:i:s', $request->input('from') . ' 00:00:00');
            $fin = Carbon::createFromFormat('Y-m-d H:i:s', $request->input('to') . ' 23:59:59');
            
			$roads = Road::with('user')
				->where('driver_id', $id)
				->where('created_at', '>', $ini)
				->where('created_at', '<=', $fin)
				->get();
				
            return response()->json(['error' => false, 'roads' => $roads]);
        }
    }
    
	public function getNearestDrivers(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'latitude' => 'required|numeric',
            'longitude' => 'required|numeric'
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => true, 'messages' => $validator->errors()]);
        } else {
            $drivers = User::where('role', 'T')->where('availability', 'L')->get();
            $items = array();
            $limit = 0.5;
            $done = false;
            while($done == false)
            {
                foreach ($drivers as $driver)
                {
                    if ($driver->latitude != null && $driver->longitude != null)
                    {
                        $distance = $this->distance($request->input('latitude'), $request->input('longitude'), 
                        $driver->latitude, $driver->longitude);
                        if ($distance <= $limit && !in_array($driver, $items)) {
                            $items[] = $driver;
						}
                    }
                }
                if (count($items) > 3 || $limit == 5) {
                    $done = true;
                } else {
                    $limit = $limit + 0.5;
				}
            }
            return response()->json($items);
        }
    }
    
	function distance($lat1, $lon1, $lat2, $lon2) {
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        return ($miles * 1.609344);
    }
    
	public function setPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'current_password' => 'required|min:6',
            'new_password' => 'required|min:6|confirmed|different:current_password'
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => true, 'messages' => $validator->errors()]);
		} else {
            $user = Auth::user();
            if (Hash::check($request->input('current_password'), $user->password))
            {
                $user->password = bcrypt($request->input('new_password'));
                $user->save();
                return response()->json(['error' => false, 'user' => $user]);
            } else {
                return response()->json(['error' => false, 'user' => null]);
			}
        }
    }
    
	public function registerWeb(Request $request)
    {
        $validator = Validator::make($request->all(), [
			'dni' => 'required|max:255',
            'rut' => 'required|max:255',
            'name' => 'required|max:255',
			'email' => 'required|email|max:255|unique:users',
			'address' => 'required',
			'phone' => 'required',
            'cell' => 'required',
			'number_bps' => 'required',
			'license_cta' => 'required',
			'license_vto' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => true, 'messages' => $validator->errors()]);
        } else {
            $user = User::create([
				'dni' => $request->input('dni'),
				'rut' => $request->input('rut'),
                'name' => $request->input('name'),
				'email' => $request->input('email'),
				'password' => bcrypt('123456'),
                'phone' => $request->input('phone'),
				'cell' => $request->input('cell'),
				'number_bps' => $request->input('number_bps'),
				'license_cta' => $request->input('license_cta'),
				'license_vto' => $request->input('license_vto'),
                'role' => 'T',
                'status' => false
            ]);
		
			$filePermission = $request->file('file_permission');
			if (isset($filePermission)) {
				$filePath = base_path() . '/public/uploads/permissions/';
				$fileName = $user->id . '_' . $filePermission->getClientOriginalName();
				$filePermission->move($filePath, $fileName);
				$user->file_permission = $fileName;
				$user->save();
			}
		
			return response()->json(['error' => false, 'user' => $user]);
        }
    }
	
	public function registerDriver(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
			'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'phone' => 'required|max:255|unique:users,phone'
        ]); 
        if ($validator->fails()) {
            return response()->json(['error' => true, 'messages' => $validator->errors()]);
        } else {
            $user = User::create([
                'name' => $request->input('name'),
				'email' => $request->input('email'),
				'password' => bcrypt($request->input('password')),
                'phone' => $request->input('phone'),
                'role' => 'T',
                'status' => false
            ]);
			return response()->json(['error' => false, 'user' => $user]);
        }  
    }
    
	public function registerUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
			'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'phone' => 'required|max:255|unique:users,phone',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => true, 'messages' => $validator->errors()]);
        } else {
            $user = User::create([
                'name' => $request->input('name'),
				'email' => $request->input('email'),
				'password' => bcrypt($request->input('password')),
                'phone' => $request->input('phone'),
				'role' => 'C',
                'status' => true
            ]);
            return response()->json(['error' => false, 'user' => $user]);
        }  
    }
    
	public function registerStatus(Request $request)
    {
        $validator = Validator::make($request->all(), [
			'email' => 'required|email',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => true, 'messages' => $validator->errors()]);
        } else {
            $user = User::where('email', '=', $request->input('email'))->first();
			if (!empty($user)) {
				return response()->json(['status' => true]);
			} else {
				return response()->json(['status' => false]);
			}
        }  
    }
	
	public function getUserRoads($id)
    {
        $roads = Road::orderBy('created_at', 'desc')->where('user_id', $id)->get();
		
        return response()->json($roads);
    }
    
	public function getDriverRoads($id)
    {
        $roads = Road::orderBy('created_at', 'desc')->where('driver_id', $id)->get();
		
        return response()->json($roads);
    }
    
	public function available()
    {
        $user = Auth::user();
        $user->availability = 'L';
        $user->save();
        return response()->json(['status' => 'L']);
    }
    
	public function busy()
    {
        $user = Auth::user();
        $user->availability = 'O';
        $user->save();
        return response()->json(['status' => 'O']);
    }
    
	public function disconnected()
    {
        $user = Auth::user();
        $user->availability = 'D';
        $user->save();
        return response()->json(['status' => 'D']);
    }
    
	public function updateDevice(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'device_id' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => true, 'messages' => $validator->errors()]);
        } else {
            $user = User::findOrFail($id);
            $user->device_id = $request->input('device_id');
            $user->save();
            return response()->json(['error' => false, 'user' => $user]);
        }
    }
}
