<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

class ApiController extends Controller
{
    public function index()
    {
        return view('api.token-generator');
    }
}
