<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Road;
use App\User;
use App\Notification;
use GuzzleHttp\Client;
use App\Jobs\RoadProcess;
use Validator;

class RoadsApiController extends Controller
{
    public function index()
    {
		return response()->json(Road::with('driver')->get());
    }
    
	public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required|exists:users,id',
            'location_latitude' => 'required|numeric',
            'location_longitude' => 'required|numeric',
            'location_address' => 'required|max:255',
            'location_reference' => 'required|max:255',
            'destine_address' => 'required|max:255'
        ]);
		
        if ($validator->fails()) {
            return response()->json([
				'error' => true, 
				'messages' => $validator->errors()
			]);
		} else {
            $arrayDrivers = array();
            $limit = 1;
            $drivers = User::where('role', 'T')->where('availability', 'L')->get();
            foreach ($drivers as $driver) {
                if ($driver->latitude != null && $driver->longitude != null) {
                    if (!in_array($driver, $arrayDrivers)) {
                        $arrayDrivers[] = $driver;
					}
				}
			}
			
            if (count($arrayDrivers) > 0) {
                $price = 0;
                $road = Road::create([
                    'user_id' => $request->input('user_id'),
                    'location_latitude' => $request->input('location_latitude'),
                    'location_longitude' => $request->input('location_longitude'),
                    'location_address' => $request->input('location_address'),
                    'location_reference' => $request->input('location_reference'),
                    'destine_address' => $request->input('destine_address'),
                    'price' => $price,
                    'status' => 'P',
                    'type' => 'N'
                ]);
                
				//RoadProcess::dispatch(Road::with('user')->where('id', $road->id)->first());
                
				return response()->json([
					'error' => false, 
					'road' => $road
				]);
            } else {
                return response()->json([
					'error' => false, 
					'road' => null, 
					'count' => count($arrayDrivers)
				]);
			}
        }
    }
    
	public function show($id)
    {
		return response()->json(Road::with('driver')->findOrFail($id));
    }
    
	private function sendAcceptNotification($road)
    {
        $user = User::findOrFail($road->user_id);
        $client = new Client();
        $res = $client->post(config('app.fcm'), [
            'form_params' => [
                'to' => $user->device_id,
                'data' => json_encode(['source' => 'accepted_roads', 'road' => $road])
            ],
            'headers' => [
                'Authorization' => 'key=' . config('app.fcm_key')
            ]
        ]);
    }
    
	public function sendCancelNotification($road, $device)
    {
        $client = new Client();
        $res = $client->post(config('app.fcm'), [
            'form_params' => [
                'to' => $device,
                'data' => json_encode(['source' => 'cancelled_roads', 'road' => $road])
            ],
            'headers' => [
                'Authorization' => 'key=' . config('app.fcm_key')
            ]
        ]);
    }
    
	/* cURL error 6: Could not resolve host: fcm.googleapis.com (see http://curl.haxx.se/libcurl/c/libcurl-errors.html) */
	public function accept(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'driver_id' => 'required|exists:users,id'
        ]);
        if ($validator->fails()) {
            return response()->json([
				'error' => true, 
				'messages' => $validator->errors()
			]);
        } else {
            $driver = User::findOrFail($request->input('driver_id'));
            $road = Road::whereIn('status', ['P', 'RV'])->where('id', $id)->first();
            if (isset($road) && $driver->role != 'C') {
                $driver->availability = 'O';
                $driver->save();
                
				$road->driver_id = $driver->id;
                $road->status = 'RV';
                $road->save();
                
				$road = Road::with('driver')->where('id', $id)->first();
                
				//$this->sendAcceptNotification($road);
                
				return response()->json([
					'error' => false, 
					'road' => $road, 
					'forbidden' => false
				]);
            } else {
                return response()->json([
					'error' => false, 
					'road' => null, 
					'forbidden' => true
				]);
			}
        }
    }
    
	public function notify($id)
    {
        $road = Road::findOrFail($id);
        $device = $road->user->device_id;
        if ($device != null) {
            $client = new Client();
            $res = $client->post(config('app.fcm'), [
                'form_params' => [
                    'to' => $device,
                    'data' => json_encode(['source' => 'notify_roads', 'road' => $road])
                ],
                'headers' => [
                    'Authorization' => 'key=' . config('app.fcm_key')
                ]
            ]);
        }
        return response()->json($road);
    }
    
	public function cancel(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required|exists:users,id'
        ]);
        if ($validator->fails()) {
            return response()->json([
				'error' => true, 
				'messages' => $validator->errors()
			]);
		} else {
            $road = Road::findOrFail($id);
            if ($road->status != 'F' && $road->status != 'E') {
                if ($road->user_id === $request->input('user_id')) {
                    $road->status = 'C';
                    $user = User::find($road->driver_id);
                }      
                else if ($road->driver_id === $request->input('user_id'))
                {
                    $user = User::findOrFail($road->user_id);
                    $road->status = 'P';
                    //RoadProcess::dispatch(Road::with('user')->where('id', $road->id)->first());
                }
                $road->save();
                if (isset($user)) {
                    //$this->sendCancelNotification($road, $user->device_id);
                }
                $driver = User::find($road->driver_id);
                if (isset($driver)) {
                    $driver->availability = 'L';
                    $driver->save();
                }
                return response()->json([
					'error' => false, 
					'road' => $road
				]);
            } else {
                return response()->json([
					'error' => true, 
					'message' => 'No se puede cancelar esta carrera.'
				]);
			}
        }
    }
    
	public function finish(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'price' => 'required|numeric'
        ]);
		
        if ($validator->fails()) {
            return response()->json([
				'error' => true, 
				'road' => null, 
				'messages' => $validator->errors()
			]);
		}
		
        $road = Road::findOrFail($id);
        
		if ($road->status != 'C') {
            $driver = User::find($road->driver_id);
            
			if (isset($driver)) {
                $driver->availability = 'L';
                $driver->save();
            }
            
			$road->status = 'F';
            $road->price = $request->input('price');
            $road->save();
			
            return response()->json([
				'error' => false, 
				'road' => $road, 
				'driver' => $driver
			]);
        } else { 
            return response()->json([
				'error' => true, 
				'road' => null
			]);
		}
    }
    
	public function start($id)
    {
        $road = Road::findOrFail($id);
        if ($road->status == 'R') {
            $road->status = 'E';
            $road->save();
            return response()->json([
				'error' => false, 
				'road' => $road
			]);
        } else { 
            return response()->json([
				'error' => true, 
				'road' => null
			]);
		}
    }
}
