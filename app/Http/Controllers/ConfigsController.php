<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

class ConfigsController extends Controller
{
    public function index()
    {
        return view('config');
    }
	
    public function image(Request $request)
    {
        $this->validate($request, [
            'photo' => 'required|image'
        ]);
		
        $file = $request->file('photo');
        $photoPath = base_path() . '/public/img/';
        $photoName = 'precio.jpg';
        
		$file->move($photoPath, $photoName);
        
		return view('config');
    }
}
