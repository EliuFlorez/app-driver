<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class PermissionMiddleware
{
    public function handle($request, Closure $next)
    {
        if (Auth::user()->role === 'S' || Auth::user()->role === 'A' || Auth::user()->role === 'D') {
            return $next($request);
        } else {
            Auth::logout();
            return redirect('/login');
        }
    }
}
