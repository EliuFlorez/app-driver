<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class BasicAuthMiddleware
{
    public function handle($request, Closure $next)
    {
        if (Auth::user()->role != 'S') {
            Auth::logout();
            abort(403, 'Forbidden');
        }
		
        return $next($request);
    }
}
