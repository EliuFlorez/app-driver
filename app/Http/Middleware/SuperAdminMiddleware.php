<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class SuperAdminMiddleware
{
    public function handle($request, Closure $next)
    {
        if (Auth::user()->role != 'S') {
            Auth::logout();
            return redirect('/login');   
        }
        else {
            return $next($request);
		}
    }
}
