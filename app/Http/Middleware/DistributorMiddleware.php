<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class DistributorMiddleware
{
    public function handle($request, Closure $next)
    {
        if (Auth::user()->role == 'A') {
            Auth::logout();
            return redirect('/login');   
        }
        else {
            return $next($request);
		}
    }
}
