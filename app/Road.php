<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Road extends Model
{
	/**
	 * SoftDeletingTrait
	 */
	use SoftDeletes;

	/**
	 * SoftDeletingTrait
	 *
	 * @var Date
	 */
    protected $dates = [
		'reservation_date',
		'created_at', 
		'deleted_at'
	];
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    protected $table = 'roads';
	
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
    protected $fillable = [
		'user_id', 
		'location_latitude', 
		'location_longitude', 
		'status', 
		'driver_id', 
		'location_address', 
		'location_reference', 
		'price', 
		'destine_address', 
		'route', 
		'type', 
		'reservation_date', 
		'anticipation_send', 
		'cooperative_id', 
		'job_id'
    ];
	
    public function cooperative()
    {
        return $this->belongsTo('App\Cooperative');
    }
    
	public function user()
    {
        return $this->belongsTo('App\User');
    }
    
	public function driver()
    {
        return $this->belongsTo('App\User');
    }
}
