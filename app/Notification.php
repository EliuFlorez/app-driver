<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Notification extends Model
{
	/**
	 * SoftDeletingTrait
	 */
	use SoftDeletes;

	/**
	 * SoftDeletingTrait
	 *
	 * @var Date
	 */
    protected $dates = [
		'deleted_at'
	];
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    protected $table = 'notifications';
	
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
    protected $fillable = [
		'road_id', 
		'user_id'
    ];
}
