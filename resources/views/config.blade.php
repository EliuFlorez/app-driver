@extends('layouts.app')
@section('title')
Configuración del contenido
@endsection
@section('content')
<h3><i class="fa fa-angle-right"></i> Configurar el sitio</h3>
<div class="row mt">
    <div class="col-lg-12">
        <div class="form-panel">
            <h4 class="mb"><i class="fa fa-angle-right"></i> Parámetros</h4>
            <form enctype="multipart/form-data" action="/configs/image" class="form-horizontal style-form" method="POST">
                {{ csrf_field() }}
                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Seleccionar imagen de tarifas</label>
                    <div class="col-sm-5">
                        <input id="image" name="image" type="file" value="{{ old('image') }}" />
                        @if ($errors->has('image'))
                            <span class="help-block"><strong>{{ $errors->first('image') }}</strong></span>
                        @endif
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="/assets/js/custom/config.js"></script>
@endsection