@extends('layouts.app')
@section('title')
    Zonas
@endsection
@section('content')
    <h3><i class="fa fa-angle-right"></i> Lista de zones</h3>
    <div class="row mt">
        <div class="col-lg-12">
            <div class="form-panel">
                <h4 class="mb"><i class="fa fa-angle-right"></i> Consulta</h4>
                <form method="POST" action="{{ url('/zones/search') }}" class="form-inline" role="form">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <input name="search" type="text" class="form-control" id="search">
                    </div>
                    <button type="submit" class="btn btn-theme">Buscar</button>
                </form>
            </div>
        </div>
    </div>
    <div class="row mt">
        <div class="col-lg-12">
            <div class="content-panel">
                <h4><i class="fa fa-angle-right"></i> Resultados</h4>
                @if($zones->count() > 0)
                    <div class="table-responsive">
                        <section id="unseen">
                            <table class="table table-bordered table-striped table-condensed text-center">
                                <thead>
                                <tr>
                                    <th>Editar</th>
                                    <th>Descripcion</th>
                                    <th>Cooperativa</th>
                                    <th>Fecha de creación</th>
                                    <th>Fecha de modificación</th>
                                    <th>Estado</th>
                                    <th>Eliminar</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($zones as $zone)
                                    <tr>
                                        <td><a href="{{ url('/zones/' . $zone->id) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a></td>
                                        <td>{{ $zone->descripcion }}</td>
                                        <td>{{ ($zone->cooperative!=null)?$zone->cooperative->name:""}}</td>
                                        <td>{{ $zone->created_at }}</td>
                                        <td>{{ $zone->updated_at }}</td>
                                        <td>
                                           <div class="col-lg-12">
                                               <div class="switch switch-square"
                                                    data-on-label="<i class=' fa fa-check'></i>"
                                                    data-off-label="<i class='fa fa-times'></i>">
                                                    <input onchange="activateZona('{{ url("/zones/" . $zone->id . '/activate') }}', this.checked);" type="checkbox" {{ ($zone->status == true)?'checked=""':'' }} />
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                          <form onsubmit="return confirm('¿Estás seguro de eliminar esta zone?');" method="POST" action="{{ url('/zones/' . $zone->id) }}">
                                               {{ csrf_field() }}
                                               <input type="hidden" name="_method" value="DELETE" />
                                               <button type="submit" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>
                                          </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{ $zones->links() }}
                        </section>
                    </div>
                @else
                    <div class="alert alert-info">No hubo resultados.</div>
                @endif
            </div>
        </div>
    </div>
@endsection
@section('scripts')
   <script>
       function activateZona(url, status)
       {
           $.post(url, {
               status : (status)?1:0
           }, function (data) {
               if (data.error == true) {
                   alert('Hubo un error al realizar la acción solicitada.');
			   }
           }, 'json');
       }
       document.getElementById('a_zones').classList.add('active');
   </script>
@endsection