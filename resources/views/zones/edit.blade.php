@extends('layouts.app')
@section('title')
    Editar zone
@endsection
@section('content')
    <style>
		#map{min-height:500px;width:100%;height:100%;}
		.controls {margin-top:10px;border:1px solid transparent;border-radius:2px 0 0 2px;box-sizing:border-box;-moz-box-sizing:border-box;height:32px;outline:none;box-shadow:0 2px 6px rgba(0, 0, 0, 0.3);}
		#pac-input {background-color:#fff;font-family:Roboto;font-size:15px;font-weight:300;margin-left:12px;padding:0 11px 0 13px;text-overflow:ellipsis;width:300px;}
		#pac-input:focus {border-color:#4d90fe;}
		.pac-container {font-family:Roboto;}
		#type-selector {color:#fff;background-color:#4d90fe;padding:5px 11px 0px 11px;}
		#type-selector label {font-family:Roboto;font-size:13px;font-weight:300;}
    </style>
    <h3><i class="fa fa-angle-right"></i> Editar zone</h3>
    <div class="row mt">
        <div class="col-lg-12">
            <div class="form-panel">
                <h4 class="mb"><i class="fa fa-angle-right"></i> Información</h4>
                <form enctype="multipart/form-data" class="form-horizontal style-form" method="POST">
                    <form enctype="multipart/form-data"  class="form-horizontal style-form">
                        {{ csrf_field() }}
                        <div class="form-group" id="div-cooperative" >
                            <label class="col-sm-2 col-sm-2 control-label">Cooperativa</label>
                            <div class="col-sm-10">
                                <select class="form-control" id="cooperative_id" name="cooperative_id">
                                    @foreach ($cooperatives as $cooperative_id)
                                        <option value="{{ $cooperative_id->id }}">
                                            {{ $cooperative_id->name }}
                                        </option>
                                    @endforeach
                                </select>
                                <span class="help-block" id="span_cooperative"></span>
                            </div>
                        </div>
                        <div class="form-group" id="div-descripcion">
                            <label class="col-sm-2 col-sm-2 control-label">Descripción</label>
                            <div class="col-sm-10">
                                <input name="descripcion" id="descripcion" type="text" class="form-control" value="{{ $zone->descripcion }}">
                                <span class="help-block" id="span_descripcion"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Zona</label>
                            <div class="col-sm-10">
                                <input id="pac-input" class="controls" type="text" placeholder="Buscar ubicación">
                                <div id="map"></div>
                                <button type="button" onclick="dibujar();" class="btn-xs btn-primary" id="btn-dibujar"><i class="fa fa-pencil-square-o"></i> Dibujar zone</button>
                                <button type="button" onclick="borrar();" class="btn-xs btn-secundaty" id="btn-borrar"><i class="fa fa-eraser"></i> Borrar zone</button>
                            </div>
                        </div>
                    </form>
                </form>
                <div class="modal-footer">
                    <button type="button" onclick="editar();" class="btn btn-primary"><i class="fa fa-save"></i> Guardar cambios</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

    <script>

        @if(isset($zone->cooperative_id))
            document.getElementById('cooperative_id').value="{{ $zone->cooperative_id }}";
        @endif

        var markers = [];
        var line;
        var map;
        var array_zone = [];

        function editar()
        {
            var cooperative_id = document.getElementById('cooperative_id');
            var descripcion = document.getElementById('descripcion');
            var url = '{{url('/zones/'.$zone->id)}}';
            var div_descripcion = document.getElementById('div-descripcion');
            var span_descripcion = document.getElementById('span_descripcion');
            var div_cooperative = document.getElementById('div-cooperative');
            var span_cooperative = document.getElementById('span_cooperative');

            if (array_zone.length > 0) {
                $.post(url, {
                    cooperative_id: cooperative_id.value,
                    descripcion: descripcion.value,
                    zone: array_zone,
                    _method : 'PUT'
                } ,function( data ) {
                    if (data.error == true) {
                        if (data.messages.hasOwnProperty('descripcion')){
                            div_descripcion.classList.add('has-error');
                            span_descripcion.innerHTML = '<strong>' + data.messages.descripcion + '</strong>';
                        }
                        if (data.messages.hasOwnProperty('cooperative_id')){
                            div_cooperative.classList.add('has-error');
                            span_cooperative.innerHTML = '<strong>' + data.messages.cooperative_id + '</strong>';
                        }
                    } else {
                        window.location.replace('{{url('/zones')}}');
                    }
                }, "json");
            } else {
                alert('No se encontró ninguna zone dibuja.');
            }
        }

        function dibujar()
        {
            var path = [];
            array_zone = [];
            line.setMap(map);
            for(var i = 0;i < markers.length;i++) {
                path.push({lat:markers[i].position.lat(),lng:markers[i].position.lng()});
                array_zone.push({"lat":markers[i].position.lat(),"lgn":markers[i].position.lng()});
            }

            if (path.length < 3) {
                alert("Para poder dibujar la zone debe seleccionar mínimo tres puntos en el mapa.");
                //line.setMap(null);
            } else {
                generarZona(path);
            }
        }

        function borrar()
        {
            array_zone = [];
            markers.forEach(function(marker) {
                marker.setMap(null);
            });
            markers = [];
            line.setMap(null);
        }

        function generarZona(path)
        {
            line.setPath(path);
            line.setMap(map);
            map.setCenter(path[0]);
            map.setZoom(14);
        }

        function setZonaArea()
        {
            var array_aux = [];
            var array_zone_actual = [];
            array_zone = [];
            @foreach($zone->zone as $punto)
                @foreach($punto as $p)
                    array_aux.push('{{$p}}');
                @endforeach
                array_zone_actual.push({lat:parseFloat(array_aux[0]),lng:parseFloat(array_aux[1])});
                array_zone.push({'lat':parseFloat(array_aux[0]),'lng':parseFloat(array_aux[1])});
                array_aux = [];
            @endforeach

            generarZona(array_zone_actual);
        }
        function initAutocomplete() {
            map = new google.maps.Map(document.getElementById('map'), {
                center: { lat : -33.4309221, lng : -55.7682438 },
                zoom: 7,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            line = new google.maps.Polygon({
                strokeColor: '#f69f18',
                strokeOpacity: 0.8,
                strokeWeight: 3,
                fillColor: '#f69f18',
                fillOpacity: 0.35,
                map:map
            });
            line.setMap(map);

            setZonaArea();

            // Create the search box and link it to the UI element.
            var input = document.getElementById('pac-input');
            var searchBox = new google.maps.places.SearchBox(input);
            var btn_dibujar = document.getElementById('btn-dibujar');
            var btn_borrar = document.getElementById('btn-borrar');
            var marker;

            map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
            map.controls[google.maps.ControlPosition.LEFT_CENTER].push(btn_dibujar);
            map.controls[google.maps.ControlPosition.LEFT_CENTER].push(btn_borrar);

            // Bias the SearchBox results towards current map's viewport.
            map.addListener('bounds_changed', function() {
                searchBox.setBounds(map.getBounds());
            });

            var ubicacion_actual = null;
            // Listen for the event fired when the user selects a prediction and retrieve
            // more details for that place.
            searchBox.addListener('places_changed', function() {
                var places = searchBox.getPlaces();

                if (places.length == 0) {
                    return;
                }
                // For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function(place) {
                    var icon = {
                        url: place.icon,
                        size: new google.maps.Size(71, 71),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(17, 34),
                        scaledSize: new google.maps.Size(25, 25)
                    };

                    // Create a marker for each place.
                    /*  markers.push(new google.maps.Marker({
                     map: map,
                     icon: icon,
                     title: place.name,
                     position: place.geometry.location
                     }));*/

                    map.setCenter(place.geometry.location);
                    map.setZoom(14);

                    ubicacion_actual=markers[markers.length-1].position;

                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                });
                map.fitBounds(bounds);
            });

            map.addListener('click', function(e){
                marker = new google.maps.Marker({
                    map : map
                });
                var latLng = e.latLng;
                marker.setPosition(latLng);
                map.setCenter(latLng);
                markers.push(marker);
            });
        }
        document.getElementById('a_zones').classList.add('active');
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB249EeF8ARkbmlsjDDezz-gm3cLG3VO7M&libraries=places&callback=initAutocomplete" async defer></script>
@endsection
