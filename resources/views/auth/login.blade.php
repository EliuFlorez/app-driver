@extends('layouts.single')
@section('content')
<div id="login-page">
    <div class="container">
        <form method="POST" class="form-login" action="{{ url('/login') }}">
            {{ csrf_field() }}
            <h2 class="form-login-heading"><img class="login-logo" src="{{ asset('/assets/img/logo.jpg') }}" /></h2>
            <div class="login-wrap">
                <input name="email" type="text" class="form-control" placeholder="Nombre de usuario" autofocus>
                @if ($errors->has('email'))
                    <span class="help-block">* {{ $errors->first('email') }}</span>
                @endif
                <br>
                <input name="password" type="password" class="form-control{{ $errors->has('password') ? ' has-error' : '' }}" placeholder="Contraseña">
                @if ($errors->has('password'))
                    <span class="help-block">* {{ $errors->first('password') }}</span>
                @endif
                <br/>
                <button class="btn btn-theme btn-block" type="submit"><i class="fa fa-lock"></i> INICIAR SESIÓN</button>
                <hr>
				<div class="form-group">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="remember"> Recordarme
                        </label>
						<span class="pull-right">
							<a href="{{ url('/password/reset') }}"> Olvidaste tu contraseña?</a>
						</span>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
