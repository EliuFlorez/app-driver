@if (App\User::where('role', 'S')->get()->count() == 0)
@extends('layouts.single')
@section('content')
<div id="login-page">
    <div class="container">
        <form method="POST" class="form-login" action="{{ url('/register') }}">
            {{ csrf_field() }}
            <h2 class="form-login-heading"><a href="/login"><img class="login-logo" src="{{ asset('/assets/img/logo.jpg" /></a> Registro de cuenta</h2>
            <div class="login-wrap">
                <form role="form" method="POST" action="{{ url('/register') }}">
                    {{ csrf_field() }}
                    <input placeholder="Nombre" id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
                    @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                    <br/>
                    <input placeholder="Email" id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                    <br/>
                    <input placeholder="Contraseña" id="password" type="password" class="form-control" name="password" required>
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                    <br/>
                    <input placeholder="Confirmación de contraseña" id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                    @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                    @endif
                    <br/>
                    <button type="submit" class="btn btn-theme btn-block">
                        Registrarme
                    </button>
                    <hr/>
                </form>
            </div>
        </form>	  	 
    </div>
</div>
@endsection
@else
    <div class="content-panel">
        <h1><strong>Forbidden 403</strong></h1>
    </div>
@endif