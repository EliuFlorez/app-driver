@extends('layouts.single')
@section('content')
<div id="login-page">
    <div class="container">
        <form class="form-login" role="form" method="POST" action="{{ url('/password/email') }}">
            <h2 class="form-login-heading"><a href="{{ url('/login') }}"><img class="login-logo" src="{{ url('/assets/img/logo.jpg') }}" /></a> Recuperar cuenta</h2>
            {{ csrf_field() }}
            <div class="login-wrap">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                    <br/>
                @endif
                <input placeholder="Email" id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
                <br/>
                <button type="submit" class="btn btn-primary">
                    Enviar link a mi correo
                </button>
                <hr/>
            </div> 
        </form>
    </div>
</div>
@endsection
