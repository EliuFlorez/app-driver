@extends('layouts.single')
@section('content')
<div id="login-page">
    <div class="container">
        <form class="form-login" role="form" method="POST" action="{{ url('/password/reset') }}">
            {{ csrf_field() }}
            <h2 class="form-login-heading"><a href="{{ url('/login') }}"><img class="login-logo" src="{{ url('/assets/img/logo.jpg') }}" /></a> Recuperar cuenta</h2>
            <div class="login-wrap">
                <input type="hidden" name="token" value="{{ $token }}">
                <input placeholder="Email" id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}" required autofocus>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
                <br/>
                <input placeholder="Contraseña" id="password" type="password" class="form-control" name="password" required>
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
                <br/>
                <input placeholder="Confirmación de contraseña" id="password-confirm" type="password" class="form-control" name="password_confirmation" required>

                @if ($errors->has('password_confirmation'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                @endif
                <br/>
                <button type="submit" class="btn btn-primary">
                    Recuperar contraseña
                </button>
                <hr/>
            </div>
        </form>
    </div>
</div>
@endsection
