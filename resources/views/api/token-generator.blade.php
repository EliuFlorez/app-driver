@extends('layouts.app')
@section('title')
API
@endsection
@section('content')
<h3><i class="fa fa-angle-right"></i> Crear usuario</h3>
<div  id="app" class="row mt">
    <div class="col-lg-12">
        <passport-clients></passport-clients>
        <passport-authorized-clients></passport-authorized-clients>
        <passport-personal-access-tokens></passport-personal-access-tokens>
    </div>
</div>
@endsection
@section('scripts')
<script src="/js/app.js"></script>
@endsection