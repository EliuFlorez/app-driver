@extends('layouts.app')
@section('title')
Usuarios
@endsection
@section('content')
<h3><i class="fa fa-angle-right"></i> Lista de usuarios</h3>
<div class="row mt">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="col-lg-4">
            <div class="form-panel">
                <h4 class="mb"><i class="fa fa-angle-right"></i> Consulta</h4>
                <form method="GET" action="{{ url('/users/search') }}" role="form">
                    <div class="form-group">
                        <input name="search" type="text" class="form-control" id="search" placeholder="Nombre/Email">
                    </div>
                    <h4 class="mb"><i class="fa fa-angle-right"></i> Estado</h4>
                    <div class="form-group">
                        <div class="radio">
                            <label><input onclick="this.form.submit();" type="radio" name="status" value="A" {{ (isset($status) && $status === 'A') ? 'checked' : '' }} /> Activos</label>
                        </div>
                        <div class="radio">
                            <label><input onclick="this.form.submit();" type="radio" name="status" value="I" {{ (isset($status) && $status === 'I') ? 'checked' : '' }} /> Inactivos</label>
                        </div>
                        <div class="radio">
                            <label><input onclick="this.form.submit();" type="radio" name="status" value="T" {{ ((isset($status) && $status === 'T') || !isset($status)) ? 'checked' : '' }} /> Todos</label>
                        </div>
                    </div>
                    <h4 class="mb"><i class="fa fa-angle-right"></i> Roles</h4>
                    <div class="form-group">
                        @if (Auth::user()->role === 'S')
                            <div class="radio">
                                <label><input onclick="this.form.submit();" type="radio" name="role" value="S" {{ ((isset($role) && $role === 'S') || !isset($role))? 'checked' : '' }} /> Administradores</label>
                            </div>
                        @endif
                        @if (Auth::user()->role != 'A')
                            <div class="radio">
                                <label><input onclick="this.form.submit();" type="radio" name="role" value="D" {{ ((isset($role) && $role === 'D') || !isset($role)) ? 'checked' : '' }} /> Despachadores</label>
                            </div>
                        @endif
                        <div class="radio">
                            <label><input onclick="this.form.submit();" type="radio" name="role" value="A" {{ ((isset($role) && $role === 'A') || !isset($role)) ? 'checked' : '' }} /> Administradores de cooperative</label>
                        </div>
                        @if (Auth::user()->role != 'A')
                            <div class="radio">
                                <label><input onclick="this.form.submit();" type="radio" name="role" value="C" {{ (((isset($role) && $role === 'C') || !isset($role)) || !isset($role)) ? 'checked' : '' }} /> Cualquiera</label>
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Se encontró:</label> {{ $users->count() }} resultado(s)<br/>
                        @if (Auth::user()->role === 'S')
                            <label>Total de Administradores:</label> {{ App\User::whereIn('role', ['S'])->get()->count() }} <br/>
                        @endif
                        @if (Auth::user()->role != 'A')
                            <label>Total de despachadores:</label> {{ App\User::whereIn('role', ['D'])->get()->count() }} <br/>
                            <label>Total de administradores:</label> {{ App\User::whereIn('role', ['A'])->get()->count() }} <br/>
                        @endif
                    </div>
                    <button type="submit" class="btn btn-theme">Buscar</button>
                </form>
            </div>
        </div>
        <div class="col-lg-8">
            <div class="content-panel">
                @if($users->count() > 0)
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-condensed text-center">
                            <th>Editar</th>
                            <th>Cambiar contraseña</th>
                            <th>Cooperativa</th>
                            <th>Nombre</th>
                            <th>Correo electrónico</th>
                            <th>Rol</th>
                            <th>Estado</th>
                            <th>Eliminar</th>
                            @foreach ($users as $user)
                                <tr>
                                    <td><a href="{{ url('/users/' . $user->id) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a></td>
                                    <td><a href="{{ url('/users/' . $user->id) }}/password" class="btn btn-warning"><i class="fa fa-lock"></i></a></td>
                                    <td>{{ ($user->cooperative == null) ? '-' : $user->cooperative->name }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ ($user->role == 'S')?'SuperAdmin':(($user->role == 'A')?'Admin':(($user->role == 'T')?'Conductor':(($user->role === 'D')?'Distribuidor':'Cliente'))) }}</td>
                                    <td>
                                        <div class="col-lg-12">
                                            <div class="switch switch-square"
                                                data-on-label="<i class=' fa fa-check'></i>"
                                                data-off-label="<i class='fa fa-times'></i>">
                                                <input onchange="activateUser('{{ url("/users/" . $user->id . '/activate') }}', this.checked);" type="checkbox" {{ ($user->status == true)?'checked=""':'' }} />
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <form onsubmit="return confirm('¿Estás seguro de eliminar este usuario?');" method="POST" action="{{ url('/users/' . $user->id) }}">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE" />
                                            <button type="submit" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        {{ $users->links() }}
                    </div>
                @else
                    <div class="alert alert-info">No hubo resultados.</div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="{{ asset('/assets/js/custom/users.js') }}"></script>
<script>
    document.getElementById('a_users').classList.add('active');
</script>
@endsection