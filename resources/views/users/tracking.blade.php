@extends('layouts.app')
@section('title')
Seguimiento
@endsection
@section('content')
<h3><i class="fa fa-angle-right"></i> Seguimiento de usuarios</h3>
<div class="row mt">
    <div class="col-lg-4 col-md-6 col-sm-12">
        <div class="form-panel">
        <h4 class="mb"><i class="fa fa-angle-right"></i> Filtros de consulta</h4>
            <div class="form-group">
                <strong><label>Cantidad de autos: </label></strong> {{ $users->count() }}
            </div>
            <form id="form" class="form-horizontal style-form" method="POST">
                {{ csrf_field() }}
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Ver</label>
                    <div class="col-sm-10">
						<div class="radio">
							<label><input onclick="this.form.submit();" type="radio" name="filter" id="optionAvailable" value="L"> Disponibles</label>
						</div>
						<div class="radio">
							<label><input onclick="this.form.submit();" type="radio" name="filter" id="optionBusy" value="O"> Ocupados</label>
						</div>
						<div class="radio">
							<label><input onclick="this.form.submit();" type="radio" name="filter" id="optionDisconnected" value="D"> Desconectados</label>
						</div>
						<div class="radio">
							<label><input onclick="this.form.submit();" type="radio" name="filter" id="optionAll" value="T" checked> Todos</label>
						</div>
                    </div>
                </div>
                @if (isset($filter))
                    <script>
                        var options = document.getElementsByName('filter');
                        for (var i = 0; i < options.length; i++) {
                            options[i].checked = false;
                            if (options[i].value == '{{ $filter }}') {
                                options[i].checked = true;
							}
                        }
                    </script>
                @endif
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Nombre</label>
                    <div class="col-sm-10">
                        <input id="search" name="search" type="text" class="form-control" />
                    </div>
                </div>
                @if (isset($search))
                    <script>
                        document.getElementById('search').value = '{{ $search }}';
                    </script>
                @endif
                <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Buscar</button>
                <h4 class="mb"><i class="fa fa-angle-right"></i> Usuarios</h4>
                <div class="form-group">
                    @if ($users->count() > 0)
                        <div class="col-lg-12">
                            <ul style="overflow-y: scroll; height:300px;" class="list-group">
                                @foreach ($users as $user)
                                    <li class="list-group-item">  <button onclick="locateMarker('{{ $user->id }}');" type="button" class="btn btn-primary"><i class="fa fa-eye"></i></button> {{ $user->name . ' (#' . $user->vehicle_number . ')' }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @else 
                        <div class="alert alert-info">No hay usuarios disponibles.</div>
                    @endif
                </div>
            </form>
        </div>
    </div>
    <div class="col-lg-8 col-md-6 col-sm-12">
        <div class="content-panel">
            <div class="col-lg-12">
                <div class="form-group">
                    <input type="text" name="address" id="address" class="form-control" placeholder="Consultar por lugar..." onkeyup="geocodeAddress(map, this.value);" />
                </div>
            </div>
            <h4 class="mb"><i class="fa fa-angle-right"></i> Mapa</h4>
            <div class="tracking-map" id="map"></div>
            <h4 class="mb"><i class="fa fa-angle-right"></i> Significado de los colores</h4>
            <ul>
                <li style="color:#40B300;"><i class="fa fa-cab"></i> Disponible</li>
                <li style="color:#FFB300;"><i class="fa fa-cab"></i> Ocupado</li>
                <li style="color:#FF0000;"><i class="fa fa-cab"></i> Desconectado</li>
            </ul>
        </div>
    </div>
</div>
@endsection
@section('scripts')   
    <script src="{{ asset('/assets/js/custom/users-tracking.js') }}"></script>
    <script>
		var urlBase = '<?php echo url('/'); ?>';
		function locateMarker(id)
		{
			$.get(url+'/users/'+id+'/get', function (data) {
				map.setCenter({
					lat : parseFloat(data.latitude), 
					lng : parseFloat(data.longitude)
				});
				map.setZoom(18);
			}, 'json');
		}
        function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                center: gye,
                zoom: 7
            });
            geocoder = new google.maps.Geocoder();
            @if ($users->count() > 0)
                @foreach ($users as $user)
                    var availability = '{{ $user->availability }}';
                    var icon = (availability == 'O') ? urlBase+'/assets/img/auto-amarillo.png' : ((availability == 'L') ? urlBase+'/assets/img/auto-verde.png' : urlBase+'/assets/img/auto-rojo.png');
                    var image = '{{ ($user->image == null) ? url('assets/img/user.png') : url('/uploads/images/' . $user->image) }}';
                    var status = (availability == 'O') ? 'Ocupado' : ((availability == 'L') ? 'Disponible' : 'Desconectado(a)');
                    var url = '{{ url('/users/'.$user->id.'/get') }}';
                    addMarker({
                        lat : parseFloat('{{ $user->latitude }}'),
                        lng : parseFloat('{{ $user->longitude }}')
                    }, 
					'{{ $user->id }}', 
					userInformationToHtml('{{ $user->id }}', '{{ $user->name }}', image, status), icon, url);
                @endforeach
            @else 
                alert('No hay usuarios disponibles para hacer seguimiento.');
            @endif
        }
        document.getElementById('a_drivers').classList.add('active');
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBsBqDu5Zh1inkcAhb9MHJOFQm6W4rwIe8&callback=initMap" async defer></script>
@endsection