@extends('layouts.app')
@section('title')
Editar usuario
@endsection
@section('content')
<h3><i class="fa fa-angle-right"></i> Editar usuario</h3>
<div class="row mt">
    <div class="col-lg-12">
        <div class="form-panel">
            <h4 class="mb"><i class="fa fa-angle-right"></i> Información</h4>
            <form enctype="multipart/form-data" class="form-horizontal style-form" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="PUT" />
                <div class="form-group{{ $errors->has('name')?' has-error':'' }}">
                    <label class="col-sm-2 col-sm-2 control-label">Nombres y apellidos</label>
                    <div class="col-sm-10">
                        <input name="name" type="text" class="form-control" value="{{ $user->name }}">
                        @if ($errors->has('name'))
                            <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Foto actual/seleccionada</label>
                    <div class="col-sm-10">
                        <img id="image" class="profile-pic" src="{{ ($user->image == null || filter_var($user->image, FILTER_VALIDATE_URL)) ? url('assets/img/user.png') : url('/uploads/images/' . $user->image) }}" />
                    </div>
                </div>
                <div class="form-group{{ $errors->has('image')?' has-error':'' }}">
                    <label class="col-sm-2 col-sm-2 control-label">Seleccionar foto de perfil</label>
                    <div class="col-sm-5">
                        <input id="image" type="file" name="image" />
                        @if ($errors->has('image'))
                            <span class="help-block"><strong>{{ $errors->first('image') }}</strong></span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('email')?' has-error':'' }}">
                    <label class="col-sm-2 col-sm-2 control-label">Dirección de correo electrónico</label>
                    <div class="col-sm-10">
                        <input type="email" class="form-control" name="email" value="{{ $user->email }}">
                        @if($errors->has('email'))
                            <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('role')?' has-error':'' }}">
                    <label class="col-sm-2 col-sm-2 control-label">Rol de usuario</label>
                    <div class="col-sm-10">
                        <select name="role" id="role" class="form-control">
                            @if (Auth::user()->role === 'S')
                                <option value="S">Superadmin</option>
                            @endif
                            @if (Auth::user()->role != 'A')
                                <option value="D">Distribuidor</option>
                            @endif
                            <option value="A">Administrador de cooperative</option>
                        </select>
                        @if ($errors->has('role'))
                            <span class="help-block"><strong>{{ $errors->first('role') }}</strong></span>
                        @endif
                    </div>
                </div>
                <script>
                    document.getElementById('role').value = '{{ $user->role }}';
                </script>

                <div id="div_cooperative" class="form-group{{ $errors->has('cooperative')?' has-error':'' }}">
                    <label class="col-sm-2 col-sm-2 control-label">Cooperativa</label>
                    <div class="col-sm-10">
                        <select class="form-control" name="cooperative" id="cooperative">
                            @foreach ($cooperatives as $cooperative)
                                <option value="{{ $cooperative->id }}" {{ ($cooperative->id === $user->cooperative_id)?'selected':'' }}>
                                    {{ $cooperative->name }}
                                </option>
                            @endforeach
                        </select>
                        @if ($errors->has('cooperative'))
                            <span class="help-block"><strong>{{ $errors->first('cooperative') }}</strong></span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('phone')?' has-error':'' }}">
                    <label class="col-sm-2 col-sm-2 control-label">Celular</label>
                    <div class="col-sm-10">
                        <input name="phone" type="text" class="form-control" value="{{ $user->phone }}">
                        @if ($errors->has('phone'))
                            <span class="help-block"><strong>{{ $errors->first('phone') }}</strong></span>
                        @endif
                    </div>
                </div>
                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
            </form>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="{{ asset('/assets/js/custom/users.js') }}"></script>
<script>
    $('#role').change(function () {
        if ($(this).val() != 'A') {
            $('#div_cooperative').hide();
            $('#cooperative').prop('selectedIndex', -1);
        } else {
            $('#div_cooperative').show();
		}
    });
    $('#role').trigger('change');
</script>
<script>
    document.getElementById('a_users').classList.add('active');
</script>
@endsection