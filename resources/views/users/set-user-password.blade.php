@extends('layouts.app')
@section('title')
Cambiar contraseña
@endsection
@section('content')
<h3><i class="fa fa-angle-right"></i> Cambio de contraseña</h3>
<div class="row mt">
    <div class="col-lg-12">
        <div class="form-panel">
            <h4 class="mb"><i class="fa fa-angle-right"></i> {{ $user->name }}</h4>
            <form action="{{ url('/users/' . $user->id . '/password')" class="form-horizontal style-form" method="POST">
                {{ csrf_field() }}
                <div class="form-group{{ $errors->has('new_password')?' has-error':'' }}">
                    <label class="col-sm-2 col-sm-2 control-label">Nueva contraseña</label>
                    <div class="col-sm-10">
                        <input name="new_password" type="password" class="form-control">
                        @if ($errors->has('new_password'))
                            <span class="help-block"><strong>{{ $errors->first('new_password') }}</strong></span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Confirmación de la nueva contraseña</label>
                    <div class="col-sm-10">
                        <input name="new_password_confirmation" type="password" class="form-control">
                    </div>
                </div>
                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
            </form>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="{{ asset('/assets/js/custom/users.js') }}"></script>
@if ($changed == true)
<script>
    alert('Los cambios fueron almacenados exitosamente.');
</script>
@endif
<script>
    document.getElementById('a_users').classList.add('active');
</script>
@endsection