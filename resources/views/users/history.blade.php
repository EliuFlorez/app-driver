@extends('layouts.app')
@section('title')
Historia de usuarios
@endsection
@section('content')
<h3><i class="fa fa-angle-right"></i> Historial de usuarios</h3>
<div class="row mt">
    <div class="col-lg-4 col-md-6 col-sm-12">
        <div class="form-panel">
            <h4 class="mb"><i class="fa fa-angle-right"></i> Consulta</h4>
            <form action="{{ url('/users/history') }}" method="POST">
                <input type="hidden" name="_method" value="PUT"/>
                {{ csrf_field() }}
                <div class="form-group">
                    <div class="input-group">
                        <input name="search" type="text" class="form-control">
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-primary" type="button"><i class="fa fa-search"></i></button>
                        </span>
                    </div>
                </div>
                <div class="form-group{{ $errors->has('user')?' has-error':'' }}">
                    <select id="user_id" onchange="document.getElementById('user').value = this.value;" class="form-control" multiple>
                        @foreach ($users as $user)
                            <option value="{{ $user->id }}">{{ $user->name . ' (' . $user->vehicle_number . ')'  }}</option>
                        @endforeach
                    </select>
                    @if($errors->has('from'))
                        <span class="help-block">{{ $errors->first('user') }}</span>
                    @endif
                </div>
            </form>
        </div>
    </div>
    <div class="col-lg-8 col-md-6 col-sm-12">
        <div class="form-panel">
            <h4><i class="fa fa-angle-right"></i> Resultados</h4>
            <form action="{{ url('/users/history') }}" class="form-horizontal style-form" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="user" id="user" />
                <div class="form-group{{ $errors->has('filter')?' has-error':'' }}">
                    <label class="col-sm-2 col-sm-2 control-label">Filtrar por</label>
                    <div class="col-sm-10">
                        <select id="filter" name="filter" onchange="hideDates(this.value);" class="form-control">
                            <option value="H">Hoy</option>
                            <option value="S">Esta semana</option>
                            <option value="M">Este mes</option>
                            <option selected value="O">Rango de fechas</option>
                        </select>
                        @if($errors->has('filter'))
                            <span class="help-block">{{ $errors->first('filter') }}</span>
                        @endif
                    </div>
                </div>
                <div id="div_from" class="form-group{{ $errors->has('from')?' has-error':'' }}">
                    <label class="col-sm-2 col-sm-2 control-label">Desde</label>
                    <div class="col-sm-10">
                        <input id="from" name="from" type="text" class="form-control"/>
                        @if($errors->has('from'))
                            <span class="help-block">{{ $errors->first('from') }}</span>
                        @endif
                    </div>
                </div>
                <div id="div_to" class="form-group{{ $errors->has('to')?' has-error':'' }}">
                    <label class="col-sm-2 col-sm-2 control-label">Hasta</label>
                    <div class="col-sm-10">
                        <input id="to" name="to" type="text" class="form-control"/>
                        @if($errors->has('to'))
                            <span class="help-block">{{ $errors->first('to') }}</span>
                        @endif
                    </div>
                </div>
                <input class="btn btn-primary" type="submit" value="Consultar" />
            </form>
            <br/>
            @if (isset($roads) && $roads->count() > 0)
                <h4><i class="fa fa-angle-right"></i> Resumen</h4>
                <div class="table-responsive">
                    <section id="unseen">
                        <table class="table table-bordered table-striped table-condensed">
                            <thead>
                            <tr>
                                <th class="numeric">Carreras</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="numeric">{{ $roads->count() }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </section>
                </div>
                <h4><i class="fa fa-angle-right"></i> Detalle</h4>
                <div class="table-responsive">
                    <section id="unseen">
                        <table class="table table-bordered table-striped table-condensed">
                            <thead>
                            <tr>
                                <th></th>
                                <th>Cliente</th>
                                <th>Inicio</th>
                                <th>Destino</th>
                                <th>Fecha de solicitud</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach ($roads as $road)
                                    <tr>
                                        <td>
                                            <button class="btn btn-primary"><i class="fa fa-eye"></i> Ruta</button>
                                        </td>
                                        <td>{{ $road->user->name }}</td>
                                        <td>{{ $road->location_address }}</td>
                                        <td>{{ $road->destine_address }}</td>
                                        <td>{{ $road->created_at }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </section>
                    {{ $roads->links() }}
                </div>
            @else 
                <div class="alert alert-info"><strong>No hay resultados que mostrar.</strong></div>
            @endif
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="{{ asset('/assets/js/custom/users.js') }}"></script>
<script>
    $('#from').datepicker();
    $('#to').datepicker();
    @if (isset($user_id) && isset($filter))
        document.getElementById('user_id').value = '{{ $user_id }}';
        document.getElementById('filter').value = '{{ $filter }}';
        document.getElementById('user').value = '{{ $user_id }}';
        hideDates('{{ $filter }}');
    @else 
        document.getElementById('user_id').value = "{{ (old('user') != null)?old('user'):'' }}";
        document.getElementById('filter').value = "{{ (old('filter') != null)?old('filter'):'O' }}";
        hideDates(document.getElementById('filter').value);
    @endif
    @if (isset($from) && isset($to))
        document.getElementById('from').value = '{{ $from }}';
        document.getElementById('to').value = '{{ $to }}';
    @endif

    document.getElementById('a_drivers').classList.add('active');
</script>
@endsection