@extends('layouts.app')
@section('title')
{{ Auth::user()->name }}
@endsection
@section('content')
<h3><i class="fa fa-angle-right"></i> Perfil de usuario</h3>
<div class="row-mt">
    <div class="col-lg-6 col-md-6 col-sm-12">
        <div class="content-panel text-center">
            <h4 class="mb"><i class="fa fa-angle-right"></i> Foto de perfil</h4>
            <img id="image"  height="250" class="img-circle" src="{{ (Auth::user()->image != null && Auth::user()->image != '') ? url('/uploads/images/' . Auth::user()->image) : url('/assets/img/user.png') }}" />
        </div>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-12">
        <div class="form-panel">
            <h4 class="mb"><i class="fa fa-angle-right"></i> Información personal</h4>
            <form enctype="multipart/form-data" action="{{ url('/profile') }}" method="POST">
                {{ csrf_field() }}
                <div class="form-group{{ $errors->has('name')?' has-error':'' }}">
                    <label>Nombres y apellidos</label>
                    <input type="text" name="name" class="form-control" value="{{ Auth::user()->name }}" />
                    @if($errors->has('name'))
                        <span class="help-block">{{ $errors->first('name') }}</span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('email')?' has-error':'' }}">
                    <label>Correo electrónico</label>
                    <input type="email" name="email" class="form-control" value="{{ Auth::user()->email }}" />
                    @if($errors->has('email'))
                        <span class="help-block">{{ $errors->first('email') }}</span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('phone')?' has-error':'' }}">
                    <label>Celular</label>
                    <input type="text" name="phone" class="form-control" value="{{ Auth::user()->phone }}" />
                    @if($errors->has('phone'))
                        <span class="help-block">{{ $errors->first('phone') }}</span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('image')?' has-error':'' }}">
                    <label>Foto de perfil</label>
                    <input type="file" name="image" id="image"/>
                    @if($errors->has('image'))
                        <span class="help-block">{{ $errors->first('image') }}</span>
                    @endif
                </div>
                <div class="form-group text-right">
                    <button type="submit" class="btn btn-primary">Actualizar datos</button>
                    <a href="{{ url('/change-password') }}" class="btn btn-danger">Cambiar contraseña</a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="{{ asset('/assets/js/custom/users.js') }}"></script>
@if (isset($user))
    <script>
        alert('Sus datos fueron acualizados exitosamente.');
    </script>
@endif
@endsection