@extends('layouts.app')
@section('title')
Crear usuario
@endsection
@section('content')
<h3><i class="fa fa-angle-right"></i> Crear usuario</h3>
<div class="row mt">
    <div class="col-lg-12">
        <div class="form-panel">
            <h4 class="mb"><i class="fa fa-angle-right"></i> Información</h4>
            <form enctype="multipart/form-data" action="{{ url('/users') }}" class="form-horizontal style-form" method="POST">
                {{ csrf_field() }}
                <div class="form-group{{ $errors->has('name')?' has-error':'' }}">
                    <label class="col-sm-2 col-sm-2 control-label">Nombres y apellidos</label>
                    <div class="col-sm-10">
                        <input value="{{ old('name') }}" name="name" type="text" class="form-control">
                        @if ($errors->has('name'))
                            <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Foto seleccionada</label>
                    <div class="col-sm-10">
                        <img id="image" class="profile-pic" src="{{ asset('/assets/img/user.png') }}" />
                    </div>
                </div>
                <div class="form-group{{ $errors->has('image')?' has-error':'' }}">
                    <label class="col-sm-2 col-sm-2 control-label">Seleccionar nueva foto</label>
                    <div class="col-sm-5">
                        <input id="image" name="image" type="file" value="{{ old('image') }}" />
                        @if ($errors->has('image'))
                            <span class="help-block"><strong>{{ $errors->first('image') }}</strong></span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('email')?' has-error':'' }}">
                    <label class="col-sm-2 col-sm-2 control-label">Dirección de correo electrónico</label>
                    <div class="col-sm-10">
                        <input value="{{ old('email') }}" name="email" type="email" class="form-control">
                        @if($errors->has('email'))
                            <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('role')?' has-error':'' }}">
                    <label class="col-sm-2 col-sm-2 control-label">Rol de usuario</label>
                    <div class="col-sm-10">
                        <select {{ (Auth::user()->role === 'A')?'style="display:none;"':'' }} id="role" name="role" class="form-control">
                            @if (Auth::user()->role === 'S')
                                <option value="S">Superadmin</option>
                            @endif
                            @if (Auth::user()->role != 'A')
                                <option value="D">Distribuidor</option>
                            @endif
                            <option value="A">Administrador de cooperative</option>
                        </select>
                        <script>
                            @if (old('role') != null)
                                document.getElementById('role').value = "{{ old('role') }}";
                            @endif
                        </script>
                        @if ($errors->has('role'))
                            <span class="help-block"><strong>{{ $errors->first('role') }}</strong></span>
                        @endif
                    </div>
                </div>

                <div id="div_cooperative" class="form-group{{ $errors->has('cooperative')?' has-error':'' }}">
                    <label class="col-sm-2 col-sm-2 control-label">Cooperativa</label>
                    <div class="col-sm-10">
                        <select class="form-control" name="cooperative" id="cooperative">
                            @foreach ($cooperatives as $cooperative)
                                <option value="{{ $cooperative->id }}" {{ ($cooperative->id === old('cooperative'))?'selected':'' }}>
                                    {{ $cooperative->name }}
                                </option>
                            @endforeach
                        </select>
                        @if ($errors->has('cooperative'))
                            <span class="help-block"><strong>{{ $errors->first('cooperative') }}</strong></span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('password')?' has-error':'' }}">
                    <label class="col-sm-2 col-sm-2 control-label">Contraseña</label>
                    <div class="col-sm-10">
                        <input name="password" type="password" class="form-control">
                        @if ($errors->has('password'))
                            <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Confirmación de contraseña</label>
                    <div class="col-sm-10">
                        <input name="password_confirmation" type="password" class="form-control">
                    </div>
                </div>
                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
            </form>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="{{ asset('/assets/js/custom/users.js') }}"></script>
<script>
    $('#role').change(function () {
        if ($(this).val() != 'A') {
            $('#div_cooperative').hide();
            $('#cooperative').prop('selectedIndex', -1);
        } else {
            $('#div_cooperative').show();
		}
    });
    $('#role').trigger('change');
    document.getElementById('a_users').classList.add('active');
</script>
@endsection