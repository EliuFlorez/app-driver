@extends('layouts.single')
@section('content')
<div class="container">
    <br/>
    <div class="panel">
        <div class="panel-heading">
            <h3>Error 500, el servidor no pudo procesar esta solicitud.</h3>
        </div>
        <div class="panel-body">
            <p>Este mensaje puede aparecer debido a las siguientes razones:</p>
            <ul>
                <li>Problemas con el servidor</li>
                <li>Solicitudes inadecuadas</li>
                <li>El sitio se encuentra en mantenimiento</li>
                <li>El sitio se encuentra temporalmente fuera de servicio</li>
            </ul>
            <a href="/">Ir al inicio</a>
        </div>
    </div>
</div>
@endsection
