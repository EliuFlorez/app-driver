@extends('layouts.single')
@section('content')
<div class="container">
    <br/>
    <div class="panel">
        <div class="panel-heading">
            <h3>Error 403, no tienes permisos para continuar tu solicitud.</h3>
        </div>
        <div class="panel-body">
            <a href="/">Volver al inicio</a>
        </div>
    </div>
</div>
@endsection

