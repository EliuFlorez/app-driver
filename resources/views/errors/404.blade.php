@extends('layouts.single')
@section('content')
<div class="container">
    <br/>
    <div class="panel">
        <div class="panel-heading">
            <h3>Error 404, la página solicitada no fue encontrada.</h3>
        </div>
        <div class="panel-body">
            <a href="/">Volver al inicio</a>
        </div>
    </div>
</div>
@endsection
