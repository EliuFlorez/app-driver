<!DOCTYPE html>
<html>
<head>
    <title>Reporte</title>
    <meta charset="utf-8" />
    <style>
        table {
            width: 100%;
        }

        th, td {
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even){background-color: #F2F2F2}

        th {
            background-color: #FACC2E;
            color: black;
        }
    </style>
</head>
<body>
<br/>
<img src="assets/img/mitaxiapplogo1.png') }}" width="64" height="64" align="left"/> <img src="assets/img/kimeralogo.png') }}" width="85" height="64" align="right"/>
<h3 style="font-weight: bold" align="center"> Reporte de pago por servicio del sistema “MiTaxi Ecuador” </h3>
<h4 style="font-style:italic;" align="center">Desarrollado por Kimerasoft-ec</h4>
<br/>
<h4>Fecha de generación del reporte:  {{ date('d-m-Y H:i') }}</h4>
<h4>Mes y año correspondiente del reporte:  {{$previus_month}} {{$previus_year}}</h4>
<br/>

<h4 style="font-weight: bold" align="left">Detalles de pago</h4>
<p align="justify">
    El pago por servicio del sistema “MiTaxi Ecuador” incluye soporte técnico por medio de llamadas telefónicas, correos electrónicos y contacto vía whatsapp.
    También, mantenimiento de la página web y la aplicación móvil.
</p>
<p align="justify">
    El valor del pago es de $13.50 (USD Dólares Estadounidenses) mensuales por cada taxista, este valor no incluye IVA.
</p>
<br/>
<h4 style="font-weight: bold" align="left">Clausulas</h4>
<ol>
    <li>
        <p align="justify" style="vertical-align: middle">
            Se pagará a Kimerasoft-ec por todos los taxistas que hayan sido creados hasta el día 25 del mes, si la creación de algún taxista se ha realizado pasado del día 25 el pago por éste no contará para el mes actual sino para el siguiente.
            No se realizará el pago a Kimerasoft-ec por aquellos taxistas que sean eliminados o inhabilitados hasta el día 10 de cada mes, en caso de que sean eliminados o inhabilitados posteriormente se deberá pagar por ellos el valor correspondiente por el mes en su totalidad. Cabe recalcar que cuando se inhabilita a un taxista éste no tendrá acceso al sistema.
        </p>
    </li>
    <li>
        <p align="justify" style="vertical-align: middle">
            El valor total del pago a Kimerasoft-ec deberá  ser realizado máximo hasta el día 10 del siguiente mes, de no cancelarse este valor el sistema se bloqueará para todos los usuarios y taxistas automáticamente hasta que el pago se haga efectivo. </p>
    </li>
    <li>
        <p align="justify" style="vertical-align: middle">
            En caso de que el sistema haya sido bloqueado por falta de pago, se cobrará un valor adicional por reconexión de un 2% del total adeudado. </p>
    </li>
</ol>

<br/><br/><br/>
<h4 style="font-weight: bold" align="left">Tabla de detalle mensual de taxistas</h4>
<p style="font-weight: normal" align="justify">Total de conductores del mes: {{$cantidad_taxis}}</p>
 @foreach($cooperativas as $cooperativa)
    <br/><br/>
    <h3 style="font-weight: bold" align="left">Cooperativa {{$cooperativa->nombre}}</h3>
    <table>
        <thead>
        <tr>
            <th align="center">Número de unidad</th>
            <th align="center">Conductor</th>
            <th align="center">Fecha última de modificación</th>
            <th align="center">Cantidad de carreras</th>
        </tr>
        </thead>
        <tbody>
        @foreach($array_result as $driver)
           @if($driver->cooperativa == $cooperativa->nombre)
            <tr>
                <td align="center">{{$driver->numero_unidad}}</td>
                <td align="left">{{$driver->nombre}}</td>
                <td align="center">{{$driver->ultima_actualizacion}}</td>
                <td align="center">{{$driver->total_carreras}}</td>
            </tr>
            @endif
         @endforeach
        </tbody>
    </table>
    @endforeach
    @if($null_coop_exist==true)
    <br/><br/>
    <h3 style="font-weight: bold" align="left">Sin cooperativa asignada</h3>
    <table>
        <thead>
        <tr>
            <th align="center">Número de unidad</th>
            <th align="center">Conductor</th>
            <th align="center">Fecha última de modificación</th>
            <th align="center">Cantidad de carreras</th>
        </tr>
        </thead>
        <tbody>
        @foreach($array_result as $driver)
           @if($driver->cooperativa == null)
            <tr>
                <td align="center">{{$driver->numero_unidad}}</td>
                <td align="left">{{$driver->nombre}}</td>
                <td align="center">{{$driver->ultima_actualizacion}}</td>
                <td align="center">{{$driver->total_carreras}}</td>
            </tr>
            @endif
         @endforeach
        </tbody>
    </table>
    @endif
    <br/><br/>
    <p style="font-weight: normal" align="justify">* La fecha última de modificación refleja la fecha en que uno o mas datos del taxista han sido modificados, 
    si este nunca ha sido modificado desde su creación se reflejará la fecha en que ha sido creado. Cabe recalcar que si un taxista es inhabilitado el sistema no permitirá que sea modificado hasta ser habilitado nuevamente, por lo tanto en ese caso la fecha que se visualizará será la de última inhabilitación.</p>
    <br/><br/>
    <h3 style="font-weight: bold; color: red;" align="left">Detalle de pago de Mi Taxi Ecuador a Kimerasoft-EC</h3>
    <table>
        <thead>
        <tr style="font-weight: bold;">
            <th align="center">SUBTOTAL</th>
            <th align="center">IVA (14%)</th>
            <th align="center">TOTAL</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td align="center" style="font-weight: bold;">$ {{round($subtotal, 2)}}</td>
            <td align="center" style="font-weight: bold;">$ {{round($iva, 2)}}</td>
            <td align="center" style="font-weight: bold;">$ {{round($total, 2)}}</td>
        </tr>
        </tbody>
    </table>
<br/><br/>

<h4 style="font-weight: bold" align="left">Firmas</h4>
<br/>

<p align="center">___________________________</p>
<p align="center">Jean Nombre Cando Apellido&nbsp;&nbsp;CI: 0912345678</p>
<br/>
<br/>
<br/>
<p align="center">___________________________</p>
<p align="center">Jorge Luis Molina Torres&nbsp;&nbsp;CI: 0931218234</p>
<br/>
<br/>
<br/>
<p align="center">___________________________</p>
<p align="center">Andres Alexander León Doyle&nbsp;&nbsp;CI: 0929866168</p>
<br/>
<br/>
<br/>

<p align="center">
    <img src="assets/img/mitaxiapplogo1.png') }}" width="32" height="32" align="left"/>
    mitaxiecuador@kimerasoft.com&nbsp;&nbsp;&nbsp;&nbsp;www.kimerasoft-ec.com
    <img src="assets/img/kimeralogo.png') }}" width="43" height="32" align="right"/>
</p>

</body>

</html>


