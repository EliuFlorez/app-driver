@extends('layouts.app')
@section('title')
Reportes
@endsection
@section('content')
<h3><i class="fa fa-angle-right"></i> Reportes de Mi Taxi Ecuador</h3>
<div class="row mt">
    <div class="col-lg-4 col-md-6 col-sm-12">
        <div class="form-panel">
            <h4 class="mb"><i class="fa fa-angle-right"></i> Filtros del reporte</h4>
            <form id="form" action="{{ url('/reports/search') }}" class="form-horizontal style-form" method="POST">
                {{ csrf_field() }}
                <div class="form-group{{ $errors->has('filter')?' has-error':'' }}">
                    <label class="col-sm-2 col-sm-2 control-label">Tipo</label>
                    <div class="col-sm-10">
                        <div class="radio">
                            <label>
                                <input id="radio_g" onclick="enableGeneralView(this);" type="radio" name="filter" value="G"/> General
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input checked id="radio_u" onclick="enableGeneralView(this);" type="radio" name="filter" value="U" /> Por usuario
                            </label>
                        </div>
                        @if ($errors->has('filter'))
                            <span class="help-block"><strong>{{ $errors->first('filter') }}</strong></span>
                        @endif
                    </div>
                </div>
                <div id="div_all" class="form-group">
                    <div class="col-sm-12">
                        <div class="checkbox">
                            <label>
                                <input id="select_all" onclick="selectAll(this.checked);" type="checkbox" value="T" /> <strong>Seleccionar todos</strong>
                            </label>
                        </div>
                    </div>
                </div>
                <div id="div_list" class="form-group {{ $errors->has('users')?' has-error':'' }}">
                    <label class="col-sm-2 col-sm-2 control-label">Lista</label>
                    <div class="col-sm-10">
                        @foreach ($users as $user)
                            <div class="checkbox">
                                <label>
                                    <input onchange="checkedUserEvent(this.checked);" type="checkbox" name="users[]" value="{{ $user->id }}"/> {{ $user->name }}
                                </label>
                            </div>
                        @endforeach
                        {{ $users->links() }}
                        @if($errors->has('users'))
                            <span class="help-block"><strong>{{ $errors->first('users') }}</strong></span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('from')?' has-error':'' }}">
                    <label class="col-sm-2 col-sm-2 control-label">Desde</label>
                    <div class="col-sm-10">
                        <input id="from" name="from" type="text" class="form-control" />
                        @if ($errors->has('from'))
                            <span class="help-block"><strong>{{ $errors->first('from') }}</strong></span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('to')?' has-error':'' }}">
                    <label class="col-sm-2 col-sm-2 control-label">Hasta</label>
                    <div class="col-sm-10">
                        <input id="to" name="to" type="text" class="form-control" />
                        @if ($errors->has('to'))
                            <span class="help-block"><strong>{{ $errors->first('from') }}</strong></span>
                        @endif
                    </div>
                </div>
                <button id="btn_search" type="submit" class="btn btn-primary">Consultar</button>
                <button onclick="generateReport();" id="btn_search" type="button" class="btn btn-default">Generar PDF</button>
            </form>
        </div>
    </div>
    <div class="col-lg-8 col-md-6 col-sm-12">
        <div class="content-panel">
            @if(isset($cab_rides) && $cab_rides->count() > 0)
                <h4 class="mb"><i class="fa fa-angle-right"></i> Resultados desde <small>{{ $from }}</small> hasta <small>{{ $to }}</small></h4>
                <div class="table-responsive">
                    <section id="unseen">
                        <table class="table table-bordered table-striped table-condensed">
                            <thead>
                            <tr>
                                <th class="numeric">Carreras</th>
                                <th class="numeric">Valor total recaudado</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="numeric">{{ $cab_rides->count() }}</td>
                                    <td class="numeric">{{ $cab_rides->sum('price') }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </section>
                </div>
                <div class="table-responsive">
                    <section id="unseen">
                        <table class="table table-bordered table-striped table-condensed">
                            <thead>
                            <tr>
                                <th>Conductor</th>
                                <th>Cliente</th>
                                <th>Inicio</th>
                                <th>Destino</th>
                                <th>Fecha de solicitud</th>
                                <th>Precio</th>
                                <th>Estado</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach ($cab_rides as $cab_ride)
                                    <tr>
                                        <td>{{ ($cab_ride->driver != null)?$cab_ride->driver->name:'-' }}</td>
                                        <td>{{ ($cab_ride->user != null)?$cab_ride->user->name:'-' }}</td>
                                        <td>{{ $cab_ride->location_address }}</td>
                                        <td>{{ $cab_ride->destine_address }}</td>
                                        <td>{{ $cab_ride->created_at }}</td>
                                        <td>{{ $cab_ride->price }}</td>
                                        <td>{{ ($cab_ride->status === 'P')?'Pendiente':(($cab_ride->status === 'R')?'En proceso':(($cab_ride->status === 'C')?'Cancelada':'Finalizada')) }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </section>
                </div>
            @else
                <div class="alert alert-info">
                    <strong>No hay resultados disponibles.</strong>
                </div>
            @endif
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="{{ asset('/assets/js/custom/reports.js') }}"></script>
<script>
    @if (isset($array))
        var users = document.getElementsByName('users[]');
        @foreach ($array as $a)
            for (var i = 0 ; i < users.length; i++)
            {
                if ('{{ $a }}' === users[i].value)
                    users[i].checked = true;
            }
        @endforeach
    @endif
    @if (isset($from) && isset($to))
        document.getElementById('from').value = '{{ $from->format('m/d/Y') }}';
        document.getElementById('to').value = '{{ $to->format('m/d/Y') }}';
    @endif
    @if (isset($general) && $general == true)
        document.getElementById('radio_g').checked = true;
        enableGeneralView(document.getElementById('radio_g'));
    @endif
</script>
@endsection