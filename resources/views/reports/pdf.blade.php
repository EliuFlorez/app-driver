<!DOCTYPE html>
<html>
    <head>
        <title>Reporte</title>
        <meta charset="utf-8" />
        <style>
            table {
                width: 100%;
            }

            th, td {
                text-align: left;
                padding: 8px;
            }

            tr:nth-child(even){background-color: #f2f2f2}

            th {
                background-color: #4CAF50;
                color: white;
            }
        </style>
    </head>
    <body>
        <h4><img src="assets/img/mitaxiapplogo.png') }}" width="64" height="75" /> Reporte de resultados por conductor desde <small>{{ $from }}</small> hasta <small>{{ $to }}</small></h4>
        <h3>Generado en {{ date('Y-m-d H:i:s') }}</h3>
        @if(isset($cab_rides) && $cab_rides->count() > 0)
            
            <table>
                <thead>
                <tr>
                    <th class="numeric">Carreras</th>
                    <th class="numeric">Valor total recaudado</th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="numeric">{{ $cab_rides->count() }}</td>
                        <td class="numeric">{{ $cab_rides->sum('price') }}</td>
                    </tr>
                </tbody>
            </table>
            <table>
                <thead>
                <tr>
                    <th>Conductor</th>
                    <th>Unidad</th>
                    <th>Cliente</th>
                    <th>Inicio</th>
                    <th>Destino</th>
                    <th>Fecha</th>
                    <th>Estado</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($cab_rides as $cab_ride)
                        <tr>
                            <td>{{ ($cab_ride->driver != null)?$cab_ride->driver->name:'-' }}</td>
                            <td>{{ ($cab_ride->driver != null)?$cab_ride->driver->vehicle_number:'-' }}</td>
                            <td>{{ $cab_ride->user->name }}</td>
                            <td>{{ $cab_ride->location_address }}</td>
                            <td>{{ $cab_ride->destine_address }}</td>
                            <td>{{ $cab_ride->created_at }}</td>
                            <td>{{ ($cab_ride->status === 'P')?'Pendiente':(($cab_ride->status === 'R')?'En proceso':(($cab_ride->status === 'C')?'Cancelada':'Finalizada')) }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @else
            <div class="alert alert-info">
                <strong>No hay resultados disponibles.</strong>
            </div>
        @endif
    </body>
</html>