@extends('layouts.app')
@section('title')
    Cooperativas
@endsection
@section('content')
    <h3><i class="fa fa-angle-right"></i> Lista de cooperativas</h3>
    <div class="row mt">
        <div class="col-lg-12">
            <div class="form-panel">
                <h4 class="mb"><i class="fa fa-angle-right"></i> Consulta</h4>
                <form method="POST" action="{{ url('/cooperatives/search') }}" class="form-inline" role="form">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <input name="search" type="text" class="form-control" id="search">
                    </div>
                    <button type="submit" class="btn btn-theme">Buscar</button>
                </form>
            </div>
        </div>
    </div>
    <div class="row mt">
        <div class="col-lg-12">
            <div class="content-panel">
                <h4><i class="fa fa-angle-right"></i> Resultados</h4>
                @if($cooperatives->count() > 0)
                    <div class="table-responsive">
                        <section id="unseen">
                            <table class="table table-bordered table-striped table-condensed text-center">
                                <thead>
                                <tr>
                                    <th>Editar</th>
                                    <th>Nombre</th>
                                    <th>Fecha de creación</th>
                                    <th>Fecha de modificación</th>
                                    <th>Estado</th>
                                    <th>Eliminar</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($cooperatives as $cooperative)
                                    <tr>
                                        @if($cooperative->name!='Mi Taxi Ecuador')
                                            <td><a href="{{ url('/cooperatives/'.$cooperative->id) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a></td>
                                        @else
                                            <td></td>
                                        @endif
                                        <td>{{ $cooperative->name }}</td>
                                        <td>{{ $cooperative->created_at }}</td>
                                        <td>{{ $cooperative->updated_at }}</td>
                                        @if($cooperative->name!='Mi Taxi Ecuador')
											<td>
												<div class="col-lg-12">
													<div class="switch switch-square"
														 data-on-label="<i class=' fa fa-check'></i>"
														 data-off-label="<i class='fa fa-times'></i>">
														<input onchange="activateCooperativa('{{ url("/cooperatives/" . $cooperative->id . '/activate') }}', this.checked);" type="checkbox" {{ ($cooperative->status == true)?'checked=""':'' }} />
													</div>
												</div>
											</td>
											<td>
												<form onsubmit="return confirm('¿Estás seguro de eliminar esta cooperativa?');" method="POST" action="{{ url('/cooperatives/' . $cooperative->id) }}">
													{{ csrf_field() }}
													<input type="hidden" name="_method" value="DELETE" />
													<button type="submit" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>
												</form>
											</td>
                                        @else
                                           <td></td>
                                           <td></td>
                                        @endif

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{ $cooperatives->links() }}
                        </section>
                    </div>
                @else
                    <div class="alert alert-info">No hubo resultados.</div>
                @endif
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/assets/js/custom/cooperatives.js') }}"></script>
    <script>
        document.getElementById('a_cooperatives').classList.add('active');
    </script>
@endsection
