@extends('layouts.app')
@section('title')
    Crear cooperativa
@endsection
@section('content')
    <h3><i class="fa fa-angle-right"></i> Crear Cooperativa</h3>
    <div class="row mt">
        <div class="col-lg-12">
            <div class="form-panel">
                <h4 class="mb"><i class="fa fa-angle-right"></i> Información</h4>
                <form enctype="multipart/form-data" action="{{ url('/cooperatives') }}" class="form-horizontal style-form" method="POST">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('name')?' has-error':'' }}">
                        <label class="col-sm-2 col-sm-2 control-label">Nombre</label>
                        <div class="col-sm-10">
                            <input value="{{ old('name') }}" name="name" type="text" class="form-control">
                            @if ($errors->has('name'))
                                <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 col-sm-2 control-label">Foto seleccionada</label>
                        <div class="col-sm-10">
                            <img id="image" class="profile-pic" src="{{ asset('/assets/img/logo-auto-generico.png') }}" />
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('image')?' has-error':'' }}">
                        <label class="col-sm-2 col-sm-2 control-label">Seleccionar nueva foto</label>
                        <div class="col-sm-5">
                            <input id="image" name="image" type="file" value="{{ old('image') }}" />
                            @if ($errors->has('image'))
                                <span class="help-block"><strong>{{ $errors->first('image') }}</strong></span>
                            @endif
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('/assets/js/custom/cooperatives.js') }}"></script>
    <script>
        document.getElementById('a_cooperatives').classList.add('active');
    </script>
@endsection
