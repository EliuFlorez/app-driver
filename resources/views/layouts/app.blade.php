<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="_token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="@eliuflorez">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <title>UDV | @yield('title')</title>
    <link href="{{ asset('/assets/css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('/assets/font-awesome/css/font-awesome.css" rel="stylesheet') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('/assets/css/zabuto_calendar.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/assets/js/gritter/css/jquery.gritter.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('/assets/lineicons/style.css') }}">    
    <link href="{{ asset('/assets/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('/assets/css/style-responsive.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('/js/datetime/jquery.datetimepicker.css')}}">
    <link href="{{ asset('/js/chosen/chosen.min.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script>
        window.Laravel = <?php echo json_encode(['csrfToken' => csrf_token()]); ?>
	</script>
  </head>
  <body>
  <section id="container" >
      <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
              </div>
            <a href="{{ url('/') }}" class="logo"><b><img class="login-logo" src="{{ asset('/assets/img/logo.jpg') }}"/></b></a>
            <div class="top-menu">
            	<ul class="nav pull-right top-menu">
                    <li>
                        <a class="logout" href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
							Salir
						</a>
                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
            	</ul>
            </div>
        </header>
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <ul class="sidebar-menu" id="nav-accordion">
              
              	  <p class="centered"><a href="{{ url('/profile') }}"><img src="{{ (Auth::user()->image == null) ? url('/assets/img/user.png') : url('/uploads/images/' . Auth::user()->image) }}" class="img-circle" width="60"></a></p>
              	  <h5 class="centered">{{ Auth::user()->name }}</h5>
              	  	
                  <li class="mt">
                      <a id="a_tracking" href="{{ url('/home') }}">
                          <i class="fa fa-dashboard"></i>
                          <span>Escritorio</span>
                      </a>
                  </li>

                  <li class="sub-menu">
                        <a id="a_users" href="javascript:;" >
                            <i class="fa fa-users"></i>
                            <span>Administración</span>
                        </a>
                        <ul class="sub">
                            <li><a  href="{{ url('/users/create') }}">Crear nuevo usuario</a></li>
                            <li><a  href="{{ url('/users') }}">Ver lista de usuarios</a></li>
                        </ul>
                    </li>

                  <li class="sub-menu">
                      <a id="a_drivers" href="javascript:;" >
                          <i class="fa fa-cab"></i>
                          <span>Unidades/Conductores</span>
                      </a>
                      <ul class="sub">
                        <li><a  href="{{ url('/drivers/create') }}">Registrar conductor</a></li>
                        <li><a  href="{{ url('/drivers') }}">Ver lista de conductores</a></li>
                        <li><a  href="{{ url('/users/history') }}">Ver historial de unidades</a></li>
                        <li><a href="{{ url('/users/tracking') }}">Rastreo</a></li>
                      </ul>
                  </li>
              
                  @if (Auth::user()->role != 'A')
                    <li class="sub-menu">
                        <a id="a_customers" href="javascript:;" >
                            <i class="fa fa-user"></i>
                            <span>Clientes</span>
                        </a>
                        <ul class="sub">
                            <li><a  href="{{ url('/customers/create') }}">Registrar cliente</a></li>
                            <li><a  href="{{ url('/customers') }}">Ver lista de clientes</a></li>
                        </ul>
                    </li>
                    <li class="sub-menu">
                        <a id="a_cooperativas" href="javascript:;" >
                            <i class="fa fa-building"></i>
                            <span>Cooperativas</span>
                        </a>
                        <ul class="sub">
                            <li><a  href="{{ url('/cooperatives/create') }}">Crear nueva cooperativa</a></li>
                            <li><a  href="{{ url('/cooperatives') }}">Ver lista de cooperativas</a></li>
                        </ul>
                    </li>
                  @endif

                  <li class="sub-menu">
                      <a id="a_zones" href="javascript:;" >
                          <i class="fa fa-globe"></i>
                          <span>Zonas</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="{{ url('/zones/create') }}">Crear nueva zona</a></li>
                          <li><a  href="{{ url('/zones') }}">Ver lista de zones</a></li>
                      </ul>
                  </li>
				  
				  <li class="sub-menu">
                      <a id="a_reports" href="{{ url('/reports') }}" >
                          <i class=" fa fa-bar-chart-o"></i>
                          <span>Reportes</span>
                      </a>
                  </li>
				  
                  <li class="sub-menu">
                      <a id="a_cab_rides" href="javascript:;" >
                          <i class="fa fa-gear"></i>
                          <span>Control</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="{{ url('/roads/create') }}">Crear asignacion</a></li>
                          <li><a  href="{{ url('/reservas/create') }}">Crear reserva</a></li>
                          <li><a  href="{{ url('/roads') }}">Ver lista</a></li>
                      </ul>
                  </li>
				  
                  @if (Auth::user()->role !='A')
                    <li class="mt">
                        <a href="{{ url('/configs') }}">
                            <i class="fa fa-cog"></i>
                            <span>Ajustes</span>
                        </a>
                    </li>
                  @endif
				  
                  @if(Auth::user()->role == 'S')
                      <li class="mt">
                          <a href="{{ url('/reports_pago') }}">
                              <i class="fa fa-file"></i>
                              <span>Reporte de pago</span>
                          </a>
                      </li>
                  @endif
              </ul>
          </div>
      </aside>
      <section id="main-content">
          <section class="wrapper">
                @yield('content')
          </section>
      </section>
      <footer class="site-footer">
          <div class="text-center">
              {{ date('Y') }} - Developed by <a href="http://twitter.com/EliuFlorez">E-Florez</a>
              <a href="#" class="go-top">
                  <i class="fa fa-angle-up"></i>
              </a>
          </div>
      </footer>
	</section>
    <script src="{{ asset('/assets/js/jquery.js') }}"></script>
    <script src="{{ asset('/assets/js/jquery-1.8.3.min.js') }}"></script>
    <script src="{{ asset('/assets/js/bootstrap.min.js') }}"></script>
    <script class="include" type="text/javascript" src="{{ asset('/assets/js/jquery.dcjqaccordion.2.7.js') }}"></script>
    <script src="{{ asset('/assets/js/jquery.scrollTo.min.js') }}"></script>
    <script src="{{ asset('/assets/js/jquery.nicescroll.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/assets/js/jquery.sparkline.js') }}"></script>
    <script src="{{ asset('/assets/js/jquery-ui-1.9.2.custom.min.js') }}"></script> 
    <script src="{{ asset('/assets/js/common-scripts.js') }}"></script> 
    <script type="text/javascript" src="{{ asset('/assets/js/gritter/js/jquery.gritter.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/assets/js/gritter-conf.js') }}"></script>	
    <script src="{{ asset('/assets/js/bootstrap-switch.js') }}"></script>
    <script src="{{ asset('/js/chosen/chosen.jquery.min.js') }}"></script>
    <script src="{{ asset('/js/datetime/build/jquery.datetimepicker.full.min.js')}}"></script>
	<script type="application/javascript">
        $(document).ready(function () {
            $("#date-popover").popover({html: true, trigger: "manual"});
            $("#date-popover").hide();
            $("#date-popover").click(function (e) {
                $(this).hide();
            });
        });
        
        function myNavFunction(id) {
            $("#date-popover").hide();
            var nav = $("#" + id).data("navigation");
            var to = $("#" + id).data("to");
            console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
        }
		
        $.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
			}
		});
    </script>
	@yield('scripts')
  </body>
</html>
