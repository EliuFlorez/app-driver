<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="UDV">
        <meta name="author" content="@eliuflorez">
        <meta name="keyword" content="UDV">
        <title>UDV</title>
        <link href="/assets/css/bootstrap.css" rel="stylesheet">
        <link href="/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        <link href="/assets/css/style.css" rel="stylesheet">
        <link href="/assets/css/style-responsive.css" rel="stylesheet"> 
    </head>
    <body>
        <section id="container">
            <header class="header black-bg">
                <div class="sidebar-toggle-box">
                    <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
                </div>
                <a href="/" class="logo"><b><img class="login-logo" src="/assets/img/logo.jpg"/></b></a>
                <div class="top-menu">
                    <ul class="nav pull-right top-menu">
                        <li>
                            <a class="logout" href="#">Inicio</a>
                        </li>
                        <li>
                            <a class="logout" href="#">Nosotros</a>
                        </li>
                        <li>
                            <a class="logout" href="#">Contactos</a>
                        </li>
                        <li>
                            <a class="logout" href="{{ url('/login') }}">
                                Entrar
                            </a>
                        </li>
                    </ul>
                </div>
            </header>
        </section>
        <section id="main-content">
            <section class="wrapper">
                <div class="row mt">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <h4><i class="fa fa-angle-right"></i> Contactos</h4>
                                <form method="POST" action="/contact">
                                    <div class="form-group">
                                        <label>Nombres y apellidos</label>
                                        <input class="form-control" name="name" id="name" placeholder="Tu nombre aquí"/>
                                    </div>
                                    <div class="form-group">
                                        <label>Asunto</label>
                                        <input class="form-control" name="asunto" id="asunto" placeholder="Tema de interés"/>
                                    </div>
                                    <div class="form-group">
                                        <label>Mensaje</label>
                                        <input class="form-control" name="message" id="message" placeholder="Tu mensaje aquí"/>
                                    </div>
                                    <div class="form-group">
                                        <input type="submit" class="btn btn-primary" value="Enviar mensaje" />
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </section>
        <script src="/assets/js/jquery.js"></script>
        <script src="/assets/js/bootstrap.min.js"></script>
    </body>
</html>