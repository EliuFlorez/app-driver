@extends('layouts.app')
@section('title')
    Crear reservación
@endsection
@section('content')
    <h3><i class="fa fa-angle-right"></i> Crear reservación</h3>
    <div class="row mt">
        <div class="col-lg-12">
            <div class="form-panel">
                <h4 class="mb"><i class="fa fa-angle-right"></i> Información</h4>
                <form enctype="multipart/form-data" action="{{ url('/reservas') }}" class="form-horizontal style-form" method="POST">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('phone')?' has-error':'' }}">
                        <label class="col-sm-2 col-sm-2 control-label">Celular del cliente</label>
                        <div class="col-sm-10">
                            <input onblur="searchCustomer('{{ url('/users') }}' + '/' + this.value + '/phone');" value="{{ old('phone') }}" name="phone" type="text" class="form-control"/>
                            @if ($errors->has('phone'))
                                <span class="help-block"><strong>{{ $errors->first('phone') }}</strong></span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('name')?' has-error':'' }}">
                        <label class="col-sm-2 col-sm-2 control-label">Nombre del cliente</label>
                        <div class="col-sm-10">
                            <input id="name" value="{{ old('name') }}" name="name" type="text" class="form-control"/>

                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('reservation_date')?' has-error':'' }}">
                        <label class="col-sm-2 col-sm-2 control-label">Fecha y hora</label>
                        <div class="col-sm-10">
                            <input  name="reservation_date" id="reservation_date" class="form-control" type="text" />
                            @if ($errors->has('reservation_date'))
                                <span class="help-block"><strong>{{ $errors->first('reservation_date') }}</strong></span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('anticipation_send')?' has-error':'' }}">
                        <label class="col-sm-2 col-sm-2 control-label">Anticipación de envio</label>
                        <div class="col-sm-10">
                            <div class="col-sm-3">
                                <div class="radio">
                                    <label><input {{ (old('anticipation_send') == 10)?'checked':'' }} type="radio" name="anticipation_send"  value="10" checked/> 10 minutos</label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="radio">
                                    <label><input  {{ (old('anticipation_send') == 20)?'checked':'' }} type="radio" name="anticipation_send" value="20" /> 20 minutos</label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="radio">
                                    <label><input  {{ (old('anticipation_send') == 30)?'checked':'' }} type="radio" name="anticipation_send" value="30" /> 30 minutos</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group {{ ($errors->has('location_address')||$errors->has('location_latitude')||$errors->has('location_longitude'))?' has-error':'' }}">
                        <label class="col-sm-2 col-sm-2 control-label">Dirección inicial</label>
                        <div class="col-sm-6">
                            <input value="{{ old('location_latitude') }}" type="hidden" id="location_latitude" name="location_latitude" />
                            <input value="{{ old('location_longitude') }}" type="hidden" id="location_longitude" name="location_longitude" />
                            <input  id="location_address" value="{{ old('location_address') }}" name="location_address" type="text" class="form-control" placeholder="Buscar ubicación..."/>
                            <div id="div_location_address_zone">
                                <select onchange="selectZone();" class="form-control chosen-select" id="location_address_zone" name="location_address_zone">
                                    <option value="{{ old('location_address') }}" disabled selected hidden>Buscar zone...</option>
                                    @foreach ($zones as $zone)
                                        <option value="{{ $zone->id }}">{{ $zone->descripcion }}</option>
                                    @endforeach
                                </select>
                            </div>
                            @if ($errors->has('location_address'))
                                <span class="help-block"><strong>{{ $errors->first('location_address') }}</strong></span>
                            @endif
                        </div>
                        <div class="col-sm-2">
                            <div class="radio">
                                <label><input id="filter" type="radio" name="filter" onchange="changeFilter(this.value)" value="U" checked/> Ubicación</label>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="radio">
                                <label><input  id="filter" type="radio" name="filter"  onchange="changeFilter(this.value)" value="Z" /> Zonas</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('location_reference')?' has-error':'' }}">
                        <label class="col-sm-2 col-sm-2 control-label">Referencia inicial</label>
                        <div class="col-sm-10">
                            <input value="{{ old('location_reference') }}" name="location_reference" type="text" class="form-control"/>
                            @if ($errors->has('location_reference'))
                                <span class="help-block"><strong>{{ $errors->first('location_reference') }}</strong></span>
                            @endif
                        </div>
                    </div>
                    <h4 class="mb"><i class="fa fa-angle-right"></i> Mapa de localización del cliente</h4>
                    <div class="cab-ride-map" id="mapLocation" ></div>
                    <div class="form-group {{ ($errors->has('location_longitude')||$errors->has('location_latitude'))?' has-error':'' }}">
                        <div class="col-sm-10">
                            @if ($errors->has('location_latitude') || $errors->has('location_longitude') )
                                <span class="help-block"><strong>{{ $errors->first('location_latitude') }}</strong></span>
                            @endif
                        </div>
                    </div>
                    <br/>
                    <div class="form-group{{ $errors->has('destine_address')?' has-error':'' }}">
                        <label class="col-sm-2 col-sm-2 control-label">Dirección de destino</label>
                        <div class="col-sm-10">
                            <input id="destine_address" value="{{ old('destine_address') }}" name="destine_address" type="text" class="form-control"/>
                            @if ($errors->has('destine_address'))
                                <span class="help-block"><strong>{{ $errors->first('destine_address') }}</strong></span>
                            @endif
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB249EeF8ARkbmlsjDDezz-gm3cLG3VO7M&libraries=places&callback=initAutocomplete" async defer></script>
    <script src="{{ asset('/assets/js/custom/reservas.js') }}"></script>
    <script>
        isPause = true;

        function selectZone()
        {
            borrarZona();
            var zone_id = document.getElementById("location_address_zone").value;
            path_zone = [];

            @foreach($zones as $zone)
            if (zone_id == '{{$zone->id}}') {
                input.value = '{{$zone->descripcion}}';
                @foreach($zone->zone as $item)
                   path_zone.push({lat:parseFloat('{{$item["lat"]}}'),lng:parseFloat('{{$item["lng"]}}')});
                @endforeach
            }
            @endforeach

            if (path_zone.length > 0) {
                line.setMap(map);
                locationLongitude.value = path_zone[0].lng;
                locationLatitude.value = path_zone[0].lat;
                generarZona(path_zone);
            } else {
                input.value=null;
                locationLongitude.value = null;
                locationLatitude.value = null;
                alert("No se encontró la ubicación de la zona seleccionada.");
            }
        }
        $('#reservation_date').datetimepicker();
    </script>
@endsection