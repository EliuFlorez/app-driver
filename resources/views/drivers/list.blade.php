@extends('layouts.app')
@section('title')
Usuarios
@endsection
@section('content')
<h3><i class="fa fa-angle-right"></i> Lista de conductores</h3>
<div class="row mt">
    <div class="col-lg-12">
        <div class="form-panel">
            <h4 class="mb"><i class="fa fa-angle-right"></i> Consulta</h4>
            <form method="POST" action="{{ url('/drivers/search') }}" class="form-inline" role="form">
                {{ csrf_field() }}
                <div class="form-group">
                    <input name="search" type="text" class="form-control" id="search">
                </div>
                <button type="submit" class="btn btn-theme">Buscar</button>
            </form>
        </div>
    </div>
</div>
<div class="row mt">
    <div class="col-lg-12">
        <div class="content-panel">
            <h4><i class="fa fa-angle-right"></i> Resultados</h4>
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="form-group">
					<strong><label>Cantidad de autos activos: </label></strong>{{ App\User::where('role', 'T')->where('status', true)->count() }}<br/>
					<strong><label>Cantidad de autos inactivos: </label></strong>{{ App\User::where('role', 'T')->where('status', false)->count() }}
				</div>
			</div>
			@if($users->count() > 0)
				<div class="table-responsive">
					<section id="unseen">
						<table class="table table-bordered table-striped table-condensed text-center">
							<thead>
								<tr>
									<th>Editar</th>
									<th>Cambiar contraseña</th>
									<th>Cedula</th>
									<th>Nombres y apellidos</th>
									<th>Correo electrónico</th>
									<th>Cooperativa</th>
									<th>Celular</th>
									<th>Permisario</th>
									<th>Fecha modificación</th>
									<th>Estado</th>
									@if(Auth::user()->role == 'S')
									 <th>Eliminar</th>
									@endif
								</tr>
							</thead>
							<tbody>
							@foreach ($users as $user)
								<tr>
									<td><a href="{{ url('/drivers/' . $user->id) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a></td>
									<td><a href="{{ url('/users/' . $user->id) }}/password" class="btn btn-warning"><i class="fa fa-lock"></i></a></td>
									<td>{{ $user->dni }}</td>
									<td>{{ $user->name }}</td>
									<td>{{ $user->email }}</td>
									<td>{{ ($user->cooperative == null) ? '-' : $user->cooperative->name }}</td>
									<td>{{ ($user->cell != null) ? $user->cell : '-' }}</td>
									@if($user->file_permission)
										<td><a href="{{ url('/uploads/permissions/' . $user->file_permission) }}" target="_blank">Permisario</a></td>
									@else
										<td>No Permisario</td>
									@endif
									<td>{{$user->updated_at}}</td>
									<td>
										<div class="col-lg-12">
											<div class="switch switch-square"
												data-on-label="<i class=' fa fa-check'></i>"
												data-off-label="<i class='fa fa-times'></i>">
												<input onchange="activateUser('{{ url("/users/" . $user->id . '/activate') }}', this.checked);" type="checkbox" {{ ($user->status == true)?'checked=""':'' }} />
											</div>
										</div>
									</td>
									@if(Auth::user()->role == 'S')
									 <td>
										<form onsubmit="return confirm('¿Está seguro de eliminar el registro?');" method="POST" action="{{ url('/drivers/' . $user->id) }}">
											{{ csrf_field() }}
											<input type="hidden" name="_method" value="DELETE" />
											<button type="submit" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>
										</form>
									 </td> 
									@endif
								</tr>
								@endforeach
							</tbody>
						</table>
						{{ $users->links() }}
					</section>
				</div>
			@else
				<div class="alert alert-info">No hubo resultados.</div>
			@endif
		</div>
    </div>
</div>
@endsection
@section('scripts')
<script src="{{ asset('/assets/js/custom/users.js') }}"></script>
<script>
    document.getElementById('a_drivers').classList.add('active');
</script>
@endsection