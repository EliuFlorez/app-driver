@extends('layouts.app')
@section('title')
Crear autosta
@endsection
@section('content')
<h3><i class="fa fa-angle-right"></i> Registrar conductor</h3>
<div class="row mt">
    <div class="col-lg-12">
        <div class="form-panel">
            <h4 class="mb"><i class="fa fa-angle-right"></i> Información</h4>
            <form enctype="multipart/form-data" action="{{ url('/drivers') }}" class="form-horizontal style-form" method="POST">
                {{ csrf_field() }}
				
				<div class="form-group{{ $errors->has('dni') ? ' has-error' : '' }}">
                    <label class="col-sm-2 col-sm-2 control-label">Cedula</label>
                    <div class="col-sm-10">
                        <input required value="{{ old('dni') }}" name="dni" type="text" class="form-control">
                        @if ($errors->has('dni'))
                            <span class="help-block"><strong>{{ $errors->first('dni') }}</strong></span>
                        @endif
                    </div>
                </div>
				
				<div class="form-group{{ $errors->has('rut') ? ' has-error' : '' }}">
                    <label class="col-sm-2 col-sm-2 control-label">RUT</label>
                    <div class="col-sm-10">
                        <input required value="{{ old('rut') }}" name="rut" type="text" class="form-control">
                        @if ($errors->has('rut'))
                            <span class="help-block"><strong>{{ $errors->first('rut') }}</strong></span>
                        @endif
                    </div>
                </div>

				<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label class="col-sm-2 col-sm-2 control-label">Nombres y apellidos</label>
                    <div class="col-sm-10">
                        <input required value="{{ old('name') }}" name="name" type="text" class="form-control">
                        @if ($errors->has('name'))
                            <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
                        @endif
                    </div>
                </div>
				
                <div class="form-group{{ $errors->has('cooperative')?' has-error':'' }}">
                    <label class="col-sm-2 col-sm-2 control-label">Cooperativa</label>
                    <div class="col-sm-10">
                        <select class="form-control" name="cooperative" id="cooperative" required>
                            @foreach ($cooperatives as $cooperative)
                                <option value="{{ $cooperative->id }}" {{ ($cooperative->id === old('cooperative')) ? 'selected' : '' }}>
                                    {{ $cooperative->name }}
                                </option>
                            @endforeach
                        </select>
                        @if ($errors->has('cooperative'))
                            <span class="help-block"><strong>{{ $errors->first('cooperative') }}</strong></span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Foto seleccionada</label>
                    <div class="col-sm-10">
                        <img id="image" class="profile-pic" src="{{ asset('/assets/img/user.png') }}" />
                    </div>
                </div>
                <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                    <label class="col-sm-2 col-sm-2 control-label">Seleccionar nueva foto</label>
                    <div class="col-sm-5">
                        <input id="image" name="image" type="file" value="{{ old('image') }}" />
                        @if ($errors->has('image'))
                            <span class="help-block"><strong>{{ $errors->first('image') }}</strong></span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('email')?' has-error':'' }}">
                    <label class="col-sm-2 col-sm-2 control-label">Dirección de correo electrónico</label>
                    <div class="col-sm-10">
                        <input value="{{ old('email') }}" name="email" type="email" class="form-control">
                        @if($errors->has('email'))
                            <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                        @endif
                    </div>
                </div>
				<div class="form-group{{ $errors->has('address')?' has-error':'' }}">
                    <label class="col-sm-2 col-sm-2 control-label">Dirección de domicilio</label>
                    <div class="col-sm-10">
                        <input value="{{ old('address') }}" name="address" type="text" class="form-control">
                        @if($errors->has('address'))
                            <span class="help-block"><strong>{{ $errors->first('address') }}</strong></span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('phone')?' has-error':'' }}">
                    <label class="col-sm-2 col-sm-2 control-label">Teléfono</label>
                    <div class="col-sm-10">
                        <input name="phone" type="text" class="form-control" value="{{ old('phone') }}">
                        @if ($errors->has('phone'))
                            <span class="help-block"><strong>{{ $errors->first('phone') }}</strong></span>
                        @endif
                    </div>
                </div>
				<div class="form-group{{ $errors->has('cell')?' has-error':'' }}">
                    <label class="col-sm-2 col-sm-2 control-label">Celular</label>
                    <div class="col-sm-10">
                        <input name="cell" type="text" class="form-control" value="{{ old('cell') }}">
                        @if ($errors->has('cell'))
                            <span class="help-block"><strong>{{ $errors->first('cell') }}</strong></span>
                        @endif
                    </div>
                </div>
				<div class="form-group{{ $errors->has('number_bps')?' has-error':'' }}">
                    <label class="col-sm-2 col-sm-2 control-label">Número Certificado BPS</label>
                    <div class="col-sm-10">
                        <input id="number_bps" name="number_bps" type="text" class="form-control" value="{{ old('number_bps') }}">
                        @if ($errors->has('number_bps'))
                            <span class="help-block"><strong>{{ $errors->first('number_bps') }}</strong></span>
                        @endif
                    </div>
                </div>
				<div class="form-group{{ $errors->has('license_cta')?' has-error':'' }}">
                    <label class="col-sm-2 col-sm-2 control-label">Cta. Licencia</label>
                    <div class="col-sm-10">
                        <input id="license_cta" name="license_cta" type="text" class="form-control" value="{{ old('license_cta') }}">
                        @if ($errors->has('license_cta'))
                            <span class="help-block"><strong>{{ $errors->first('license_cta') }}</strong></span>
                        @endif
                    </div>
                </div>
				<div class="form-group{{ $errors->has('license_vto')?' has-error':'' }}">
                    <label class="col-sm-2 col-sm-2 control-label">Vto. Licencia</label>
                    <div class="col-sm-10">
                        <input id="license_vto" name="license_vto" type="text" class="form-control" value="{{ old('license_vto') }}">
                        @if ($errors->has('license_vto'))
                            <span class="help-block"><strong>{{ $errors->first('license_vto') }}</strong></span>
                        @endif
                    </div>
                </div>
				<div class="form-group{{ $errors->has('file_permission') ? ' has-error' : '' }}">
                    <label class="col-sm-2 col-sm-2 control-label">Documento de Permisario</label>
                    <div class="col-sm-5">
                        <input id="file_permission" name="file_permission" type="file" value="{{ old('file_permission') }}" />
                        @if ($errors->has('file_permission'))
                            <span class="help-block"><strong>{{ $errors->first('file_permission') }}</strong></span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('password')?' has-error':'' }}">
                    <label class="col-sm-2 col-sm-2 control-label">Contraseña</label>
                    <div class="col-sm-10">
                        <input name="password" type="password" class="form-control">
                        @if ($errors->has('password'))
                            <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Confirmación de contraseña</label>
                    <div class="col-sm-10">
                        <input name="password_confirmation" type="password" class="form-control">
                    </div>
                </div>
                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
            </form>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="{{ asset('/assets/js/custom/users.js') }}"></script>
<script>
    document.getElementById('a_drivers').classList.add('active');
</script>
@endsection