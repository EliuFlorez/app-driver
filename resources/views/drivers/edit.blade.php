@extends('layouts.app')
@section('title')
Editar usuario
@endsection
@section('content')
<h3><i class="fa fa-angle-right"></i> Editar conductor</h3>
<div class="row mt">
    <div class="col-lg-12">
        <div class="form-panel">
            <h4 class="mb"><i class="fa fa-angle-right"></i> Información</h4>
            <form enctype="multipart/form-data" class="form-horizontal style-form" method="POST">
                {{ csrf_field() }}
				
				<div class="form-group{{ $errors->has('dni') ? ' has-error' : '' }}">
                    <label class="col-sm-2 col-sm-2 control-label">Cedula</label>
                    <div class="col-sm-10">
                        <input required value="{{ old('dni', $user->dni) }}" name="dni" type="text" class="form-control">
                        @if ($errors->has('dni'))
                            <span class="help-block"><strong>{{ $errors->first('dni') }}</strong></span>
                        @endif
                    </div>
                </div>
				
				<div class="form-group{{ $errors->has('rut') ? ' has-error' : '' }}">
                    <label class="col-sm-2 col-sm-2 control-label">RUT</label>
                    <div class="col-sm-10">
                        <input required value="{{ old('rut', $user->rut) }}" name="rut" type="text" class="form-control">
                        @if ($errors->has('rut'))
                            <span class="help-block"><strong>{{ $errors->first('rut') }}</strong></span>
                        @endif
                    </div>
                </div>
				
                <input type="hidden" name="_method" value="PUT" />
                <div class="form-group{{ $errors->has('name')?' has-error':'' }}">
                    <label class="col-sm-2 col-sm-2 control-label">Nombres y apellidos</label>
                    <div class="col-sm-10">
                        <input name="name" type="text" class="form-control" value="{{ $user->name }}">
                        @if ($errors->has('name'))
                            <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('cooperative')?' has-error':'' }}">
                    <label class="col-sm-2 col-sm-2 control-label">Cooperativa</label>
                    <div class="col-sm-10">
                        <select class="form-control" name="cooperative" id="cooperative" required>
                            @foreach ($cooperatives as $cooperative)
                                <option value="{{ $cooperative->id }}" {{ ($cooperative->id === old('cooperative'))?'selected':(($user->cooperative != null && $user->cooperative->id === $cooperative->id)?'selected':'') }}>
                                    {{ $cooperative->name }}
                                </option>
                            @endforeach
                        </select>
                        @if ($errors->has('cooperative'))
                            <span class="help-block"><strong>{{ $errors->first('cooperative') }}</strong></span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Foto actual/seleccionada</label>
                    <div class="col-sm-10">
                        <img id="image" class="profile-pic" src="{{ ($user->image == null || filter_var($user->image, FILTER_VALIDATE_URL)) ? url('assets/img/user.png') : url('/uploads/images/' . $user->image) }}" />
                    </div>
                </div>
                <div class="form-group{{ $errors->has('image')?' has-error':'' }}">
                    <label class="col-sm-2 col-sm-2 control-label">Seleccionar foto de perfil</label>
                    <div class="col-sm-5">
                        <input id="image" type="file" name="image" />
                        @if ($errors->has('image'))
                            <span class="help-block"><strong>{{ $errors->first('image') }}</strong></span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('email')?' has-error':'' }}">
                    <label class="col-sm-2 col-sm-2 control-label">Dirección de correo electrónico</label>
                    <div class="col-sm-10">
                        <input value="{{ old('email', $user->email) }}" name="email" type="email" class="form-control">
                        @if($errors->has('email'))
                            <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                        @endif
                    </div>
                </div>
				<div class="form-group{{ $errors->has('address')?' has-error':'' }}">
                    <label class="col-sm-2 col-sm-2 control-label">Dirección de domicilio</label>
                    <div class="col-sm-10">
                        <input value="{{ old('address', $user->address) }}" name="address" type="text" class="form-control">
                        @if($errors->has('address'))
                            <span class="help-block"><strong>{{ $errors->first('address') }}</strong></span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('phone')?' has-error':'' }}">
                    <label class="col-sm-2 col-sm-2 control-label">Teléfono</label>
                    <div class="col-sm-10">
                        <input name="phone" type="text" class="form-control" value="{{ old('phone', $user->phone) }}">
                        @if ($errors->has('phone'))
                            <span class="help-block"><strong>{{ $errors->first('phone') }}</strong></span>
                        @endif
                    </div>
                </div>
				<div class="form-group{{ $errors->has('cell')?' has-error':'' }}">
                    <label class="col-sm-2 col-sm-2 control-label">Celular</label>
                    <div class="col-sm-10">
                        <input name="cell" type="text" class="form-control" value="{{ old('cell', $user->cell) }}">
                        @if ($errors->has('cell'))
                            <span class="help-block"><strong>{{ $errors->first('cell') }}</strong></span>
                        @endif
                    </div>
                </div>
				<div class="form-group{{ $errors->has('number_bps')?' has-error':'' }}">
                    <label class="col-sm-2 col-sm-2 control-label">Número Certificado BPS</label>
                    <div class="col-sm-10">
                        <input id="number_bps" name="number_bps" type="text" class="form-control" value="{{ old('number_bps', $user->number_bps) }}">
                        @if ($errors->has('number_bps'))
                            <span class="help-block"><strong>{{ $errors->first('number_bps') }}</strong></span>
                        @endif
                    </div>
                </div>
				<div class="form-group{{ $errors->has('license_cta')?' has-error':'' }}">
                    <label class="col-sm-2 col-sm-2 control-label">Cta. Licencia</label>
                    <div class="col-sm-10">
                        <input id="license_cta" name="license_cta" type="text" class="form-control" value="{{ old('license_cta', $user->license_cta) }}">
                        @if ($errors->has('license_cta'))
                            <span class="help-block"><strong>{{ $errors->first('license_cta') }}</strong></span>
                        @endif
                    </div>
                </div>
				<div class="form-group{{ $errors->has('license_vto')?' has-error':'' }}">
                    <label class="col-sm-2 col-sm-2 control-label">Vto. Licencia</label>
                    <div class="col-sm-10">
                        <input id="license_vto" name="license_vto" type="text" class="form-control" value="{{ old('license_vto', $user->license_vto) }}">
                        @if ($errors->has('license_vto'))
                            <span class="help-block"><strong>{{ $errors->first('license_vto') }}</strong></span>
                        @endif
                    </div>
                </div>
				<div class="form-group{{ $errors->has('file_permission') ? ' has-error' : '' }}">
                    <label class="col-sm-2 col-sm-2 control-label">Documento de Permisario</label>
                    <div class="col-sm-5">
                        <input id="file_permission" name="file_permission" type="file" />
                        @if ($errors->has('file_permission'))
                            <span class="help-block"><strong>{{ $errors->first('file_permission') }}</strong></span>
                        @endif
						@if ($user->file_permission)
							<span><a href="{{ url('/uploads/permissions/' . $user->file_permission) }}" target="_blank" >Documento Click</span>
						@endif
                    </div>
                </div>
                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
            </form>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="{{ asset('/assets/js/custom/users.js') }}"></script>
<script>
    document.getElementById('a_drivers').classList.add('active');
</script>
@endsection