@extends('layouts.app')
@section('title')
Despachos
@endsection
@section('content')
<h3><i class="fa fa-angle-right"></i> Lista de despachos</h3>
<div class="row mt">
    <div class="col-lg-4 col-md-6 col-sm-12">
        <div class="form-panel">
            <form id="form" method="GET" action="{{ url('/roads/search') }}">
                <h4 class="mb"><i class="fa fa-angle-right"></i> Estado de la carrera</h4>
                <div class="form-group">
                    <div class="radio">
                        <label><input onclick="doSearch();" id="filter" type="radio" name="filter" value="P" checked/> Pendientes</label>
                    </div>
                    <div class="radio">
                        <label><input onclick="doSearch();" id="filter" type="radio" name="filter" value="R,E" /> En proceso</label>
                    </div>
                    <div class="radio">
                        <label><input onclick="doSearch();" id="filter" type="radio" name="filter" value="F"/> Finalizadas</label>
                    </div>
                    <div class="radio">
                        <label><input onclick="doSearch();" id="filter" type="radio" name="filter" value="C" /> Canceladas por el usuario</label>
                    </div>
                    <div class="radio">
                        <label><input onclick="doSearch();" id="filter" type="radio" name="filter" value="RV" /> Reservadas</label>
                    </div>
                </div>
                <h4 class="mb"><i class="fa fa-angle-right"></i> Fecha de la carrera</h4>
                <div class="form-group">
                    <div class="radio">
                        <label><input onclick="hideDates(true);doSearch();" type="radio" name="today" value="H" checked /> Hoy</label>
                    </div>
                    <div class="radio">
                        <label><input onclick="hideDates(false);doSearch();" type="radio" name="today" value="P" /> Personalizado</label>
                    </div>
                </div>
                <div class="form-group" id="div_from" style="display:none;">
                    <label>Desde</label>
                    <input onchange="doSearch();" value="{{ date('m/d/Y') }}" type="text" name="from" id="from" class="form-control" />
                </div>
                <div class="form-group" id="div_to" style="display:none;">
                    <label>Hasta</label>
                    <input onchange="doSearch();" value="{{ date('m/d/Y') }}" type="text" name="to" id="to" class="form-control" />
                </div>
                <h4 class="mb"><i class="fa fa-angle-right"></i> Filtro de busqueda</h4>
                <div class="form-group">
                    <label>Filtrar por</label>
                    <select onchange="hideSearchSection();doSearch();" class="form-control" name="option" id="option">
                        <option value="N" selected >Ninguno</option>
                        <option value="U">Unidad</option>
                        <option value="C">Cliente</option>
                    </select>
                </div>
                <div class="form-group" style="display:none;" id="div_search">
                    <div class="input-group">
                        <input id="search" type="text" class="form-control" placeholder="Escribe tu consulta...">
                        <span class="input-group-btn">
                            <button onclick="doSearch();" class="btn btn-primary" type="button"><i class="fa fa-search"></i></button>
                        </span>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="col-lg-8 col-md-6 col-sm-12">
        <div style="overflow:auto;height:440px;" class="content-panel" id="table">
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="{{ asset('/assets/js/custom/cab-rides.js') }}"></script>
<script>
    doSearch();
</script>
@endsection