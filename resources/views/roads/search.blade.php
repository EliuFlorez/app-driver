@if($roads->count() > 0)
    <h4 class="mb"><i class="fa fa-angle-right"></i> Resultados</h4>
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="form-group">
            <label><strong>Cantidad de carreras: </strong>{{ $roads->count() }}</label>
            <label><strong>Cantidad recaudada: </strong>{{ $roads->sum('price') }}</label>
        </div>
    </div>
    <div class="table-responsive">
        <section id="unseen">
            <table class="table table-bordered table-striped table-condensed text-center">
                <thead>
                    <tr>
                        <th>Cooperativa</th>
                        <th>Cliente</th>
                        @if($filter!='RV')
                            <th>Conductor</th>
                            <th>Cooperativa del conductor</th>
                            <th>Unidad</th>
                        @endif
                        <th>Inicio</th>
                        <th>Destino</th>
                        <th>Estado</th>
                        @if($filter!='RV')
                         <th>Tipo</th>
                        @endif
                        <th>Fecha de creación</th>
                        <th>Fecha de modificación</th>
                        @if($filter!='RV')
                            <th>Precio</th>
                        @endif
                        @if($filter=='RV')
                            <th>Editar</th>
                        @endif
                        <th>Cancelar</th>
                        @if(Auth::user()->role == 'S')
                            <th>Eliminar</th>
                        @endif
                        @if($filter=='P')
                            <th>Reenviar a varios</th>
                            <th>Reenviar a uno</th>
                        @endif
                        @if($filter=='RV')
                            <th>Enviar a varios</th>
                            <th>Enviar a uno</th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                @foreach ($roads as $road)
                    <tr class="{{ ($road->type==='RV' && ($road->status ==='E' || $road->status==='R'))?'warning':(($road->status === 'F')?'success':(($road->status === 'C')?'danger':(($road->status === 'R' || ($road->status === 'E'))?'info':'warning'))) }}">
                        <td>{{ ($road->cooperativa == null)?'-':$road->cooperativa->nombre }}</td>
                        <td>{{ ($road->user == null)?'-' : $road->user->name }}</td>
                        @if($filter!='RV')
                            <td>{{ ($road->driver == null)?'-':$road->driver->name }}</td>
                            <td>{{ (($road->driver != null && $road->driver->cooperativa != null))?($road->driver->cooperativa->nombre):'-' }}</td>
                            <td>{{ ($road->driver != null)?$road->driver->vehicle_number:'-' }}</td>
                        @endif
                        <td>{{ $road->location_address }}</td>
                        <td>{{ $road->destine_address }}</td>
                        <td>{{ ($road->status === 'P')?'Pendiente':(($road->status === 'R' || $road->status === 'E')?'En proceso':(($road->status === 'F')?'Finalizada':(($road->status === 'RV')?'Reservada':'Cancelada'))) }}</td>
                        @if($filter!='RV')
                             <td>{{ ($road->type == 'M')?'Manual':(($road->type == 'RV')?'Reservación':'Normal' )}}</td>
                        @endif
                        <td>{{ $road->created_at }}</td>
                        <td>{{ $road->updated_at }}</td>
                        @if($filter!='RV')
                            <td>{{ $road->price }}</td>
                        @endif
                        @if($filter=='RV')
                            <td><a href="{{ url('/reservas/' . $road->id) }}" class="btn btn-info"><i class="fa fa-edit"></i></a></td>
                        @endif
                        <td>
                            <button onclick="cancel('{{ url('/roads/' . $road->id . '/cancel') }}', this);" type="button" class="btn btn-warning" {{ ($road->status === 'C' || $road->status === 'F')?'disabled':'' }}><i class="fa fa-power-off"></i></button>
                        </td>
                        @if(Auth::user()->role == 'S')
                            <td>
                                <form onsubmit="return confirm('¿Está seguro que desea eliminar esta carrera?');" action="{{ url('/roads/' . $road->id) }}" method="POST">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="DELETE" />
                                    <button type="submit" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>
                                </form>
                            </td>
                        @endif
                        @if($filter=='P' || $filter=='RV' )
                            <td>
                               <button onclick="reenviarVarios('{{ url('/roads/' . $road->id . '/sendall') }}');" type="button" class="btn btn-primary"><i class="fa fa-reply-all"></i></button>
                            </td>
                            <td>
                               <button onclick="setPausaCabId('{{$road->id}}')" data-toggle="modal" data-target="#myModal_2" class="btn btn-info" type="button"><i class="fa fa-reply"></i></button>
                            </td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
            </table>
        </section>
    </div>
    <div class="modal" id="myModal_2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Consulta de conductor</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <input onkeyup="searchDrivers('{{ url('/drivers/search-drivers') }}', this.value);" id="driver_search" type="text" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <select id="select_drivers" class="form-control" multiple>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button onclick="reenviarUno('{{ url('/roads/sendone') }}');" type="button" class="btn btn-primary">Seleccionar</button>
                </div>
            </div>
        </div>
    </div>
@else
    <div class="alert alert-info">No hubo resultados.</div>
@endif