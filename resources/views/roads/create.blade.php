@extends('layouts.app')
@section('title')
Crear despacho
@endsection
@section('content')
<h3><i class="fa fa-angle-right"></i> Crear despacho</h3>
<div class="row mt">
    <div class="col-lg-12">
        <div class="form-panel">
            <h4 class="mb"><i class="fa fa-angle-right"></i> Información</h4>
            <form enctype="multipart/form-data" action="{{ url('/roads') }}" class="form-horizontal style-form" method="POST">
                {{ csrf_field() }}
                <div class="form-group{{ $errors->has('phone')?' has-error':'' }}">
                    <label class="col-sm-2 col-sm-2 control-label">Celular del cliente</label>
                    <div class="col-sm-10">
                        <input onblur="searchCustomer('{{ url('/users') }}' + '/' + this.value + '/phone');" value="{{ old('phone') }}" name="phone" type="text" class="form-control"/>
                        @if ($errors->has('phone'))
                            <span class="help-block"><strong>{{ $errors->first('phone') }}</strong></span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('name')?' has-error':'' }}">
                    <label class="col-sm-2 col-sm-2 control-label">Nombre del cliente</label>
                    <div class="col-sm-10">
                        <input id="name" value="{{ old('name') }}" name="name" type="text" class="form-control"/>
                        @if ($errors->has('name'))
                            <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('driver_id')?' has-error':'' }}">
                    <label class="col-sm-2 col-sm-2 control-label">Conductor (opcional)</label>
                    <div class="col-sm-10">
                        <input id="driver_id" type="hidden" name="driver_id" value="{{ old('driver_id') }}"/>
                        <div class="input-group">
                            <input id="driver" readonly value="{{ old('driver') }}" name="driver" type="text" class="form-control"/>
                            <span class="input-group-btn">
                                <button data-toggle="modal" data-target="#myModal" class="btn btn-primary" type="button"><i class="fa fa-search"></i></button>
                                <button onclick="removeDriver();" class="btn btn-danger" type="button"><i class="fa fa-power-off"></i></button>
                            </span>
                        </div>
                        @if ($errors->has('driver_id'))
                            <span class="help-block"><strong>{{ $errors->first('driver_id') }}</strong></span>
                        @endif
                    </div>
                </div>
                <div class="form-group {{ ($errors->has('location_address')||$errors->has('location_latitude')||$errors->has('location_longitude'))?' has-error':'' }}">
                    <label class="col-sm-2 col-sm-2 control-label">Dirección inicial</label>
                    <div class="col-sm-6">
                        <input value="{{ old('location_latitude') }}" type="hidden" id="location_latitude" name="location_latitude" />
                        <input value="{{ old('location_longitude') }}" type="hidden" id="location_longitude" name="location_longitude" />
                        <input  id="location_address" value="{{ old('location_address') }}" name="location_address" type="text" class="form-control" placeholder="Buscar ubicación..."/>
                        <div id="div_location_address_zone">
                            <select onchange="selectZone();" class="form-control chosen-select" id="location_address_zone" name="location_address_zone">
                                <option value="{{ old('location_address') }}" disabled selected hidden>Buscar zona...</option>
                                @foreach ($zones as $zona)
                                    <option value="{{ $zona->id }}">{{ $zona->descripcion }}</option>
                                @endforeach
                            </select>
                        </div>
                        @if ($errors->has('location_address'))
                            <span class="help-block"><strong>{{ $errors->first('location_address') }}</strong></span>
                        @endif
                    </div>
                    <div class="col-sm-2">
                            <div class="radio">
                                <label><input id="filter" type="radio" name="filter" onchange="changeFilter(this.value)" value="U" checked/> Ubicación</label>
                            </div>
                    </div>
                    <div class="col-sm-2">
                            <div class="radio">
                                <label><input  id="filter" type="radio" name="filter"  onchange="changeFilter(this.value)" value="Z" /> Zonas</label>
                            </div>
                    </div>
                </div>
                <div class="form-group{{ $errors->has('location_reference')?' has-error':'' }}">
                    <label class="col-sm-2 col-sm-2 control-label">Referencia inicial</label>
                    <div class="col-sm-10">
                        <input value="{{ old('location_reference') }}" name="location_reference" type="text" class="form-control"/>
                        @if ($errors->has('location_reference'))
                            <span class="help-block"><strong>{{ $errors->first('location_reference') }}</strong></span>
                        @endif
                    </div>
                </div>
                <h4 class="mb"><i class="fa fa-angle-right"></i> Mapa de localización del cliente</h4>
                <div class="cab-ride-map" id="mapLocation"></div>
                <div class="form-group {{ ($errors->has('location_longitude')||$errors->has('location_latitude'))?' has-error':'' }}">
                    <div class="col-sm-10">
                    @if ($errors->has('location_latitude') || $errors->has('location_longitude') )
                        <span class="help-block"><strong>{{ $errors->first('location_latitude') }}</strong></span>
                    @endif
                        </div>
                </div>
                <br/>
                <div class="form-group{{ $errors->has('destine_address')?' has-error':'' }}">
                    <label class="col-sm-2 col-sm-2 control-label">Dirección de destino</label>
                    <div class="col-sm-10">
                        <input id="destine_address" value="{{ old('destine_address') }}" name="destine_address" type="text" class="form-control"/>
                        @if ($errors->has('destine_address'))
                            <span class="help-block"><strong>{{ $errors->first('destine_address') }}</strong></span>
                        @endif
                    </div>
                </div>
                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Consulta</h4>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <input onkeyup="searchDrivers('{{ url('/drivers/search-drivers') }}', this.value);" id="driver_search" type="text" class="form-control"/>
                </div>
                <div class="form-group">
                    <select id="select_drivers" class="form-control" multiple>
                    </select>
                </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button onclick="selectDriver();" type="button" class="btn btn-primary">Seleccionar</button>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB249EeF8ARkbmlsjDDezz-gm3cLG3VO7M&libraries=places&callback=initAutocomplete" async defer></script>
<script src="{{ asset('/assets/js/custom/cab-rides.js') }}"></script>
<script>
	isPause=true;

	function selectZone()
	{
		borrarZona();
		var zona_id=document.getElementById("location_address_zone").value;
		path_zona=[];
		@foreach($zones as $zona)
			if(zona_id=='{{$zona->id}}')
			{
				input.value='{{$zona->descripcion}}';
				@foreach($zona->zona as $item)
				   path_zona.push({lat:parseFloat('{{$item["lat"]}}'),lng:parseFloat('{{$item["lng"]}}')});
				@endforeach
			}
		@endforeach

		if(path_zona.length>0)
		{
			line.setMap(map);
			locationLongitude.value = path_zona[0].lng;
			locationLatitude.value = path_zona[0].lat;
			generarZona(path_zona);
		}
		else
		{
			input.value=null;
			locationLongitude.value = null;
			locationLatitude.value = null;
			alert("No se encontró la ubicación de la zona seleccionada.");
		}
	}
</script>
@endsection