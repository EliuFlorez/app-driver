@extends('layouts.app')
@section('title')
Crear cliente
@endsection
@section('content')
<h3><i class="fa fa-angle-right"></i> Crear cliente</h3>
<div class="row mt">
    <div class="col-lg-12">
        <div class="form-panel">
            <h4 class="mb"><i class="fa fa-angle-right"></i> Información</h4>
            <form enctype="multipart/form-data" action="{{ url('/customers') }}" class="form-horizontal style-form" method="POST">
                {{ csrf_field() }}
                <div class="form-group{{ $errors->has('name')?' has-error':'' }}">
                    <label class="col-sm-2 control-label">Nombres y apellidos</label>
                    <div class="col-sm-10">
                        <input value="{{ old('name') }}" name="name" type="text" class="form-control">
                        @if ($errors->has('name'))
                            <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('phone')?' has-error':'' }}">
                    <label class="col-sm-2 control-label">Celular</label>
                    <div class="col-sm-10">
                        <input name="phone" type="text" class="form-control" value="{{ old('phone') }}">
                        @if ($errors->has('phone'))
                            <span class="help-block"><strong>{{ $errors->first('phone') }}</strong></span>
                        @endif
                    </div>
                </div>
                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
            </form>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="{{ asset('/assets/js/custom/users.js') }}"></script>
<script>
    isDriver(document.getElementById('role').value);
</script>
<script>
    document.getElementById('a_customers').classList.add('active');
</script>
@endsection