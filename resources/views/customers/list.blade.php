@extends('layouts.app')
@section('title')
Clientes
@endsection
@section('content')
<h3><i class="fa fa-angle-right"></i> Lista de clientes</h3>
<div class="row mt">
    <div class="col-lg-4">
        <div class="form-panel">
            <h4 class="mb"><i class="fa fa-angle-right"></i> Consulta</h4>
            <form method="GET" action="{{ url('/customers/search') }}" role="form">
                <div class="form-group">
                    <input name="search" type="text" class="form-control" id="search" placeholder="Nombre/Celular" value="{{ (isset($search))?$search:'' }}">
                </div>
                <div class="form-group">
                    <div class="radio">
                        <label><input onclick="this.form.submit();" type="radio" name="status" value="A" {{ (isset($status) && $status === 'A') ? 'checked' : '' }} /> Activos</label>
                    </div>
                    <div class="radio">
                        <label><input onclick="this.form.submit();" type="radio" name="status" value="I" {{ (isset($status) && $status === 'I') ? 'checked' : '' }} /> Inactivos</label>
                    </div>
                    <div class="radio">
                        <label><input onclick="this.form.submit();" type="radio" name="status" value="F" {{ (isset($status) && $status === 'F') ? 'checked' : '' }} /> Creados a través de Facebook</label>
                    </div>
                    <div class="radio">
                        <label><input onclick="this.form.submit();" type="radio" name="status" value="T" {{ ((isset($status) && $status === 'T') || !isset($status)) ? 'checked' : '' }}/> Todos</label>
                    </div>
                </div>
                <div class="form-group">
                    <label>Cantidad de clientes encontrados: </label>{{ $users->count() }} <br/>
                    <label>Clientes activos: </label>{{ App\User::where('status', true)->where('role', 'C')->get()->count() }} <br/>
                    <label>Clientes inactivos: </label>{{ App\User::where('status', false)->where('role', 'C')->get()->count() }} <br/>
                </div>
                <button type="submit" class="btn btn-theme">Buscar</button>
            </form>
        </div>
    </div>
    <div class="col-lg-8">
        <div class="content-panel">
            @if($users->count() > 0)
                <div class="table-responsive">
                    <section id="unseen">
                        <table class="table table-bordered table-striped table-condensed text-center">
                            <thead>
                                <tr>
                                    <th>Editar</th>
                                    <th>Nombres y apellidos</th>
                                    <th>Celular</th>
                                    <th>Fecha de creación</th>
                                    <th>Fecha de modificación</th>
                                    <th>Creado desde Facebook</th>
                                    <th>Estado</th>
                                    <th>Eliminar</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($users as $user)
                                <tr>
                                    <td><a href="{{ url('/customers/'. $user->id) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a></td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ ($user->phone != null)?$user->phone:'-' }}</td>
                                    <td>{{ $user->created_at }}</td>
                                    <td>{{ $user->updated_at }}</td>
                                    <td>
                                        <div class="col-lg-12">
                                            <div class="switch switch-square"
                                                data-on-label="<i class=' fa fa-check'></i>"
                                                data-off-label="<i class='fa fa-times'></i>">
                                                <input onchange="activateUser('{{ url("/users/" . $user->id . '/activate') }}', this.checked);" type="checkbox" {{ ($user->status == true)?'checked=""':'' }} />
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <form onsubmit="return confirm('¿Estás seguro de eliminar este usuario?');" method="POST" action="{{ url('/customers/' . $user->id) }}">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE" />
                                            <button type="submit" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $users->links() }}
                    </section>
                </div>
            @else
                <div class="alert alert-info">No hubo resultados.</div>
            @endif
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="{{ asset('/assets/js/custom/users.js') }}"></script>
<script>
    document.getElementById('a_customers').classList.add('active');
</script>
@endsection