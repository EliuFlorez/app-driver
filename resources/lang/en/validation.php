<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'The :attribute must be accepted.',
    'active_url'           => 'The :attribute is not a valid URL.',
    'after'                => 'The :attribute must be a date after :date.',
    'alpha'                => 'The :attribute may only contain letters.',
    'alpha_dash'           => 'The :attribute may only contain letters, numbers, and dashes.',
    'alpha_num'            => 'The :attribute may only contain letters and numbers.',
    'array'                => 'The :attribute must be an array.',
    'before'               => 'The :attribute must be a date before :date.',
    'between'              => [
        'numeric' => 'The :attribute must be between :min and :max.',
        'file'    => 'The :attribute must be between :min and :max kilobytes.',
        'string'  => 'The :attribute must be between :min and :max characters.',
        'array'   => 'The :attribute must have between :min and :max items.',
    ],
    'boolean'              => 'The :attribute field must be true or false.',
    'confirmed'            => 'The :attribute confirmation does not match.',
    'date'                 => 'The :attribute is not a valid date.',
    'date_format'          => 'The :attribute does not match the format :format.',
    'different'            => 'The :attribute and :other must be different.',
    'digits'               => 'The :attribute must be :digits digits.',
    'digits_between'       => 'The :attribute must be between :min and :max digits.',
    'dimensions'           => 'The :attribute has invalid image dimensions.',
    'distinct'             => 'The :attribute field has a duplicate value.',
    'email'                => 'The :attribute must be a valid email address.',
    'exists'               => 'The selected :attribute is invalid.',
    'file'                 => 'The :attribute must be a file.',
    'filled'               => 'The :attribute field is required.',
    'image'                => 'The :attribute must be an image.',
    'in'                   => 'The selected :attribute is invalid.',
    'in_array'             => 'The :attribute field does not exist in :other.',
    'integer'              => 'The :attribute must be an integer.',
    'ip'                   => 'The :attribute must be a valid IP address.',
    'json'                 => 'The :attribute must be a valid JSON string.',
    'max'                  => [
        'numeric' => 'The :attribute may not be greater than :max.',
        'file'    => 'The :attribute may not be greater than :max kilobytes.',
        'string'  => 'The :attribute may not be greater than :max characters.',
        'array'   => 'The :attribute may not have more than :max items.',
    ],
    'mimes'                => 'The :attribute must be a file of type: :values.',
    'mimetypes'            => 'The :attribute must be a file of type: :values.',
    'min'                  => [
        'numeric' => 'The :attribute must be at least :min.',
        'file'    => 'The :attribute must be at least :min kilobytes.',
        'string'  => 'The :attribute must be at least :min characters.',
        'array'   => 'The :attribute must have at least :min items.',
    ],
    'not_in'               => 'The selected :attribute is invalid.',
    'numeric'              => 'The :attribute must be a number.',
    'present'              => 'The :attribute field must be present.',
    'regex'                => 'The :attribute format is invalid.',
    'required'             => 'The :attribute field is required.',
    'required_if'          => 'The :attribute field is required when :other is :value.',
    'required_unless'      => 'The :attribute field is required unless :other is in :values.',
    'required_with'        => 'The :attribute field is required when :values is present.',
    'required_with_all'    => 'The :attribute field is required when :values is present.',
    'required_without'     => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same'                 => 'The :attribute and :other must match.',
    'size'                 => [
        'numeric' => 'The :attribute must be :size.',
        'file'    => 'The :attribute must be :size kilobytes.',
        'string'  => 'The :attribute must be :size characters.',
        'array'   => 'The :attribute must contain :size items.',
    ],
    'string'               => 'The :attribute must be a string.',
    'timezone'             => 'The :attribute must be a valid zone.',
    'unique'               => 'The :attribute has already been taken.',
    'uploaded'             => 'The :attribute failed to upload.',
    'url'                  => 'The :attribute format is invalid.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'email' => [
            'required' => 'El correo electrónico es requerido',
            'unique' => 'El correo electrónico ya ha sido registrado con anterioridad'
        ],
        'password' => [
            'required' => 'La contraseña es requerida',
            'min' => 'La contraseña debe contener 6 caracteres como mínimo',
            'confirmed' => 'La contraseña debe ser confirmada'
        ],
        'name' => [
            'required' => 'Se requiere el nombre',
            'max' => 'El nombre debe tener como máximo 255 caracteres'
        ],
        'nombre' => [
            'required' => 'Se requiere el nombre',
            'max' => 'El nombre debe tener como máximo 255 caracteres',
            'unique' => 'El nombre ya se encuentra registrado, pruebe con otro.'
        ],
        'description' => [
            'required' => 'La descripción es requerida',
            'max' => 'La descripcion debe tener como maximo 255 caracteres'
        ],
        'descripcion' => [
            'required' => 'La descripción es requerida',
            'max' => 'La descripcion debe tener como maximo 255 caracteres',
            'unique'=>'La descripción ya se encuentra en uso'
        ],
        'cooperativa_id' => [
            'required' => 'La cooperativa es requerida'
        ],
        'photo' => [
            'image' => 'Debe ser una imagen válida'
        ],
        'title' => [
            'required' => 'El título es requerido',
            'max' => 'El título debe tener 255 caracteres como máximo'
        ],
        'from' => [
            'date' => 'Desde debe tener un formato de fecha valido',
            'required_without' => 'El campo desde es requerido cuando no se ha marcado como publicación permanente',
            'before' => 'Desde debe tener una fecha menor a la descrita en el campo hasta',
            'required_if' => 'Desde es requerido cuando se consulta por rango de fechas',
            'required' => 'Desde es requerido'
        ],
        'to' => [
            'date' => 'Hasta debe tener un formato de fecha valido',
            'required_without' => 'El campo hasta es requerido cuando no se ha marcado como publicación permanente',
            'required_if' => 'Hasta es requerido cuando se consulta por rango de fechas',
            'to' => 'Hasta es requerido'
        ],
        'role' => [
            'required' => 'El rol es requerido'
        ],
        'latitude' => [
            'required' => 'La latitud es requerida',
            'numeric' => 'La latitud debe contener un valor numérico'
        ],
        'location_address' => [
            'required' => 'La dirección inicial es requerida',
        ],
        'location_latitude' => [
            'required' => 'La ubicación en el mapa es requerida',
        ],
        'location_longitude' => [
            'required' => 'La ubicación en el mapa es requerida',
        ],
        'location_reference' => [
            'required' => 'La referencia inicial es requerida',
        ],
        'destine_address' => [
            'required' => 'La dirección destino es requerida',
        ],
        'longitude' => [
            'required' => 'La longitud es requerida',
            'numeric' => 'La longitud debe contener un valor numérico'
        ],
        'current_password' => [
            'required' => 'La contraseña actual es requerida',
            'min' => 'La contraseña actual debe contener 6 caracteres como mínimo'
        ],
        'new_password' => [
            'required' => 'La nueva contraseña es requerida',
            'min' => 'La nueva contraseña debe contener 6 caracteres como mínimo',
            'confirmed' => 'La nueva contraseña debe ser confirmada'
        ],
        'gender' => [
            'required' => 'El género es requerido'
        ],
        'phone' => [
            'required' => 'El celular es requerido'
        ],
        'filter' => [
            'required' => 'El filtro es requerido'
        ],
        'user' => [
            'required' => 'Debe seleccionar un usuario de la lista'
        ],
        'users' => [
            'required_if' => 'Se requiere seleccionar al menos un usuario'
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
