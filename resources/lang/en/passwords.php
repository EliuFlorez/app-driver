<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Las contraseñas deben tener al menos 6 caracteres y requieren ser confirmadas.',
    'reset' => 'Tu cuenta ha sido recuperada',
    'sent' => 'Hemos enviado un link de confirmación a tu cuenta de email',
    'token' => 'El token de contraseña es inválido',
    'user' => "No se pudo encontrar usuario alguno con estas credenciales",

];
