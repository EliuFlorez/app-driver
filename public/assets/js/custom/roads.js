
var id_cab_actual=null;
var path_zona=[];

function setPausaCabId(id)
{
    id_cab_actual=id;
    isPause=true;
}

function reenviarVarios(url)
{
    if (confirm('¿Está seguro de reenviar esta carrera?'))
    {
        $.post(url, function (data) {
            alert('La carrera ha sido reenviada con éxito.');
            location.reload(true);
        }, 'json');
    }
}

function reenviarUno(url)
{
    if (confirm('¿Está seguro de reenviar esta carrera?'))
    {
        var select = $('#select_drivers');
        if (select.val() != null)
        {
           $.post(url, {
               id_cab:id_cab_actual,
               driver_id:select.val()
            },function (data) {
                if (data.envio)
                {
                    alert('La carrera ha sido enviada al taxista seleccionado.');
                    location.reload(true);
                }
                else
                {
                    alert('No se pudo enviar la carrera al taxista seleccionado.');
                    location.reload(true);
                }
            }, 'json');
        }
        else
            alert('No ha seleccionado a ningún taxista.');
    }
}

function cancel(url, button)
{
    if (confirm('¿Está seguro de cancelar este despacho?'))
    {
        $.post(url, function (data) {
            if (data != null)
            {
                location.reload(true);
            }
        }, 'json');
    }
}

function editar_reserva(url)
{
   $.post(url, function (data) {
      if (data != null)
      {
        location.reload(true);
      }
   }, 'json');
}

document.getElementById('a_roads').classList.add('active');
var gye = { lat : -2.145665, lng : -79.9024778 };
var mapLocation;
var locationMarker;
var geocoder;
var locationLatitude = document.getElementById('location_latitude');
var locationLongitude = document.getElementById('location_longitude');
var input = document.getElementById('location_address');
var input_zona =document.getElementById('div_location_address_zone');
var searchBox;
var marker;
var line;

function initAutocomplete() {

    map = new google.maps.Map(document.getElementById('mapLocation'), {
        center: {lat: -2.1613905698142006, lng: -79.91300582885742},
        zoom: 11,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    line = new google.maps.Polygon({
        strokeColor: '#f69f18',
        strokeOpacity: 0.8,
        strokeWeight: 3,
        fillColor: '#f69f18',
        fillOpacity: 0.35,
        map:map
    });

    marker = new google.maps.Marker({
        map : map
    });

    searchBox = new google.maps.places.SearchBox(input);

    searchBox.addListener('places_changed', function() {
        var places = searchBox.getPlaces();

        if (places.length == 0) {
            return;
        }
        var bounds = new google.maps.LatLngBounds();
        places.forEach(function(place) {
            var icon = {
                url: place.icon,
                size: new google.maps.Size(71, 71),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(25, 25)
            };

            locationLongitude.value = place.geometry.location.lng();
            locationLatitude.value = place.geometry.location.lat();

            var latLng = place.geometry.location;
            marker.setPosition(latLng);
            locationMarker=latLng;

            map.setCenter(latLng);
            map.setZoom(13);

            if (place.geometry.viewport) {
                bounds.union(place.geometry.viewport);
            } else {
                bounds.extend(latLng);
            }
        });
        map.fitBounds(bounds);
    });

    map.addListener('bounds_changed', function() {
        searchBox.setBounds(map.getBounds());
    });

    $('#location_address').bind("keypress", function(e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            return false;
        }
    });

    changeFilter(document.getElementById('filter').value);

}

function generarZona(path)
{
    line.setPath(path);
    line.setMap(map);
    map.setCenter(path[0]);
    map.setZoom(13);
}

function borrarZona()
{
    line.setMap(null);
}

function changeFilter(value)
{
    locationLongitude.value = null;
    locationLatitude.value = null;
    input.value=null;
    borrarZona();
   if(value=="U")
   {
       input_zona.style="display:none;";
       input.style="";
   }
   else
   {

       input_zona.style="";
       input.style="display:none;";
   }
}
function geocodeAddress(map, marker, address, latitudeInput, longitudeInput) {
  geocoder.geocode({'address': address}, function(results, status) {
    if (status === google.maps.GeocoderStatus.OK) {
      map.setCenter(results[0].geometry.location);
      latitudeInput.value = results[0].geometry.location.lat();
      longitudeInput.value = results[0].geometry.location.lng();
      marker.setPosition(results[0].geometry.location);
    } else if (status != google.maps.GeocoderStatus.ZERO_RESULTS && status != google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
      alert('Geocode was not successful for the following reason: ' + status);
    }
  });
}

function searchDrivers(url, search)
{
    $.get(url, {
        search : search
    }, function(data) {
        $('#select_drivers').empty();
        for (var i = 0; i < data.length; i++)
        {
            $('#select_drivers').append('<option value="' + data[i].id + '">' + data[i].name + '</option>');
        }
    }, 'json');
}

function selectDriver()
{
    var select = $('#select_drivers');
    if (select.val() != null)
    {
        var driver_id = document.getElementById('driver_id');
        var driver = document.getElementById('driver');
        driver_id.value = select.val();
        driver.value = select.text();
        $("#myModal").modal('hide');
    }
    else 
        alert('No ha seleccionado a ningún taxista.');
}
function removeDriver()
{
    document.getElementById('driver_id').value = null;
    document.getElementById('driver').value = null;
}
function searchCustomer(url)
{
    $.get(url, function (data) {
        if (data.name != null)
        {
            document.getElementById('name').value = data.name;
        }
    }, 'json');
}

$( function() {
    $( "#from" ).datepicker();
  } );
  $( function() {
    $( "#to" ).datepicker();
  } );


var isPause=false;


function doSearch()
{
    if(!isPause)
    {
        var action = document.getElementById('form').action;
        var filters = document.getElementsByName('filter');
        var filter;
        for(var i = 0; i < filters.length; i++)
            if (filters[i].checked == true)
                filter = filters[i].value;
        var from = document.getElementById('from').value;
        var to = document.getElementById('to').value;
        var dates = document.getElementsByName('today');
        var today;
        for (var i = 0; i < dates.length; i++)
            if (dates[i].checked)
                today = dates[i].value;
        var option = document.getElementById('option').value;
        var search = document.getElementById('search').value;
        var parameters = {
            filter : filter,
            from : from,
            to : to,
            today : today,
            option : option,
            search : search
        };
        $.post(action, parameters, function(data) {
            $('#table').empty();
            $('#table').append(data);
        });
    }
}
$(document).on('hide.bs.modal','#myModal_2', function () {
    isPause=false;
});
function hideSearchSection()
{
    var div_search = document.getElementById('div_search');
    var option = document.getElementById('option').value;
    if (option == 'N')
        div_search.style = 'display:none;';
    else 
        div_search.style = '';
}
function hideDates(hide)
{
    var div_from = document.getElementById('div_from');
    var div_to = document.getElementById('div_to');
    if (hide == true)
    {
        div_from.style = 'display:none;';
        div_to.style = 'display:none;';
    }
    else 
    {
        div_from.style = '';
        div_to.style = '';
    }
}
setInterval(doSearch, 15000);

$('#location_address_zone').chosen({ width : '100%' });
