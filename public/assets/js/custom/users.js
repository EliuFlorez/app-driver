function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#image').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$("#image").change(function(){
    readURL(this);
});
function activateUser(url, status)
{
    $.post(url, {
        status : (status)?1:0
    }, function (data) {
        if (data.error == true)
            alert('Hubo un error al realizar la acción solicitada.');
        else
        {
            if(status==1)
                alert('El conductor ha sido inhabilitado exitosamente.');
            else
                alert('El conductor ha sido habilitado exitosamente.');
        }
    }, 'json');
}
function hideDates(value)
{
    var div_from = document.getElementById('div_from');
    var div_to = document.getElementById('div_to');
    var from = document.getElementById('from');
    var to = document.getElementById('to');
    if (value == 'O')
    {
        div_from.style = '';
        div_to.style = '';
    }
    else 
    {
        div_from.style = 'display:none;';
        div_to.style = 'display:none';
        from.value = null;
        to.value = null;
    }
}