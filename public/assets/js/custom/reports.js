function enableGeneralView(radio)
{
    var div_all = document.getElementById('div_all');
    var div_list = document.getElementById('div_list');
    if (radio.value == 'G')
    {
        div_all.style = 'display:none;';
        div_list.style = 'display:none;';
    }
    else 
    {
        div_all.style = '';
        div_list.style = '';
    }
}
function selectAll(checked)
{
    var users = document.getElementsByName('users[]');
    for (var i = 0; i < users.length; i++)
    {
        users[i].checked = checked;
    }
}
function checkedUserEvent(checked)
{
    var users = document.getElementsByName('users[]');
    var count = 0;
    for (var i = 0; i < users.length; i++)
    {
        if (users[i].checked == true)
            count++;
    }
    document.getElementById('select_all').checked = (count == users.length);
}
function generateReport()
{
    var form = document.getElementById('form');
    form.action = '/reports/pdf';
    form.submit();
}
$( function() {
    $( "#from" ).datepicker();
  } );
  $( function() {
    $( "#to" ).datepicker();
  } );
document.getElementById('a_reports').classList.add('active');