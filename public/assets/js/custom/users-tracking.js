var map;
var gye = { lat : -33.4309221, lng : -55.7682438 };
var markers = [];
var geocoder;

function geolocate()
{
    if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var pos = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
      map.setCenter(pos);
    }, function() {
      alert('Error de rastreo.');
    });
  } else {
    alert('Tu navegador no permite geolocalizarte, te recomendamos utilizar uno actualizado.');
  }
}

function userInformationToHtml(id, name, image, status)
{
  var html = '<div class="col-lg-12 col-md-12 col-sm-12 text-center">' +
		'<h4>' + name + '</h4>' +
		'<p><strong>Estado: </strong>' + status + '</p>' +
		'<img class="img-square" src="' + image + '" width="80"/><br//><br//>' +
		'</div>';
  return html;
}

function addMarker(latLng, id, html, icon, url)
{
  var marker = new google.maps.Marker({
    title : id, 
    map : map,
    position : latLng,
    icon : icon
  });
  var info = new google.maps.InfoWindow({
    content : html
  });
  marker.addListener('click', function(){
    info.open(map, marker);
  });
  markers.push(marker);
  setInterval(updateMarkerPosition, 3000, marker, url, info);
}

function updateMarkerPosition(marker, url, info)
{
	var urlBase = 'http://udv.com.uy/app-driver/public';
  $.get(url, function (data) {
    marker.setPosition({
      lat : parseFloat(data.latitude),
      lng : parseFloat(data.longitude)
    });
    var icon = (data.availability == 'O') ? urlBase+'/assets/img/auto-amarillo.png' : ((data.availability == 'L') ? urlBase+'/assets/img/auto-verde.png' : urlBase+'/assets/img/auto-rojo.png');
    var image = (data.image == null) ? urlBase+'/assets/img/user.png' : urlBase+'/uploads/images/'+data.image;
    var status = (data.availability == 'O') ? 'Ocupado' : ((data.availability == 'L') ? 'Disponible' : 'Desconectado(a)');
    marker.setIcon(icon);
    info.setContent(userInformationToHtml(data.id, data.name, image, status));
  }, 'json');
}

function geocodeAddress(map, address) {
  geocoder.geocode({'address': address}, function(results, status) {
    if (status === google.maps.GeocoderStatus.OK) {
      map.setCenter(results[0].geometry.location);
      map.setZoom(18);
    } else if (status != google.maps.GeocoderStatus.ZERO_RESULTS && status != google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
      alert('Geocode was not successful for the following reason: ' + status);
    }
  });
}