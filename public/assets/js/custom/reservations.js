
var id_cab_actual=null;
var path_zona=[];
var direccion_locacion=null;
var edit=false;
var latitud;
var longitud;

var gye = { lat : -2.145665, lng : -79.9024778 };
var mapLocation;
var locationMarker;
var geocoder;
var locationLatitude = document.getElementById('location_latitude');
var locationLongitude = document.getElementById('location_longitude');
var input = document.getElementById('location_address');
var input_zona =document.getElementById('div_location_address_zone');
var searchBox;
var marker;
var line;

function initAutocomplete() {

    map = new google.maps.Map(document.getElementById('mapLocation'), {
        center: {lat: -2.1613905698142006, lng: -79.91300582885742},
        zoom: 11,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    line = new google.maps.Polygon({
        strokeColor: '#f69f18',
        strokeOpacity: 0.8,
        strokeWeight: 3,
        fillColor: '#f69f18',
        fillOpacity: 0.35,
        map:map
    });

    marker = new google.maps.Marker({
        map : map
    });

    searchBox = new google.maps.places.SearchBox(input);

    searchBox.addListener('places_changed', function() {
        var places = searchBox.getPlaces();

        if (places.length == 0) {
            return;
        }
        var bounds = new google.maps.LatLngBounds();
        places.forEach(function(place) {
            var icon = {
                url: place.icon,
                size: new google.maps.Size(71, 71),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(25, 25)
            };

            locationLongitude.value = place.geometry.location.lng();
            locationLatitude.value = place.geometry.location.lat();

            var latLng = place.geometry.location;
            marker.setPosition(latLng);
            locationMarker=latLng;

            map.setCenter(latLng);
            map.setZoom(13);

            if (place.geometry.viewport) {
                bounds.union(place.geometry.viewport);
            } else {
                bounds.extend(latLng);
            }
        });
        map.fitBounds(bounds);
    });

    map.addListener('bounds_changed', function() {
        searchBox.setBounds(map.getBounds());
    });

    $('#location_address').bind("keypress", function(e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            return false;
        }
    });
    changeFilter(document.getElementById('filter').value);

    document.getElementById("location_address").value=direccion_locacion;

    if(edit==true)
    {
        var edit_ubication = {lat:parseFloat(latitud),lng:parseFloat(longitud)};
        marker.setPosition(edit_ubication);
        map.setCenter(edit_ubication);
        map.setZoom(15);
        document.getElementById("location_latitude").value=latitud;
        document.getElementById("location_longitude").value=longitud;
    }

}

function generarZona(path)
{
    line.setPath(path);
    line.setMap(map);
    map.setCenter(path[0]);
    map.setZoom(13);
}

function borrarZona()
{
    line.setMap(null);
}

function cancel(url, button)
{
    if (confirm('¿Está seguro de cancelar esta reservación?'))
    {
        $.post(url, function (data) {
            if (data != null)
            {
                location.reload(true);
            }
        }, 'json');
    }
}

function searchCustomer(url)
{
    $.get(url, function (data) {
        if (data.name != null)
        {
            document.getElementById('name').value = data.name;
        }
    }, 'json');
}


function changeFilter(value)
{
    locationLongitude.value = null;
    locationLatitude.value = null;
    input.value=null;
    borrarZona();
    if(value=="U")
    {
        input_zona.style="display:none;";
        input.style="";
    }
    else
    {
        input_zona.style="";
        input.style="display:none;";
    }
}

function geocodeAddress(map, marker, address, latitudeInput, longitudeInput) {
    geocoder.geocode({'address': address}, function(results, status) {
        if (status === google.maps.GeocoderStatus.OK) {
            map.setCenter(results[0].geometry.location);
            latitudeInput.value = results[0].geometry.location.lat();
            longitudeInput.value = results[0].geometry.location.lng();
            marker.setPosition(results[0].geometry.location);
        } else if (status != google.maps.GeocoderStatus.ZERO_RESULTS && status != google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
            alert('Geocode was not successful for the following reason: ' + status);
        }
    });
}

$('#location_address_zone').chosen({ width : '100%' });
