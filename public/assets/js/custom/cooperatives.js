/**
 * Created by José Daniel on 10/12/2016.
 */

function activateCooperativa(url, status)
{
    $.post(url, {
        status : (status)?1:0
    }, function (data) {
        if (data.error == true)
            alert('Hubo un error al realizar la acción solicitada.');
    }, 'json');
}

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#image').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

$("#image").change(function(){
    readURL(this);
});
