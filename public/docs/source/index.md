---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.

Status: C = Client, T = Driver, E = Start, P = Pending, R = Road, F = Finish

Availability: L = available, O = busy, D = disconnected

URL: http://udv.com.uy/app-driver/pubic/api/*

[Get Postman Collection](http://localhost:8000/docs/collection.json)
<!-- END_INFO -->

#general
<!-- START_a925a8d22b3615f12fca79456d286859 -->
## api/auth/login

> Example request:

```bash
curl -X POST "http://localhost:8000/api/auth/login" \
-H "Accept: application/json" \
    -d "email"="mdibbert@example.org" \
    -d "password"="eveniet" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/auth/login",
    "method": "POST",
    "data": {
        "email": "mdibbert@example.org",
        "password": "eveniet"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "token_type": "Bearer",
    "expires_in": 31536000,
    "access_token": "eyJ0eXAiOiJK...",
    "refresh_token": "def502003a67..."
}
```

### HTTP Request
`POST api/auth/login`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    email | email |  required  | 
    password | string |  required  | Minimum: `6`

<!-- END_a925a8d22b3615f12fca79456d286859 -->

<!-- START_994af8f47e3039ba6d6d67c09dd9e415 -->
## api/auth/refresh

> Example request:

```bash
curl -X POST "http://localhost:8000/api/auth/refresh" \
-H "Accept: application/json" \
    -d "refresh_token"="ipsum" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/auth/refresh",
    "method": "POST",
    "data": {
        "refresh_token": "ipsum"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "token_type": "Bearer",
    "expires_in": 31536000,
    "access_token": "eyJ0eXAiOiJKV1...",
    "refresh_token": "def50200438cb9..."
}
```

### HTTP Request
`POST api/auth/refresh`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    refresh_token | string |  required  | 

<!-- END_994af8f47e3039ba6d6d67c09dd9e415 -->

<!-- START_9b5a93b456cff16271845bca3f7d0e77 -->
## api/register/users

> Example request:

```bash
curl -X POST "http://localhost:8000/api/register/users" \
-H "Accept: application/json" \
    -d "name"="quisquam" \
    -d "email"="kevin55@example.net" \
    -d "password"="quisquam" \
    -d "password_confirmation"="quisquam" \
    -d "phone"="quisquam" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/register/users",
    "method": "POST",
    "data": {
        "name": "quisquam",
        "email": "kevin55@example.net",
        "password": "quisquam",
        "password_confirmation": "quisquam",
        "phone": "quisquam"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": false,
    "user": {
        "name": "quisquam",
        "email": "kevin55@example.net",
        "phone": "55555555",
        "role": "C",
        "status": true,
        "updated_at": "2017-11-19 11:36:37",
        "created_at": "2017-11-19 11:36:37",
        "id": 6
    }
}
```

### HTTP Request
`POST api/register/users`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    name | string |  required  | Maximum: `255`
    email | email |  required  | Maximum: `255`
    password | string |  required  | Minimum: `6`
    password_confirmation | string |  required  | Minimum: `6`
    phone | string |  required  | Maximum: `255`

<!-- END_9b5a93b456cff16271845bca3f7d0e77 -->

<!-- START_8cca75bad19bdf11fc9fdfdb8a4f2d6c -->
## api/register/drivers

> Example request:

```bash
curl -X POST "http://localhost:8000/api/register/drivers" \
-H "Accept: application/json" \
    -d "dni"="555555" \
    -d "rut"="555555-5" \
    -d "name"="ab" \
    -d "email"="gutmann.conrad@example.com" \
    -d "password"="123456" \
	-d "password_confirmation"="123456" \
    -d "address"="Home" \
    -d "phone"="55555555" \
    -d "cell"="55555555" \
    -d "number_bps"="55555555" \
    -d "license_cta"="none" \
    -d "license_vto"="none" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/register/drivers",
    "method": "POST",
    "data": {
        "dni": "555555",
        "rut": "555555-5",
        "name": "ab",
        "email": "gutmann.conrad@example.com",
        "password": "123456",
		"password_confirmation": "123456",
        "address": "Home",
        "phone": "55555555",
        "cell": "55555555",
        "number_bps": "55555555",
        "license_cta": "none",
        "license_vto": "none"
	},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": false,
    "user": {
        "dni": "555555",
        "rut": "555555-5",
        "name": "ab",
        "email": "gutmann.conrad@example.com",
        "address": "Home",
		"phone": "55555555",
        "cell": "55555555",
		"number_bps": "55555555",
        "license_cta": "none",
        "license_vto": "none",
        "role": "T",
        "status": true,
        "updated_at": "2017-11-19 11:36:37",
        "created_at": "2017-11-19 11:36:37",
        "id": 6
    }
}
```

### HTTP Request
`POST api/register/drivers`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    dni | string |  required  | Maximum: `255`
    rut | string |  required  | Maximum: `255`
    name | string |  required  | Maximum: `255`
    email | email |  required  | Maximum: `255`
    password | string |  required  | Minimum: `6`
	password_confirmation | string |  required  | Minimum: `6`
    address | string |  required  | Maximum: `255`
    phone | string |  required  | Maximum: `255`
    cell | string |  required  | Maximum: `255`
    number_bps | string |  required  | Maximum: `255`
    license_cta | string |  required  | Maximum: `255`
    license_vto | string |  required  | Maximum: `255`

<!-- END_8cca75bad19bdf11fc9fdfdb8a4f2d6c -->

<!-- START_8cca75bad19bdf11fc9fdfdb8a4f2d11 -->
## api/register/web

> Example request:

```bash
curl -X POST "http://localhost:8000/api/register/web" \
-H "Accept: application/json" \
    -d "dni"="555555" \
    -d "rut"="555555-5" \
    -d "name"="ab" \
    -d "email"="gutmann.conrad@example.com" \
    -d "address"="Home" \
    -d "phone"="55555555" \
    -d "cell"="55555555" \
    -d "number_bps"="55555555" \
    -d "license_cta"="none" \
    -d "license_vto"="none" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/register/web",
    "method": "POST",
    "data": {
        "dni": "555555",
        "rut": "555555-5",
        "name": "ab",
        "email": "gutmann.conrad@example.com",
        "address": "Home",
        "phone": "55555555",
        "cell": "55555555",
        "number_bps": "55555555",
        "license_cta": "none",
        "license_vto": "none"
	},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": false,
    "user": {
        "dni": "555555",
        "rut": "555555-5",
        "name": "ab",
        "email": "gutmann.conrad@example.com",
        "address": "Home",
		"phone": "55555555",
        "cell": "55555555",
		"number_bps": "55555555",
        "license_cta": "none",
        "license_vto": "none",
        "role": "T",
        "status": true,
        "updated_at": "2017-11-19 11:36:37",
        "created_at": "2017-11-19 11:36:37",
        "id": 6
    }
}
```

### HTTP Request
`POST api/register/web`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    dni | string |  required  | Maximum: `255`
    rut | string |  required  | Maximum: `255`
    name | string |  required  | Maximum: `255`
    email | email |  required  | Maximum: `255`
    address | string |  required  | Maximum: `255`
    phone | string |  required  | Maximum: `255`
    cell | string |  required  | Maximum: `255`
    number_bps | string |  required  | Maximum: `255`
    license_cta | string |  required  | Maximum: `255`
    license_vto | string |  required  | Maximum: `255`

<!-- END_8cca75bad19bdf11fc9fdfdb8a4f2d11 -->

<!-- START_593e56ca377547c38270e1a0698a6c13 -->
## api/users/{id}/user-road

> Example request:

```bash
curl -X GET "http://localhost:8000/api/users/{id}/user-road" \
-H "Accept: application/json" \
-H "Authorization: Bearer eyJ0eXAiOiJ..." \
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/users/{id}/user-road",
    "method": "GET",
    "headers": {
        "accept": "application/json",
		"Authorization": "Bearer eyJ0eXAiOiJ..."
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
[
    {
        "id": 8,
        "user_id": 6,
        "location_latitude": "-33.4309221",
        "location_longitude": "-55.7682438",
        "driver_id": 7,
        "location_address": "Home",
        "location_reference": "Home",
        "price": 0,
        "destine_address": "Home",
        "route": null,
        "status": "R",
        "type": "N",
        "reservation_date": null,
        "anticipation_send": null,
        "cooperative_id": null,
        "job_id": null,
        "created_at": "2017-11-19 12:35:27",
        "updated_at": "2017-11-19 14:00:15",
        "deleted_at": null
    }
]
```

### HTTP Request
`GET api/users/{id}/user-road`

`HEAD api/users/{id}/user-road`


<!-- END_593e56ca377547c38270e1a0698a6c13 -->

<!-- START_fcd94d697a5cd91d130061cbf1323aed -->
## api/users/{id}/driver-road

> Example request:

```bash
curl -X GET "http://localhost:8000/api/users/{id}/driver-road" \
-H "Accept: application/json" \
-H "Authorization: Bearer eyJ0eXAiOiJ..." \
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/users/{id}/driver-road",
    "method": "GET",
    "headers": {
        "accept": "application/json",
		"Authorization": "Bearer eyJ0eXAiOiJ..."
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
[
    {
        "id": 8,
        "user_id": 6,
        "location_latitude": "-33.4309221",
        "location_longitude": "-55.7682438",
        "driver_id": 7,
        "location_address": "Home",
        "location_reference": "Home",
        "price": 0,
        "destine_address": "Home",
        "route": null,
        "status": "R",
        "type": "N",
        "reservation_date": null,
        "anticipation_send": null,
        "cooperative_id": null,
        "job_id": null,
        "created_at": "2017-11-19 12:35:27",
        "updated_at": "2017-11-19 14:00:15",
        "deleted_at": null
    }
]
```

### HTTP Request
`GET api/users/{id}/driver-road`

`HEAD api/users/{id}/driver-road`


<!-- END_fcd94d697a5cd91d130061cbf1323aed -->

<!-- START_f253534aa8d9a64dc4f37137ccbd9e4c -->
## api/users/{id}/update-position

> Example request:

```bash
curl -X POST "http://localhost:8000/api/users/{id}/update-position" \
-H "Accept: application/json" \
-H "Authorization: Bearer eyJ0eXAiOiJ..." \
    -d "latitude"="8750199" \
    -d "longitude"="8750199" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/users/{id}/update-position",
    "method": "POST",
    "data": {
        "latitude": 8750199,
        "longitude": 8750199
},
    "headers": {
        "accept": "application/json",
		"Authorization": "Bearer eyJ0eXAiOiJ..."
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": false,
    "user": {
        "id": 6,
        "name": "demo",
        "email": "demo@demo.com",
        "device_id": null,
        "cooperativa_id": null,
        "role": "C",
        "status": "1",
        "availability": null,
        "image": null,
        "latitude": "-33.4309222",
        "longitude": "-55.7682434",
        "phone": "55555555",
        "trademark": null,
        "license_plate": null,
        "created_at": "2017-11-19 11:36:37",
        "updated_at": "2017-11-19 14:22:45",
        "deleted_at": null
    }
}
```

### HTTP Request
`POST api/users/{id}/update-position`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    latitude | numeric |  required  | 
    longitude | numeric |  required  | 

<!-- END_f253534aa8d9a64dc4f37137ccbd9e4c -->

<!-- START_5ed06991faf77333124e8fa232178a63 -->
## api/users/{id}/device

> Example request:

```bash
curl -X POST "http://localhost:8000/api/users/{id}/device" \
-H "Accept: application/json" \
-H "Authorization: Bearer eyJ0eXAiOiJ..." \
    -d "device_id"="id" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/users/{id}/device",
    "method": "POST",
    "data": {
        "device_id": "id"
},
    "headers": {
        "accept": "application/json",
		"Authorization": "Bearer eyJ0eXAiOiJ..."
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": false,
    "user": {
        "id": 6,
        "name": "demo",
        "email": "demo@demo.com",
        "device_id": "7",
        "cooperativa_id": null,
        "role": "C",
        "status": "1",
        "availability": null,
        "image": null,
        "latitude": "-33.4309222",
        "longitude": "-55.7682434",
        "phone": "55555555",
        "trademark": null,
        "license_plate": null,
        "created_at": "2017-11-19 11:36:37",
        "updated_at": "2017-11-19 14:24:55",
        "deleted_at": null
    }
}
```

### HTTP Request
`POST api/users/{id}/device`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    device_id | string |  required  | 

<!-- END_5ed06991faf77333124e8fa232178a63 -->

<!-- START_70457f3d1a0e3f4c118f3532f88e5a46 -->
## api/users/{id}/history

> Example request:

```bash
curl -X GET "http://localhost:8000/api/users/{id}/history" \
-H "Accept: application/json" \
-H "Authorization: Bearer eyJ0eXAiOiJ..." \
    -d "from"="2002-06-16" \
    -d "to"="2002-06-16" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/users/{id}/history",
    "method": "GET",
    "data": {
        "from": "2002-06-16",
        "to": "2002-06-16"
},
    "headers": {
        "accept": "application/json",
		"Authorization": "Bearer eyJ0eXAiOiJ..."
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": false,
    "roads": [
        {
            "id": 8,
            "user_id": 6,
            "location_latitude": "-33.4309221",
            "location_longitude": "-55.7682438",
            "driver_id": 7,
            "location_address": "Home",
            "location_reference": "Home",
            "price": 0,
            "destine_address": "Home",
            "route": null,
            "status": "E",
            "type": "N",
            "reservation_date": null,
            "anticipation_send": null,
            "cooperative_id": null,
            "job_id": null,
            "created_at": "2017-11-19 12:35:27",
            "updated_at": "2017-11-19 14:00:15",
            "deleted_at": null,
            "user": {
                "id": 6,
                "name": "demo",
                "email": "demo@demo.com",
                "device_id": 7,
                "cooperativa_id": null,
                "role": "C",
                "status": "1",
                "availability": null,
                "image": null,
                "latitude": "-33.4309222",
                "longitude": "-55.7682434",
                "phone": "55555555",
                "trademark": null,
                "license_plate": null,
                "created_at": "2017-11-19 11:36:37",
                "updated_at": "2017-11-19 14:24:55",
                "deleted_at": null
            }
        }
    ]
}
```

### HTTP Request
`GET api/users/{id}/history`

`HEAD api/users/{id}/history`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    from | date |  required  | 
    to | date |  required  | 

<!-- END_70457f3d1a0e3f4c118f3532f88e5a46 -->

<!-- START_c4376aca7aed34b6f4637389cd585985 -->
## api/users/set-password

> Example request:

```bash
curl -X POST "http://localhost:8000/api/users/set-password" \
-H "Accept: application/json" \
-H "Authorization: Bearer eyJ0eXAiOiJ..." \
    -d "current_password"="et" \
    -d "new_password"="et" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/users/set-password",
    "method": "POST",
    "data": {
        "current_password": "et",
        "new_password": "et"
},
    "headers": {
        "accept": "application/json",
		"Authorization": "Bearer eyJ0eXAiOiJ..."
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": false,
    "user": {
        "id": 6,
        "name": "demo",
        "email": "demo@demo.com",
        "device_id": 7,
        "cooperativa_id": null,
        "role": "C",
        "status": "1",
        "availability": null,
        "image": null,
        "latitude": "-33.4309222",
        "longitude": "-55.7682434",
        "phone": "55555555",
        "trademark": null,
        "license_plate": null,
        "created_at": "2017-11-19 11:36:37",
        "updated_at": "2017-11-19 14:58:49",
        "deleted_at": null
    }
}
```

### HTTP Request
`POST api/users/set-password`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    current_password | string |  required  | Minimum: `6`
    new_password | string |  required  | Minimum: `6` Must have a different value than parameter: `current_password`

<!-- END_c4376aca7aed34b6f4637389cd585985 -->

<!-- START_be3c726caa960bce4fc209bae0800523 -->
## api/users/available

> Example request:

```bash
curl -X POST "http://localhost:8000/api/users/available" \
-H "Accept: application/json" \
-H "Authorization: Bearer eyJ0eXAiOiJ..." \
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/users/available",
    "method": "POST",
    "headers": {
        "accept": "application/json",
		"Authorization": "Bearer eyJ0eXAiOiJ..."
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "status": "L"
}
```

### HTTP Request
`POST api/users/available`


<!-- END_be3c726caa960bce4fc209bae0800523 -->

<!-- START_96f851204c657ecdc436d23e7d13fcd5 -->
## api/users/busy

> Example request:

```bash
curl -X POST "http://localhost:8000/api/users/busy" \
-H "Accept: application/json" \
-H "Authorization: Bearer eyJ0eXAiOiJ..." \
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/users/busy",
    "method": "POST",
    "headers": {
        "accept": "application/json",
		"Authorization": "Bearer eyJ0eXAiOiJ..."
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "status": "O"
}
```

### HTTP Request
`POST api/users/busy`


<!-- END_96f851204c657ecdc436d23e7d13fcd5 -->

<!-- START_ee12835cb84740207b14762a02cdccaf -->
## api/users/disconnected

> Example request:

```bash
curl -X POST "http://localhost:8000/api/users/disconnected" \
-H "Accept: application/json" \
-H "Authorization: Bearer eyJ0eXAiOiJ..." \
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/users/disconnected",
    "method": "POST",
    "headers": {
        "accept": "application/json",
		"Authorization": "Bearer eyJ0eXAiOiJ..."
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "status": "D"
}
```

### HTTP Request
`POST api/users/disconnected`


<!-- END_ee12835cb84740207b14762a02cdccaf -->

<!-- START_c4e7df548ef65736780dd4ce0402829f -->
## api/users/nearest-drivers

> Example request:

```bash
curl -X POST "http://localhost:8000/api/users/nearest-drivers" \
-H "Accept: application/json" \
-H "Authorization: Bearer eyJ0eXAiOiJ..." \
    -d "latitude"="218102512" \
    -d "longitude"="218102512" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/users/nearest-drivers",
    "method": "POST",
    "data": {
        "latitude": 218102512,
        "longitude": 218102512
},
    "headers": {
        "accept": "application/json",
		"Authorization": "Bearer eyJ0eXAiOiJ..."
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
[
    {
        "id": 7,
        "name": "driver",
        "email": "driver@udv.com",
        "device_id": null,
        "cooperativa_id": 1,
        "role": "T",
        "status": "P",
        "availability": "L",
        "image": null,
        "latitude": "-33.4309221",
        "longitude": "-55.7682438",
        "phone": "5555555",
        "trademark": null,
        "license_plate": null,
        "created_at": "2017-11-19 11:56:46",
        "updated_at": "2017-11-19 13:51:46",
        "deleted_at": null
    }
]
```

### HTTP Request
`POST api/users/nearest-drivers`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    latitude | numeric |  required  | 
    longitude | numeric |  required  | 

<!-- END_c4e7df548ef65736780dd4ce0402829f -->

<!-- START_da5727be600e4865c1b632f7745c8e91 -->
## api/users

> Example request:

```bash
curl -X GET "http://localhost:8000/api/users" \
-H "Accept: application/json" \
-H "Authorization: Bearer eyJ0eXAiOiJ..." \
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/users",
    "method": "GET",
    "headers": {
        "accept": "application/json",
		"Authorization": "Bearer eyJ0eXAiOiJ..."
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": false,
    "user": {
        "id": 5,
        "name": "test",
        "email": "test@test.com",
        "device_id": null,
        "cooperativa_id": null,
        "role": "C",
        "status": "1",
        "availability": null,
        "image": null,
        "latitude": null,
        "longitude": null,
        "phone": "5555555",
        "trademark": null,
        "license_plate": null,
        "created_at": "2017-11-19 09:31:29",
        "updated_at": "2017-11-19 09:31:29",
        "deleted_at": null
    },
    "roads": null
}
```

### HTTP Request
`GET api/users`

`HEAD api/users`


<!-- END_da5727be600e4865c1b632f7745c8e91 -->

<!-- START_48a3115be98493a3c866eb0e23347262 -->
## api/users/{user}

> Example request:

```bash
curl -X PUT "http://localhost:8000/api/users/{user}" \
-H "Accept: application/json" \
-H "Authorization: Bearer eyJ0eXAiOiJ..." \
    -d "name"="cum" \
    -d "email"="ole99@example.net" \
    -d "phone"="cum" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/users/{user}",
    "method": "PUT",
    "data": {
        "name": "cum",
        "email": "ole99@example.net",
        "phone": "cum"
},
    "headers": {
        "accept": "application/json",
		"Authorization": "Bearer eyJ0eXAiOiJ..."
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`PUT api/users/{user}`

`PATCH api/users/{user}`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    name | string |  required  | Maximum: `255`
    email | email |  required  | Maximum: `255`
    phone | string |  required  | Maximum: `255`

<!-- END_48a3115be98493a3c866eb0e23347262 -->

<!-- START_d2db7a9fe3abd141d5adbc367a88e969 -->
## api/users/{user}

> Example request:

```bash
curl -X DELETE "http://localhost:8000/api/users/{user}" \
-H "Accept: application/json" \
-H "Authorization: Bearer eyJ0eXAiOiJ..." \
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/users/{user}",
    "method": "DELETE",
    "headers": {
        "accept": "application/json",
		"Authorization": "Bearer eyJ0eXAiOiJ..."
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`DELETE api/users/{user}`


<!-- END_d2db7a9fe3abd141d5adbc367a88e969 -->

<!-- START_a7bc1572c9765146960abc76430dce8b -->
## api/drivers

> Example request:

```bash
curl -X GET "http://localhost:8000/api/drivers" \
-H "Accept: application/json" \
-H "Authorization: Bearer eyJ0eXAiOiJ..." \
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/drivers",
    "method": "GET",
    "headers": {
        "accept": "application/json",
		"Authorization": "Bearer eyJ0eXAiOiJ..."
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
[
    {
        "id": 8,
        "user_id": 6,
        "location_latitude": "-33.4309221",
        "location_longitude": "-55.7682438",
        "driver_id": null,
        "location_address": "Home",
        "location_reference": "Home",
        "price": 0,
        "destine_address": "Home",
        "route": null,
        "status": "P",
        "type": "N",
        "reservation_date": null,
        "anticipation_send": null,
        "cooperative_id": null,
        "job_id": null,
        "created_at": "2017-11-19 12:35:27",
        "updated_at": "2017-11-19 12:35:27",
        "deleted_at": null,
        "driver": null
    }
]
```

### HTTP Request
`GET api/drivers`

`HEAD api/drivers`


<!-- END_a7bc1572c9765146960abc76430dce8b -->

<!-- START_a90dbfa952a2d00ebd5881ca8b6b9011 -->
## api/drivers

> Example request:

```bash
curl -X POST "http://localhost:8000/api/drivers" \
-H "Accept: application/json" \
-H "Authorization: Bearer eyJ0eXAiOiJ..." \
    -d "user_id"="et" \
    -d "location_latitude"="952556" \
    -d "location_longitude"="952556" \
    -d "location_address"="et" \
    -d "location_reference"="et" \
    -d "destine_address"="et" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/drivers",
    "method": "POST",
    "data": {
        "user_id": "et",
        "location_latitude": 952556,
        "location_longitude": 952556,
        "location_address": "et",
        "location_reference": "et",
        "destine_address": "et"
},
    "headers": {
        "accept": "application/json",
		"Authorization": "Bearer eyJ0eXAiOiJ..."
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": false,
    "road": {
        "user_id": "6",
        "location_latitude": "-33.4309221",
        "location_longitude": "-55.7682438",
        "location_address": "Home",
        "location_reference": "Home",
        "destine_address": "Home",
        "price": 0,
        "status": "P",
        "type": "N",
        "updated_at": "2017-11-19 12:14:59",
        "created_at": "2017-11-19 12:14:59",
        "id": 2
    }
}
```

### HTTP Request
`POST api/drivers`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    user_id | string |  required  | Valid user id
    location_latitude | numeric |  required  | 
    location_longitude | numeric |  required  | 
    location_address | string |  required  | Maximum: `255`
    location_reference | string |  required  | Maximum: `255`
    destine_address | string |  required  | Maximum: `255`

<!-- END_a90dbfa952a2d00ebd5881ca8b6b9011 -->

<!-- START_f677a4f7e42debbdf07bd71717ae2d2d -->
## api/drivers/{id}

> Example request:

```bash
curl -X GET "http://localhost:8000/api/drivers/{id}" \
-H "Accept: application/json" \
-H "Authorization: Bearer eyJ0eXAiOiJ..." \
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/drivers/{id}",
    "method": "GET",
    "headers": {
        "accept": "application/json",
		"Authorization": "Bearer eyJ0eXAiOiJ..."
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "id": 8,
    "user_id": 6,
    "location_latitude": "-33.4309221",
    "location_longitude": "-55.7682438",
    "driver_id": null,
    "location_address": "Home",
    "location_reference": "Home",
    "price": 0,
    "destine_address": "Home",
    "route": null,
    "status": "P",
    "type": "N",
    "reservation_date": null,
    "anticipation_send": null,
    "cooperative_id": null,
    "job_id": null,
    "created_at": "2017-11-19 12:35:27",
    "updated_at": "2017-11-19 12:35:27",
    "deleted_at": null,
    "driver": null
}
```

### HTTP Request
`GET api/drivers/{id}`

`HEAD api/drivers/{id}`


<!-- END_f677a4f7e42debbdf07bd71717ae2d2d -->

<!-- START_2d2599fd7a507d56cb9721b6d1afed81 -->
## api/drivers/{id}/accept

> Example request:

```bash
curl -X POST "http://localhost:8000/api/drivers/{id}/accept" \
-H "Accept: application/json" \
-H "Authorization: Bearer eyJ0eXAiOiJ..." \
    -d "driver_id"="ipsum" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/drivers/{id}/accept",
    "method": "POST",
    "data": {
        "driver_id": "ipsum"
},
    "headers": {
        "accept": "application/json",
		"Authorization": "Bearer eyJ0eXAiOiJ..."
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": false,
    "road": {
        "id": 8,
        "user_id": 6,
        "location_latitude": "-33.4309221",
        "location_longitude": "-55.7682438",
        "driver_id": 7,
        "location_address": "Home",
        "location_reference": "Home",
        "price": 0,
        "destine_address": "Home",
        "route": null,
        "status": "RV",
        "type": "N",
        "reservation_date": null,
        "anticipation_send": null,
        "cooperative_id": null,
        "job_id": null,
        "created_at": "2017-11-19 12:35:27",
        "updated_at": "2017-11-19 13:37:56",
        "deleted_at": null,
        "driver": {
            "id": 7,
            "name": "driver",
            "email": "driver@udv.com",
            "device_id": null,
            "cooperativa_id": 1,
            "role": "T",
            "status": "P",
            "availability": "O",
            "image": null,
            "latitude": "-33.4309221",
            "longitude": "-55.7682438",
            "phone": "5555555",
            "trademark": null,
            "license_plate": null,
            "created_at": "2017-11-19 11:56:46",
            "updated_at": "2017-11-19 13:37:56",
            "deleted_at": null
        }
    },
    "forbidden": false
}
```

### HTTP Request
`POST api/drivers/{id}/accept`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    driver_id | string |  required  | Valid user id

<!-- END_2d2599fd7a507d56cb9721b6d1afed81 -->

<!-- START_995c0785cdaa2c8b51f04521e4afe4fd -->
## api/drivers/{id}/cancel

> Example request:

```bash
curl -X POST "http://localhost:8000/api/drivers/{id}/cancel" \
-H "Accept: application/json" \
-H "Authorization: Bearer eyJ0eXAiOiJ..." \
    -d "user_id"="nobis" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/drivers/{id}/cancel",
    "method": "POST",
    "data": {
        "user_id": "nobis"
},
    "headers": {
        "accept": "application/json",
		"Authorization": "Bearer eyJ0eXAiOiJ..."
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": false,
    "road": {
        "id": 8,
        "user_id": 6,
        "location_latitude": "-33.4309221",
        "location_longitude": "-55.7682438",
        "driver_id": 7,
        "location_address": "Home",
        "location_reference": "Home",
        "price": 0,
        "destine_address": "Home",
        "route": null,
        "status": "P",
        "type": "N",
        "reservation_date": null,
        "anticipation_send": null,
        "cooperative_id": null,
        "job_id": null,
        "created_at": "2017-11-19 12:35:27",
        "updated_at": "2017-11-19 13:51:46",
        "deleted_at": null
    }
}
```

### HTTP Request
`POST api/drivers/{id}/cancel`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    user_id | string |  required  | Valid user id

<!-- END_995c0785cdaa2c8b51f04521e4afe4fd -->

<!-- START_737e19b784c174d7eb299a153e21e78e -->
## api/drivers/{id}/finish

> Example request:

```bash
curl -X POST "http://localhost:8000/api/drivers/{id}/finish" \
-H "Accept: application/json" \
-H "Authorization: Bearer eyJ0eXAiOiJ..." \
    -d "price"="48" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/drivers/{id}/finish",
    "method": "POST",
    "data": {
        "price": 48
},
    "headers": {
        "accept": "application/json",
		"Authorization": "Bearer eyJ0eXAiOiJ..."
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": false,
    "road": {
        "id": 8,
        "user_id": 6,
        "location_latitude": "-33.4309221",
        "location_longitude": "-55.7682438",
        "driver_id": 7,
        "location_address": "Home",
        "location_reference": "Home",
        "price": "50",
        "destine_address": "Home",
        "route": null,
        "status": "F",
        "type": "N",
        "reservation_date": null,
        "anticipation_send": null,
        "cooperative_id": null,
        "job_id": null,
        "created_at": "2017-11-19 12:35:27",
        "updated_at": "2017-11-19 13:51:46",
        "deleted_at": null
    },
    "driver": {
        "id": 7,
        "name": "driver",
        "email": "driver@udv.com",
        "device_id": null,
        "cooperativa_id": 1,
        "role": "T",
        "status": "P",
        "availability": "L",
        "image": null,
        "latitude": "-33.4309221",
        "longitude": "-55.7682438",
        "phone": "5555555",
        "trademark": null,
        "license_plate": null,
        "created_at": "2017-11-19 11:56:46",
        "updated_at": "2017-11-19 13:51:46",
        "deleted_at": null
    }
}
```

### HTTP Request
`POST api/drivers/{id}/finish`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    price | numeric |  required  | 

<!-- END_737e19b784c174d7eb299a153e21e78e -->

<!-- START_894d66d6c6d376649ac7816f569309c1 -->
## api/drivers/{id}/notify

> Example request:

```bash
curl -X POST "http://localhost:8000/api/drivers/{id}/notify" \
-H "Accept: application/json" \
-H "Authorization: Bearer eyJ0eXAiOiJ..." \
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/drivers/{id}/notify",
    "method": "POST",
    "headers": {
        "accept": "application/json",
		"Authorization": "Bearer eyJ0eXAiOiJ..."
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "id": 8,
    "user_id": 6,
    "location_latitude": "-33.4309221",
    "location_longitude": "-55.7682438",
    "driver_id": 7,
    "location_address": "Home",
    "location_reference": "Home",
    "price": 0,
    "destine_address": "Home",
    "route": null,
    "status": "P",
    "type": "N",
    "reservation_date": null,
    "anticipation_send": null,
    "cooperative_id": null,
    "job_id": null,
    "created_at": "2017-11-19 12:35:27",
    "updated_at": "2017-11-19 13:51:46",
    "deleted_at": null,
    "user": {
        "id": 6,
        "name": "demo",
        "email": "demo@demo.com",
        "device_id": null,
        "cooperativa_id": null,
        "role": "C",
        "status": "1",
        "availability": null,
        "image": null,
        "latitude": null,
        "longitude": null,
        "phone": "55555555",
        "trademark": null,
        "license_plate": null,
        "created_at": "2017-11-19 11:36:37",
        "updated_at": "2017-11-19 11:36:37",
        "deleted_at": null
    }
}
```

### HTTP Request
`POST api/drivers/{id}/notify`


<!-- END_894d66d6c6d376649ac7816f569309c1 -->

<!-- START_499e264e91928bfdc45483913273a40a -->
## api/drivers/{id}/start

> Example request:

```bash
curl -X POST "http://localhost:8000/api/drivers/{id}/start" \
-H "Accept: application/json" \
-H "Authorization: Bearer eyJ0eXAiOiJ..." \
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/drivers/{id}/start",
    "method": "POST",
    "headers": {
        "accept": "application/json",
		"Authorization": "Bearer eyJ0eXAiOiJ..."
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": false,
    "road": {
        "id": 8,
        "user_id": 6,
        "location_latitude": "-33.4309221",
        "location_longitude": "-55.7682438",
        "driver_id": 7,
        "location_address": "Home",
        "location_reference": "Home",
        "price": 0,
        "destine_address": "Home",
        "route": null,
        "status": "E",
        "type": "N",
        "reservation_date": null,
        "anticipation_send": null,
        "cooperative_id": null,
        "job_id": null,
        "created_at": "2017-11-19 12:35:27",
        "updated_at": "2017-11-19 14:00:15",
        "deleted_at": null
    }
}
```

### HTTP Request
`POST api/drivers/{id}/start`


<!-- END_499e264e91928bfdc45483913273a40a -->

<!-- START_19ff1b6f8ce19d3c444e9b518e8f7160 -->
## api/auth/logout

> Example request:

```bash
curl -X POST "http://localhost:8000/api/auth/logout" \
-H "Accept: application/json" \
-H "Authorization: Bearer eyJ0eXAiOiJ..." \
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/auth/logout",
    "method": "POST",
    "headers": {
        "accept": "application/json",
		"Authorization": "Bearer eyJ0eXAiOiJ..."
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/auth/logout`


<!-- END_19ff1b6f8ce19d3c444e9b518e8f7160 -->

