<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$seed = DB::table('users')->where('id', 1)->first();
		
		if (empty($seed)) {
			DB::table('users')->insert([
				'name' => 'admin',
				'email' => 'admin@example.com',
				'password' => bcrypt('password'),
				'role' => 'S',
				'status' => true,
				'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
			]);
		}
    }
}
