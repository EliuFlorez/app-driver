<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDriver extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
			$table->dropColumn(['cooperativa_id', 'trademark', 'license_plate']);
			$table->integer('cooperative_id')->nullable();
			$table->string('dni')->nullable();
			$table->string('rut')->nullable();
			$table->string('address')->nullable();
			$table->string('cell')->nullable();
			$table->string('number_bps')->nullable();
			$table->string('license_cta')->nullable();
			$table->string('license_vto')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
