<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roads', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('user_id');
			$table->string('location_latitude');
			$table->string('location_longitude'); 
			$table->integer('driver_id');
			$table->string('location_address');
			$table->string('location_reference');
			$table->double('price');
			$table->string('destine_address');
			$table->string('route');
			$table->char('status', 1);
			$table->char('type', 1);
			$table->date('reservation_date');
			$table->string('anticipation_send'); 
			$table->integer('cooperative_id');
			$table->integer('job_id');
            $table->timestamps();
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roads');
    }
}
