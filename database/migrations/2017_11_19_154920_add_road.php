<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRoad extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('roads', function (Blueprint $table) {
			$table->integer('driver_id')->nullable()->change();
			$table->string('route')->nullable()->change();
			$table->date('reservation_date')->nullable()->change();
			$table->string('anticipation_send')->nullable()->change();
			$table->integer('cooperative_id')->nullable()->change();
			$table->integer('job_id')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
